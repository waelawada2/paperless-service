FROM 368978185270.dkr.ecr.us-east-1.amazonaws.com/stb-oraclejdk-overops:latest

COPY target/paperless-service*.jar app.jar
COPY docker/app.sh /
RUN chmod +x /app.sh

EXPOSE 8081

ENV PORT=8081
ENV JAVA_OPTS="-Xmx384M -Djava.security.egd=file:/dev/./urandom"

ENTRYPOINT [ "/app.sh" ]
