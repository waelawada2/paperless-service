# Paperless Service

## Overview

This is the paperless service, a backend service for the bidder agent's app and the auctioneer's book. 

## How to Build

Build the project with the embedded [Maven Wrapper](https://github.com/takari/maven-wrapper):

```
$ ./mvnw clean package
```

## How to Test

Run unit tests with Maven:

```
$ ./mvnw clean test
```

## How to Run

Run the application using the [spring-boot-maven-plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html):

```
$ ./mvnw spring-boot:run
```

This will launch the application and make it available on the configured port (property `server.port` in `application.yml`, 8081 by default):

```
$ curl http://localhost:8081/paperless-service/v1/greeting
```

## Database
This application requires a database to run. For dev, you can easily start a MySql docker container with the following command:
```
docker run --name stb-mysql -e MYSQL_ROOT_PASSWORD=stb-secret -e MYSQL_DATABASE=paperless_service -v ~/mysql-data:/var/lib/mysql --rm -it -p3306:3306 mysql:5
``` 

## Swagger

Once the application is running, the Swagger file can be retrieved at [/paperless-service/v1/docs/api](http://localhost:8081/paperless-service/v1/docs/api):

```
$ curl http://localhost:8081/paperless-service/v1/docs/api
```

Swagger UI is available at [/paperless-service/v1/swagger-ui.html](http://localhost:8081/paperless-service/v1/swagger-ui.html).

## Actuator Endpoints

The actuator endpoints are mapped to the `/management` path.

For example, the Actuator health endpoint can be accessed as follows:

```
$ curl http://localhost:8081/paperless-service/v1/management/health
```

## Additional Tasks

- The generade code contains a dummy password to securty the actuator endpoints. Change the password with property `security.user.password` in the `application.yml` file. 
- Change the value of property `spring.cloud.config.failFast` to `true` as soon as a corresponding configuration repository has been set up and configured in the Config Server instances in ECS.

## Additional Documentations

* [The Spring Boot Reference Guide](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/). The official Spring Boot reference documentation.
* [The Spring Cloud Reference Guide](http://cloud.spring.io/spring-cloud-static/Dalston.SR4/single/spring-cloud.html). The official Spring Cloud reference documentation for the Dalson release train.
