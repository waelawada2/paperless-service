package com.sothebys.paperless.controller;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.sothebys.paperless.domain.AuctionCard;
import com.sothebys.paperless.domain.AuctionCards;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.exceptions.AuctionSessionStateUpdateException;
import com.sothebys.paperless.exceptions.ControllerExceptionHandlerAdvice;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import com.sothebys.paperless.service.AuctionService;
import com.sothebys.paperless.util.UserHandlerMethodParameterResolver;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class AuctionControllerTest {

  private static final String AUCTIONS_CARDS_PATH = "/auctions/cards";
  private static final String AUCTIONS_CARDS_BY_AGENT_PATH = "/auctions/cardsByAgent";
  private static final String CHANGE_AUCTION_SESSION_STATE_PATH = "/auctions/state";
  private static final String SAMPLE_AUCTION_SESSION_ID = "HK0340_1";
  private static final String SAMPLE_AUCTION_ID = "HK0340";
  private static final String SAMPLE_AUCTIONEER_ID = "alejandra.rossetti";
  private static final int SAMPLE_SESSION_NUMBER = 1;
  // GMT: Friday, March 16, 2018 2:00:00 PM
  private static final long TIMESTAMP = 1521208800000L;

  private ObjectMapper objectMapper = new ObjectMapper();
  private static final User SAMPLE_USER =
      new User("Name Last", "name@sothebys.com", "name.lastname");

  @Mock
  private AuctionService auctionService;

  @Mock
  private UserHandlerMethodParameterResolver userResolver;

  @InjectMocks
  private AuctionController auctionController;

  private MockMvc mvc;

  private AuctionCard mockAuctionCard;

  @Before
  public void setup() throws Exception {

    MockitoAnnotations.initMocks(this);
    mvc = MockMvcBuilders.standaloneSetup(auctionController)
        .setControllerAdvice(new ControllerExceptionHandlerAdvice())
        .setCustomArgumentResolvers(userResolver).build();

    // Return user
    when(userResolver.supportsParameter(any())).thenReturn(true);
    when(userResolver.resolveArgument(any(), any(), any(), any())).thenReturn(SAMPLE_USER);

    // prepare mock AuctionCard
    mockAuctionCard = new AuctionCard(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER);
    mockAuctionCard.setState(AuctionSessionState.OPEN);

    when(auctionService.getAllAuctionCards()).thenReturn(getMockedAuctionCards());
    doThrow(new AuctionSessionStateUpdateException(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER,
        AuctionSessionState.CLOSED)).when(auctionService).closeAuctionSession(mockAuctionCard,
            null);

  }

  @Test
  public void testGetAuctionsByAgent() throws Exception {
    String url = String.format("%s", AUCTIONS_CARDS_BY_AGENT_PATH);
    mvc.perform(get(url).accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
    verify(auctionService).getAuctionCardsForAgent(SAMPLE_USER);
  }

  @Test
  public void testGetAuctionCards() throws Exception {
    mvc.perform(get(AUCTIONS_CARDS_PATH)).andExpect(status().isOk())
        .andExpect(jsonPath("$.cards").exists()).andExpect(jsonPath("$.locations").exists())
        .andExpect(jsonPath("$.cards.length()").value(2))
        .andExpect(jsonPath("$.locations.length()").value(2));
    verify(auctionService).getAllAuctionCards();
  }

  @Test
  public void testGetAuctionCards_failsServerError() throws Exception {
    when(auctionService.getAllAuctionCards())
        .thenThrow(new DataRetrievalFailureException("Data Exception"));
    mvc.perform(get(AUCTIONS_CARDS_PATH)).andExpect(status().isInternalServerError())
        .andExpect(jsonPath("$.error").exists())
        .andExpect(jsonPath("$.error", startsWith("Cannot access data from the database")));
  }

  @Test
  public void testPostAuctions_OpenAuctionSession() throws Exception {
    String url = String.format(CHANGE_AUCTION_SESSION_STATE_PATH, SAMPLE_AUCTION_SESSION_ID);
    String content = objectMapper.writeValueAsString(mockAuctionCard);
    mvc.perform(post(url).content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    verify(auctionService, times(1)).openAuctionSession(mockAuctionCard, SAMPLE_USER);
    verifyNoMoreInteractions(auctionService);
  }

  @Test
  public void testPostAuctions_failsTryingOutsideTimeWindow() throws Exception {
    String url = String.format(CHANGE_AUCTION_SESSION_STATE_PATH, SAMPLE_AUCTION_SESSION_ID);

    // plot twist! trying to open an auction, the next day
    doThrow(new AuctionSessionStateUpdateException("message", AuctionSessionState.OPEN, TIMESTAMP))
        .when(auctionService).openAuctionSession(eq(mockAuctionCard), eq(SAMPLE_USER));

    String content = objectMapper.writeValueAsString(mockAuctionCard);
    MvcResult mvcResult =
        mvc.perform(post(url).content(content).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError()).andReturn();

    Map<String, String> errorObject = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(),
        TypeFactory.defaultInstance().constructMapType(HashMap.class, String.class, String.class));

    assertTrue(errorObject.get("error").contains("outside time window for state change"));

    verify(auctionService, times(1)).openAuctionSession(mockAuctionCard, SAMPLE_USER);
    verifyNoMoreInteractions(auctionService);
  }

  @Test
  public void testPostAuctions_failsNoAuctionSessionRecord() throws Exception {
    String url = String.format(CHANGE_AUCTION_SESSION_STATE_PATH, SAMPLE_AUCTION_SESSION_ID);

    // plot twist! no AuctionSession previously recorded
    doThrow(new AuctionSessionStateUpdateException(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER))
        .when(auctionService).openAuctionSession(eq(mockAuctionCard), eq(SAMPLE_USER));

    String content = objectMapper.writeValueAsString(mockAuctionCard);
    MvcResult mvcResult = mvc
        .perform(post(url).content(content).contentType(MediaType.APPLICATION_JSON)
            .param("auctioneer", SAMPLE_AUCTIONEER_ID))
        .andExpect(status().is4xxClientError()).andReturn();

    Map<String, String> errorObject = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(),
        TypeFactory.defaultInstance().constructMapType(HashMap.class, String.class, String.class));

    assertTrue(errorObject.get("error").contains("Cannot find auction HK0340 session 1"));

    verify(auctionService, times(1)).openAuctionSession(mockAuctionCard, SAMPLE_USER);
    verifyNoMoreInteractions(auctionService);
  }

  @Test
  public void testPostAuctions_failsAuctionSessionAlreadyOpen() throws Exception {
    String url = String.format(CHANGE_AUCTION_SESSION_STATE_PATH, SAMPLE_AUCTION_SESSION_ID);

    // plot twist! AuctionSession already open
    doThrow(new AuctionSessionStateUpdateException(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER,
        AuctionSessionState.OPEN)).when(auctionService).openAuctionSession(eq(mockAuctionCard),
            eq(SAMPLE_USER));

    String content = objectMapper.writeValueAsString(mockAuctionCard);
    MvcResult mvcResult =
        mvc.perform(post(url).content(content).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError()).andReturn();

    Map<String, String> errorObject = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(),
        TypeFactory.defaultInstance().constructMapType(HashMap.class, String.class, String.class));

    assertTrue(errorObject.get("error").contains("Cannot change state"));

    verify(auctionService, times(1)).openAuctionSession(mockAuctionCard, SAMPLE_USER);
    verifyNoMoreInteractions(auctionService);
  }

  // Helper methods

  private AuctionCards getMockedAuctionCards() {
    long timestamp = Instant.now().toEpochMilli();
    AuctionCard card1 =
        new AuctionCard("1", "Auction 1", 1, 1, "200", "16 March 2018", "10:00 AM EDT", timestamp,
            timestamp, timestamp, "New York", null, AuctionSessionState.SCHEDULED, null);
    AuctionCard card2 =
        new AuctionCard("2", "Auction 2", 1, 1, "100", "16 March 2018", "02:00 PM GMT", timestamp,
            timestamp, timestamp, "London", null, AuctionSessionState.SCHEDULED, null);
    return new AuctionCards(Arrays.asList(card1, card2),
        Stream.of("New York", "London").collect(Collectors.toSet()));
  }
}
