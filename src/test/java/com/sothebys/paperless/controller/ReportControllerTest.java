package com.sothebys.paperless.controller;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.sothebys.paperless.exceptions.EntityNotFoundException;
import com.sothebys.paperless.service.ReportService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(ReportController.class)
@RunWith(SpringRunner.class)
public class ReportControllerTest {

  private String SAMPLE_AUCTION_ID = "L18228";
  private int SAMPLE_SESSION_NUMBER = 1;

  @Autowired
  private MockMvc mvc;

  @MockBean
  private ReportService mockReportService;

  @Before
  public void setup() {
    when(mockReportService.getCSVReport(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER))
        .thenReturn("report");
    when(mockReportService.getReportFileName(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER))
        .thenReturn("filename.csv");
  }

  @Test
  public void testSuccess() throws Exception {
    String url = String.format("/reports/download?auctionId=%s&sessionNumber=%s", SAMPLE_AUCTION_ID,
        SAMPLE_SESSION_NUMBER);
    mvc.perform(get(url)).andExpect(status().isOk());
  }

  @Test
  public void test_auction_session_not_found() throws Exception {
    doThrow(new EntityNotFoundException("")).when(mockReportService).getCSVReport(SAMPLE_AUCTION_ID,
        SAMPLE_SESSION_NUMBER);
    String url = String.format("/reports/download?auctionId=%s&sessionNumber=%s", SAMPLE_AUCTION_ID,
        SAMPLE_SESSION_NUMBER);
    mvc.perform(get(url)).andExpect(status().isNotFound());
  }
}
