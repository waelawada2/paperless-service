package com.sothebys.paperless.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.sothebys.paperless.domain.AuctionDTO;
import com.sothebys.paperless.domain.BidLot;
import com.sothebys.paperless.domain.LotClosureResult;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.domain.integration.BidLots;
import com.sothebys.paperless.exceptions.AuctionNotFoundException;
import com.sothebys.paperless.exceptions.ControllerExceptionHandlerAdvice;
import com.sothebys.paperless.exceptions.LotNotFoundException;
import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.entity.LotResult;
import com.sothebys.paperless.service.AuctionService;
import com.sothebys.paperless.service.LotService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

public class LotControllerTest {

  private static final String AUCTIONS_PATH = "/auctions";
  private static final int SESSION_NUMBER = 1;
  private static final String AUCTION_ID = "P0104";
  private static final String LOT_ID = "501";
  private static final Long SAMPLE_HAMMER_PRICE = 450000L;
  private static final String SAMPLE_PADDLE = "L2045";
  private static final User SAMPLE_USER =
      new User("Name Lastname", "name.lastname@sothebys.com", "name.lastname");

  private ObjectMapper objectMapper = new ObjectMapper();

  private MockMvc mvc;

  @InjectMocks
  private LotController lotController;

  @Mock
  private AuctionService auctionService;

  @Mock
  private LotService lotService;

  private LotResult aLotResult;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    mvc = MockMvcBuilders.standaloneSetup(lotController)
        .setControllerAdvice(new ControllerExceptionHandlerAdvice())
        .setValidator(new LocalValidatorFactoryBean()).build();
    LotId mockLotId = new LotId(LOT_ID, AUCTION_ID, SESSION_NUMBER);
    aLotResult = new LotResult(mockLotId, null, SAMPLE_HAMMER_PRICE, LotClosureResult.SLD,
        SAMPLE_PADDLE, SAMPLE_USER.getUsername());
    when(auctionService.getLotsByAuction(AUCTION_ID, SESSION_NUMBER))
        .thenReturn(getMockedBidLots());
    when(lotService.getLotData(eq(AUCTION_ID), any(Integer.class), eq(LOT_ID)))
        .thenReturn(new BidLot());
  }

  @Test
  public void getAllLots() throws Exception {
    String url =
        String.format("%s/%s/lots?sessionNumber=%s", AUCTIONS_PATH, AUCTION_ID, SESSION_NUMBER);
    mvc.perform(get(url)).andExpect(status().isOk()).andExpect(jsonPath("$.lots").exists())
        .andExpect(jsonPath("$.auction").exists());
  }

  @Test
  public void getLotsByAgent() throws Exception {
    String url = String.format("%s/%s/lots?sessionNumber=1", AUCTIONS_PATH, AUCTION_ID);
    mvc.perform(get(url)).andExpect(status().isOk());
  }

  @Test
  public void closeLot_succeeds() throws Exception {
    String url = String.format("%s/%s/lots/%s?sessionNumber=1", AUCTIONS_PATH, AUCTION_ID, LOT_ID);
    String content = objectMapper.writeValueAsString(aLotResult);
    MvcResult result =
        mvc.perform(post(url).content(content).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();
    assertNotNull(result.getResponse().getContentAsString());
  }

  @Test
  public void closeLot_fails_missingSessionNumber() throws Exception {
    String url = String.format("%s/%s/lots/%s", AUCTIONS_PATH, AUCTION_ID, LOT_ID);
    String content = objectMapper.writeValueAsString(aLotResult);
    MvcResult mvcResult =
        mvc.perform(post(url).content(content).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest()).andReturn();

    Map<String, String> errorObject = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(),
        TypeFactory.defaultInstance().constructMapType(HashMap.class, String.class, String.class));
    assertTrue(errorObject.get("error").contains("Bad argument received"));
  }

  @Test
  public void closeLot_fails_badSessionNumber() throws Exception {
    String url = String.format("%s/%s/lots/%s?sessionNumber=N", AUCTIONS_PATH, AUCTION_ID, LOT_ID);
    String content = objectMapper.writeValueAsString(aLotResult);
    mvc.perform(post(url).content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void getBidderData_succeeds() throws Exception {
    String url = String.format("%s/%s/lots/%s?sessionNumber=1", AUCTIONS_PATH, AUCTION_ID, LOT_ID);
    mvc.perform(get(url)).andExpect(status().isOk());
  }

  @Test
  public void getBidderData_fails_AuctionNotFound() throws Exception {
    when(lotService.getLotData(eq(AUCTION_ID), any(Integer.class), eq(LOT_ID)))
        .thenThrow(new AuctionNotFoundException(AUCTION_ID));
    String url = String.format("%s/%s/lots/%s?sessionNumber=1", AUCTIONS_PATH, AUCTION_ID, LOT_ID);
    mvc.perform(get(url)).andExpect(status().isNotFound());
  }

  @Test
  public void getBidderData_fails_LotNotFound() throws Exception {
    when(lotService.getLotData(eq(AUCTION_ID), any(Integer.class), eq(LOT_ID)))
        .thenThrow(new LotNotFoundException(LOT_ID));
    String url = String.format("%s/%s/lots/%s?sessionNumber=1", AUCTIONS_PATH, AUCTION_ID, LOT_ID);
    mvc.perform(get(url)).andExpect(status().isNotFound());
  }

  // Mocked data

  private BidLots getMockedBidLots() {
    BidLot lot = new BidLot();
    AuctionDTO auction = new AuctionDTO();
    BidLots bidLots = new BidLots(Arrays.asList(lot), auction);
    return bidLots;
  }
};
