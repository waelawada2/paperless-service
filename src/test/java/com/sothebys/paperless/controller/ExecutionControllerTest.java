package com.sothebys.paperless.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.paperless.domain.BidClient;
import com.sothebys.paperless.domain.LotExecutionResult;
import com.sothebys.paperless.domain.LotResult;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.exceptions.ControllerExceptionHandlerAdvice;
import com.sothebys.paperless.persistence.entity.LotExecution;
import com.sothebys.paperless.persistence.entity.LotExecutionId;
import com.sothebys.paperless.persistence.repository.LotRepository;
import com.sothebys.paperless.service.LotExecutionService;
import com.sothebys.paperless.util.UserHandlerMethodParameterResolver;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class ExecutionControllerTest {

  private static final Integer SAMPLE_SESSION_NUMBER = 2;
  private static final String EXECUTIONS_PATH = "/auctions/%s/lots/%s/executions?sessionNumber=%s";
  private static final String SAMPLE_LOT_ID = "19";
  private static final String SAMPLE_AUCTION_ID = "L0180_2";
  private static final User SAMPLE_USER =
      new User("Name Lastname", "name.lastname@sothebys.com", "name.lastname");
  private static final String SAMPLE_CLIENT_NUMBER = "1234";
  private static final String SAMPLE_CLIENT_ID = "1234";
  private static final Integer SAMPLE_CLIENT_LEVEL = 4;
  private static final Long SAMPLE_AMOUNT = 345000L;
  private static final String SAMPLE_PADDLE = "P0193";

  private ObjectMapper objectMapper = new ObjectMapper();

  private MockMvc mvc;

  @InjectMocks
  private ExecutionController executionController;

  @Mock
  private LotExecutionService lotExecutionService;

  @Mock
  private LotRepository lotRepo;

  @Mock
  private UserHandlerMethodParameterResolver userResolver;

  private LotExecution confirmed;
  private LotResult lotExecutionResult;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    mvc = MockMvcBuilders.standaloneSetup(executionController)
        .setControllerAdvice(new ControllerExceptionHandlerAdvice())
        .setCustomArgumentResolvers(userResolver).build();
    BidClient bidClient = new BidClient();
    bidClient.setClientLevel(SAMPLE_CLIENT_LEVEL);
    bidClient.setClientNumber(SAMPLE_CLIENT_NUMBER);
    bidClient.setPaddle(SAMPLE_PADDLE);
    lotExecutionResult = new LotResult(bidClient, LotExecutionResult.WON, SAMPLE_AMOUNT);

    confirmed = new LotExecution();
    LotExecutionId id = new LotExecutionId(SAMPLE_USER.getUsername(), SAMPLE_LOT_ID,
        SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER);
    confirmed.setId(id);
    confirmed.setClientId(SAMPLE_CLIENT_ID);

    // Return user
    when(userResolver.supportsParameter(any())).thenReturn(true);
    when(userResolver.resolveArgument(any(), any(), any(), any())).thenReturn(SAMPLE_USER);
  }

  @Test
  public void executeLot() throws Exception {
    String url =
        String.format(EXECUTIONS_PATH, SAMPLE_AUCTION_ID, SAMPLE_LOT_ID, SAMPLE_SESSION_NUMBER);
    String content = objectMapper.writeValueAsString(lotExecutionResult);
    mvc.perform(post(url).content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    verify(lotExecutionService, times(1)).confirmLotExecution(lotExecutionResult, SAMPLE_AUCTION_ID,
        SAMPLE_SESSION_NUMBER, SAMPLE_LOT_ID, SAMPLE_USER);
    verifyNoMoreInteractions(lotExecutionService);
  }

}
