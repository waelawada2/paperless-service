package com.sothebys.paperless.service.impl;

import static org.junit.Assert.assertEquals;
import com.sothebys.paperless.domain.LotState;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import com.sothebys.paperless.persistence.entity.LotExecution;
import com.sothebys.paperless.persistence.entity.LotResult;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class LocalStateManagerTest {

  @InjectMocks
  private LocalStateManager underTest;

  private Optional<LotResult> lotResult;
  private AuctionSessionState auctionSessionState;
  private Optional<LotExecution> lotExecution;

  @Before
  public void setUp() {
    lotResult = Optional.ofNullable(new LotResult());
    auctionSessionState = AuctionSessionState.SCHEDULED;
    lotExecution = Optional.ofNullable(new LotExecution());
  }

  @Test
  public void test_auctioneer_lotState_CLOSED() {
    LotState actual = underTest.deriveLotState(lotResult, auctionSessionState);
    LotState expected = LotState.CLOSED;

    assertEquals(expected, actual);
  }

  @Test
  public void test_auctioneer_lotState_SCHEDULED() {
    lotResult = Optional.ofNullable(null);
    LotState actual = underTest.deriveLotState(lotResult, auctionSessionState);
    LotState expected = LotState.SCHEDULED;

    assertEquals(expected, actual);
  }

  @Test
  public void test_auctioneer_lotState_OPEN() {
    lotResult = Optional.ofNullable(null);
    auctionSessionState = AuctionSessionState.OPEN;
    LotState actual = underTest.deriveLotState(lotResult, auctionSessionState);
    LotState expected = LotState.OPEN;

    assertEquals(expected, actual);
  }

  @Test(expected = IllegalArgumentException.class)
  public void test_auctioneer_fails_auctionStateNull() {
    lotResult = Optional.ofNullable(null);
    auctionSessionState = null;

    underTest.deriveLotState(lotResult, auctionSessionState);
  }

  @Test
  public void test_bidder_lotState_CLOSED() {
    LotState actual = underTest.deriveLotState(lotResult, lotExecution, auctionSessionState);
    LotState expected = LotState.CLOSED;

    assertEquals(expected, actual);
  }

  @Test
  public void test_bidder_lotState_PENDING() {
    lotExecution = Optional.ofNullable(null);
    LotState actual = underTest.deriveLotState(lotResult, lotExecution, auctionSessionState);
    LotState expected = LotState.PENDING;

    assertEquals(expected, actual);
  }

  @Test
  public void test_bidder_lotState_CONFIRMED() {
    lotResult = Optional.ofNullable(null);
    LotState actual = underTest.deriveLotState(lotResult, lotExecution, auctionSessionState);
    LotState expected = LotState.CONFIRMED_BY_BIDDER;

    assertEquals(expected, actual);
  }

  @Test
  public void test_bidder_lotState_OPEN() {
    lotExecution = Optional.ofNullable(null);
    lotResult = Optional.ofNullable(null);
    auctionSessionState = AuctionSessionState.OPEN;
    LotState actual = underTest.deriveLotState(lotResult, lotExecution, auctionSessionState);
    LotState expected = LotState.OPEN;

    assertEquals(expected, actual);
  }

  @Test
  public void test_bidder_lotState_SCHEDULED() {
    lotExecution = Optional.ofNullable(null);
    lotResult = Optional.ofNullable(null);
    LotState actual = underTest.deriveLotState(lotResult, lotExecution, auctionSessionState);
    LotState expected = LotState.SCHEDULED;

    assertEquals(expected, actual);
  }

  @Test(expected = IllegalArgumentException.class)
  public void test_bidder_fails_auctionSessionStateNull() {
    lotExecution = Optional.ofNullable(null);
    lotResult = Optional.ofNullable(null);
    auctionSessionState = null;
    underTest.deriveLotState(lotResult, lotExecution, auctionSessionState);

  }

}
