package com.sothebys.paperless.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import com.sothebys.paperless.client.SAPClient;
import com.sothebys.paperless.domain.AbsenteeBid;
import com.sothebys.paperless.domain.BidLot;
import com.sothebys.paperless.domain.LotClosureResult;
import com.sothebys.paperless.domain.sap.SapSaleRecord;
import com.sothebys.paperless.exceptions.AuctionNotFoundException;
import com.sothebys.paperless.exceptions.LotClosureException;
import com.sothebys.paperless.exceptions.LotNotFoundException;
import com.sothebys.paperless.persistence.entity.Auction;
import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.AuctionSessionId;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import com.sothebys.paperless.persistence.entity.Lot;
import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.entity.LotResult;
import com.sothebys.paperless.persistence.repository.AuctionSessionRepository;
import com.sothebys.paperless.persistence.repository.LotRepository;
import com.sothebys.paperless.persistence.repository.LotResultRepository;
import com.sothebys.paperless.util.AbsenteeBidsFormatter;
import com.sothebys.paperless.util.PaddedSourceNumberFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class LotServiceImplTest {

  private static final String SAMPLE_AUCTION_ID = "L0180";
  private static final String SAMPLE_LOT_ID = "19";
  private static final Integer SAMPLE_SESSION_NUMBER = 1;
  private static final Long MOCK_RESERVE_LONG = 15000L;
  private static final String MOCK_LOW = "   9,000";
  private static final String MOCK_HIGH = "    17,000";
  private static final String MOCK_RESERVE = "     15,000";

  @Mock
  private LotRepository lotRepository;
  @Mock
  private LotResultRepository lotResultRepository;
  @Mock
  private LocalStateManager mockLocalStateManager;
  @Mock
  private AbsenteeBidsFormatter absenteeBidsFormatter;
  @Mock
  private SAPClient sapClient;
  @Mock
  private PaddedSourceNumberFormatter amountFormatter;
  @Mock
  private AuctionSessionRepository mockedAuctionSessionRepository;

  @InjectMocks
  private LotServiceImpl underTest;

  @Rule
  public ExpectedException expectedLotException = ExpectedException.none();


  private Lot mockLot;
  private LotId mockLotId;
  private AuctionSession mockAuctionSession;
  private LotResult mockLotResult;

  @Before
  public void setUp() {
    mockAuctionSession = new AuctionSession(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER);
    mockLotId = new LotId(SAMPLE_LOT_ID, SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER);
    mockLot = new Lot(mockLotId, mockAuctionSession, MOCK_RESERVE_LONG);
    mockLot.setReservePrice(MOCK_RESERVE_LONG);
    mockAuctionSession.setLots(Arrays.asList(mockLot));
    Auction auction = new Auction();
    auction.setCurrency("USD");
    mockAuctionSession.setAuction(auction);
    mockLotResult = new LotResult();
    mockLotResult.setPaddleNumber("L10");
    when(mockedAuctionSessionRepository.findOptionalById(eq(mockAuctionSession.getId())))
        .thenReturn(Optional.of(mockAuctionSession));
    when(lotRepository.findOptionalById(mockLotId)).thenReturn(Optional.of(mockLot));
    when(absenteeBidsFormatter.formatAbsenteeBidList(anyLong(), anyListOf(AbsenteeBid.class)))
        .thenReturn(new ArrayList<AbsenteeBid>());
    SapSaleRecord mockSapSaleRecord = mock(SapSaleRecord.class);
    when(sapClient.getLotStatusReserveEstimates(anyString(), anyString()))
        .thenReturn(mockSapSaleRecord);

    when(mockSapSaleRecord.getLow()).thenReturn(MOCK_LOW);
    when(mockSapSaleRecord.getHigh()).thenReturn(MOCK_HIGH);
    when(mockSapSaleRecord.getReserve()).thenReturn(MOCK_RESERVE);

    when(amountFormatter.parseAmount(anyString())).thenReturn(MOCK_RESERVE_LONG);

    mockLotResult = new LotResult();
    mockLotResult.setHammerPrice(10000L);
    mockLotResult.setPaddleNumber("L0001");
    mockLotResult.setStatus(LotClosureResult.BI);
  }

  @Test
  public void getBidLotData_fails_lotNotFound() {
    when(lotRepository.findOptionalById(mockLotId)).thenReturn(Optional.empty());
    expectedLotException.expect(LotNotFoundException.class);
    expectedLotException.expectMessage("Could not find lot");

    underTest.getLotData(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER, SAMPLE_LOT_ID);
  }

  @Test
  public void getBidLotData_succeeds_whenEntryFound() {
    BidLot bidLot = underTest.getLotData(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER, SAMPLE_LOT_ID);
    assertEquals(MOCK_RESERVE_LONG, bidLot.getReservePrice());
  }

  @Test
  public void closeLot_Sold_suceeds() {
    when(mockedAuctionSessionRepository
        .findOptionalById(new AuctionSessionId(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER)))
            .thenReturn(Optional.of(mockAuctionSession));
    underTest.closeLot(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER, SAMPLE_LOT_ID, mockLotResult);
    verify(lotResultRepository).save(mockLotResult);
  }


  @Test(expected = AuctionNotFoundException.class)
  public void closeLotFailsWhenNoAuctionIsFound() {
    final AuctionSessionId auctionSessionId = new AuctionSessionId("PPL001", SAMPLE_SESSION_NUMBER);
    when(mockedAuctionSessionRepository.findOptionalById(auctionSessionId))
        .thenReturn(Optional.ofNullable(null));
    underTest.closeLot("PPL001", SAMPLE_SESSION_NUMBER, SAMPLE_LOT_ID, mockLotResult);
  }

  @Test(expected = LotClosureException.class)
  public void closeLotFailsWhenAuctionIsClosed() {
    final String auctionId = "PPL002";
    final int sessionNumber = 1;
    AuctionSessionId auctionSessionId = new AuctionSessionId(auctionId, sessionNumber);
    AuctionSession auctionSession = createAuctionMock(auctionId, sessionNumber);
    auctionSession.setState(AuctionSessionState.CLOSED);
    when(mockedAuctionSessionRepository.findOptionalById(auctionSessionId))
        .thenReturn(Optional.of(auctionSession));
    underTest.closeLot(auctionId, sessionNumber, SAMPLE_LOT_ID, mockLotResult);

  }

  private AuctionSession createAuctionMock(String auctionId, int sessionId) {
    return new AuctionSession(auctionId, sessionId);
  }



}
