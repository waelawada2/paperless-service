package com.sothebys.paperless.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import com.sothebys.paperless.client.SAPClient;
import com.sothebys.paperless.configuration.toggle.ToggleRouter;
import com.sothebys.paperless.domain.AuctionCard;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.domain.integration.BidLots;
import com.sothebys.paperless.exceptions.AuctionNotFoundException;
import com.sothebys.paperless.exceptions.AuctionSessionStateUpdateException;
import com.sothebys.paperless.persistence.entity.Auction;
import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.AuctionSessionId;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import com.sothebys.paperless.persistence.entity.Lot;
import com.sothebys.paperless.persistence.entity.LotExecution;
import com.sothebys.paperless.persistence.entity.LotExecutionId;
import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.repository.AuctionRepository;
import com.sothebys.paperless.persistence.repository.AuctionSessionRepository;
import com.sothebys.paperless.persistence.repository.LotExecutionRepository;
import com.sothebys.paperless.persistence.repository.LotRepository;
import com.sothebys.paperless.persistence.repository.LotResultRepository;
import com.sothebys.paperless.util.PaddedSourceNumberFormatter;
import com.sothebys.paperless.util.ZonedDateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class AuctionServiceImplTest {

  private static final String SAMPLE_LOT_ID = "19";
  private static final String SAMPLE_AUCTION_ID = "L0180";
  private static final Integer SAMPLE_SESSION_NUMBER = 2;
  private static final User SAMPLE_USER =
      new User("Name Lastname", "name.lastname@sothebys.com", "name.lastname");

  @Mock
  private AuctionRepository auctionRepository;
  @Mock
  private AuctionSessionRepository auctionSessionRepository;
  @Mock
  private LocalStateManager localStateManager;
  @Mock
  private LotExecutionRepository lotExecutionRepository;
  @Mock
  private LotRepository lotRepository;
  @Mock
  private LotResultRepository lotResultRepository;
  @Mock
  private PaddedSourceNumberFormatter amountFormatter;
  @Mock
  private SAPClient sapClient;
  @Mock
  private ToggleRouter featureToggleRouter;
  @Mock
  private ZonedDateTimeFormatter auctionCardZonedDateTime;

  @InjectMocks
  private AuctionServiceImpl underTest;

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  private AuctionSession auctionSession;
  private Optional<Auction> mockOptionalAuction;

  @Before
  @SuppressWarnings("unchecked")
  public void setup() {
    when(auctionRepository.findOptionalById(SAMPLE_AUCTION_ID)).thenReturn(getAuction());
    when(lotRepository.findAllByIdIn((Set<LotId>) any(Set.class))).thenReturn(getLots());
    Optional<LotExecution> mockOptionalLotExecution = Optional.of(new LotExecution());
    when(lotExecutionRepository.findOptionalById(any(LotExecutionId.class)))
        .thenReturn(mockOptionalLotExecution);
    mockOptionalAuction = getAuction();
  }

  @Test
  public void getAuctionsByAgent_succeeds() {
    underTest.getAuctionCardsForAgent(SAMPLE_USER);
  }

  @Test
  public void getLotsByAgent_succeeds() {

    // When
    BidLots response =
        underTest.getLotsByAgent(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER, SAMPLE_USER);

    // Then
    assertNotNull(response);
    assertFalse(response.getLots().isEmpty());
    assertEquals(1, response.getLots().size());

  }

  @Test
  public void getLotsByAgent_fails_auctionNotFound() {
    mockOptionalAuction = Optional.ofNullable(null);
    when(auctionRepository.findOptionalById(anyString())).thenReturn(mockOptionalAuction);
    expectedException.expect(AuctionNotFoundException.class);
    underTest.getLotsByAgent(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER, SAMPLE_USER);
  }

  @Test(expected = AuctionSessionStateUpdateException.class)
  public void itThrowIfNotAuctionSessionIdIsFound() {

    when(auctionSessionRepository
        .findOne(new AuctionSessionId(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER))).thenReturn(null);
    underTest.closeAuctionSession(new AuctionCard(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER),
        SAMPLE_USER);
  }

  private Optional<Auction> getAuction() {
    Auction auction = new Auction();
    auction.setAuctionSessions(
        Arrays.asList(new AuctionSession(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER)));
    return Optional.of(auction);
  }

  private List<Lot> getLots() {
    Lot lot = new Lot();
    lot.setId(new LotId(SAMPLE_LOT_ID, SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER));
    auctionSession = new AuctionSession();
    auctionSession.setState(AuctionSessionState.SCHEDULED);
    lot.setAuctionSession(auctionSession);
    return Arrays.asList(lot);
  }

}
