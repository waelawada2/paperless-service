package com.sothebys.paperless.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import com.sothebys.paperless.domain.BidClient;
import com.sothebys.paperless.domain.LotExecutionResult;
import com.sothebys.paperless.domain.LotResult;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.AuctionSessionId;
import com.sothebys.paperless.persistence.entity.Lot;
import com.sothebys.paperless.persistence.entity.LotExecution;
import com.sothebys.paperless.persistence.entity.LotExecutionId;
import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.repository.AuctionSessionRepository;
import com.sothebys.paperless.persistence.repository.LotExecutionRepository;
import com.sothebys.paperless.persistence.repository.LotResultRepository;
import java.util.Optional;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class LotExecutionServiceImplTest {

  private static final String SAMPLE_LOT_ID = "19";
  private static final String SAMPLE_CLIENT_NUMBER = "432";
  private static final Integer SAMPLE_CLIENT_LEVEL = 4;
  private static final Long SAMPLE_AMOUNT = 345000L;
  private static final String SAMPLE_PADDLE = "P0193";
  private static final User SAMPLE_USER =
      new User("Name Lastname", "name.lastname@sothebys.com", "name.lastname");

  @Mock
  private LotExecutionRepository mockLotExecutionRepo;
  @Mock
  private LotResultRepository lotResultRepository;
  @Mock
  private AuctionSessionRepository auctionSessionRepository;
  @Mock
  private LocalStateManager stateManager;

  @InjectMocks
  private LotExecutionServiceImpl underTest;

  @Rule
  public ExpectedException expectedLotException = ExpectedException.none();

  private LotResult lotExecutionResult;
  private String auctionId;
  private String lotId;
  private Integer sessionNumber;

  private Lot mockLot;
  private LotExecution mockLotExecution;
  private LotExecutionId mockLotExecutionId;

  @Before
  public void setUp() {
    BidClient bidClient = new BidClient();
    bidClient.setClientLevel(SAMPLE_CLIENT_LEVEL);
    bidClient.setClientNumber(SAMPLE_CLIENT_NUMBER);
    bidClient.setPaddle(SAMPLE_PADDLE);
    lotExecutionResult = new LotResult(bidClient, LotExecutionResult.WON, SAMPLE_AMOUNT);

    mockLot = mock(Lot.class);
    LotId mockEmbeddedLotId = mock(LotId.class);
    when(mockLot.getId()).thenReturn(mockEmbeddedLotId);
    when(mockEmbeddedLotId.getLotId()).thenReturn(SAMPLE_LOT_ID);

    sessionNumber = 1;
    auctionId = "PF1098";
    lotId = "506";
    mockLotExecutionId =
        new LotExecutionId(SAMPLE_USER.getUsername(), lotId, auctionId, sessionNumber);
    mockLotExecution = new LotExecution(mockLotExecutionId, SAMPLE_CLIENT_NUMBER, SAMPLE_AMOUNT,
        LotExecutionResult.WON, SAMPLE_PADDLE);
    when(mockLotExecutionRepo.save(any(LotExecution.class))).thenReturn(mockLotExecution);
    Optional<AuctionSession> auctionSession = Optional.of(new AuctionSession());
    when(auctionSessionRepository.findOptionalById(any(AuctionSessionId.class)))
        .thenReturn(auctionSession);
  }

  @Test
  public void testLotExecution() {

    when(mockLotExecutionRepo.findOptionalById(mockLotExecutionId))
        .thenReturn(Optional.ofNullable(null));
    underTest.confirmLotExecution(lotExecutionResult, auctionId, sessionNumber, lotId, SAMPLE_USER);

    verify(mockLotExecutionRepo).save(mockLotExecution);
  }

}
