package com.sothebys.paperless.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import com.sothebys.paperless.domain.LotClosureResult;
import com.sothebys.paperless.persistence.entity.Auction;
import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.AuctionSessionId;
import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.entity.LotResult;
import com.sothebys.paperless.persistence.repository.AuctionSessionRepository;
import com.sothebys.paperless.persistence.repository.LotResultRepository;
import com.sothebys.paperless.util.ZonedDateTimeFormatter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ReportServiceImplTest {

  private static final String NEW_YORK = "New York";
  private static final String CLOSE_DATE = "2018-04-30 11:30";
  private String SAMPLE_AUCTION_ID = "L18228";
  private int SAMPLE_SESSION_NUMBER = 1;

  @Mock
  private LotResultRepository mockLotResultRepo;

  @Mock
  private AuctionSessionRepository mockAuctionSessionRepo;

  @Mock
  private ZonedDateTimeFormatter zonedDateTimeFormatter;

  @InjectMocks
  private ReportServiceImpl sut;

  private AuctionSession mockAuctionSession =
      new AuctionSession(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER);
  private Auction mockAuction = new Auction();

  private List<LotResult> lotResults = new ArrayList<LotResult>();

  private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

  @Before
  public void setup() throws ParseException {
    mockAuction.setTotalSessions(1);
    mockAuction.setCurrency("USD");
    mockAuction.setLocation(NEW_YORK);
    mockAuctionSession.setAuction(mockAuction);
    df.setTimeZone(TimeZone.getTimeZone("GMT"));
    Date closeDate = df.parse(CLOSE_DATE);
    mockAuctionSession.setCloseDate(closeDate);
    when(mockAuctionSessionRepo
        .findOptionalById(new AuctionSessionId(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER)))
            .thenReturn(Optional.of(mockAuctionSession));
    when(mockLotResultRepo.findById_AuctionIdAndId_SessionId(SAMPLE_AUCTION_ID,
        SAMPLE_SESSION_NUMBER)).thenReturn(lotResults);
    ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(closeDate.getTime()), ZoneId.of("America/New_York"));
    when(zonedDateTimeFormatter.of(closeDate.getTime(), NEW_YORK)).thenReturn(zonedDateTime);
    
  }

  @Test
  public void gets_report_filename_1_session() throws Exception {
    // When
    String result = sut.getReportFileName(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER);
    // Then
    assertThat(result).isEqualTo("L18228_30_APRIL_2018_07:30_AM.CSV");
  }

  @Test
  public void gets_report_filename_with_session_number_for_multiple_sessions() throws Exception {
    // Given
    mockAuction.setTotalSessions(2);
    
    // When
    String result = sut.getReportFileName(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER);
    // Then
    assertThat(result).isEqualTo("L18228_1_30_APRIL_2018_07:30_AM.CSV");
  }

  @Test
  public void generates_csv_report() {
    // Given
    lotResults.add(getLotResultForLot("1"));
    lotResults.add(getLotResultForLot("2"));
    lotResults.add(getLotResultForLot("10"));

    // When
    String result = sut.getCSVReport(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER);

    // Then
    assertThat(result).isEqualTo(
        "Lot Number, Hammer Price (USD), Status, Paddle Number, Auctioneer\n"+
        "1,50000,SLD,\tL1,auctioneer 1\n"+
        "2,50000,SLD,\tL2,auctioneer 2\n"+
        "10,50000,SLD,\tL10,auctioneer 10\n");
  }

  private LotResult getLotResultForLot(String lotId) {
    LotResult lotResult = new LotResult();
    lotResult.setAuctioneer("auctioneer " + lotId);
    lotResult.setHammerPrice(50000L);
    lotResult.setId(new LotId(lotId, SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER));
    lotResult.setPaddleNumber("L"+lotId);
    lotResult.setStatus(LotClosureResult.SLD);
    return lotResult;
  }
}
