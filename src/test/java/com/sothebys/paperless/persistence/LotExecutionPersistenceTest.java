package com.sothebys.paperless.persistence;

import static org.junit.Assert.assertEquals;
import com.sothebys.paperless.domain.LotExecutionResult;
import com.sothebys.paperless.persistence.entity.Auction;
import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.Lot;
import com.sothebys.paperless.persistence.entity.LotExecution;
import com.sothebys.paperless.persistence.entity.LotExecutionId;
import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.repository.AuctionRepository;
import com.sothebys.paperless.persistence.repository.LotExecutionRepository;
import com.sothebys.paperless.persistence.repository.LotRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class LotExecutionPersistenceTest {

  @Resource
  private LotExecutionRepository lotExecRepo;

  @Resource
  private AuctionRepository auctionRepo;

  @Resource
  private LotRepository lotRepo;

  private static final String SAMPLE_AUCTION_ID = "L0180";
  private static final Integer SAMPLE_SESSION_NUMBER = 1;
  private static final String SAMPLE_LOT_ID = "19";
  private static final String SAMPLE_CLIENT_ID = "client.id";
  private static final String SAMPLE_AGENT_ID = "agent.id";
  private static final Long SAMPLE_AMOUNT = 345000L;
  private static final String SAMPLE_PADDLE = "P0193";

  private Auction createdAuction;
  private Lot createdLot;

  @Before
  public void setUp() {
    // inserting an auction
    AuctionSession auctionSession = new AuctionSession(SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER);
    List<AuctionSession> auctionSessions = new ArrayList<AuctionSession>(1);
    auctionSessions.add(auctionSession);
    Auction auction =
        new Auction("P20180", new Date(), "Paris", "Auction", 1, 1, "USD", auctionSessions);
    createdAuction = auctionRepo.save(auction);

    // inserting a lot for that auction
    Lot lot = new Lot();
    LotId lotId = new LotId(SAMPLE_LOT_ID, SAMPLE_AUCTION_ID, SAMPLE_SESSION_NUMBER);
    lot.setId(lotId);
    createdLot = lotRepo.save(lot);
  }

  @Test
  public void testSaveAndFindOne() {
    LotExecution confirmed = new LotExecution();
    LotExecutionId id = new LotExecutionId(SAMPLE_AGENT_ID, createdLot.getId().getLotId(),
        createdAuction.getId(), SAMPLE_SESSION_NUMBER);
    confirmed.setId(id);
    confirmed.setClientId(SAMPLE_CLIENT_ID);
    confirmed.setExecutionAmount(SAMPLE_AMOUNT);
    confirmed.setExecutionResult(LotExecutionResult.WON);
    confirmed.setPaddleNumber(SAMPLE_PADDLE);

    LotExecution saved = lotExecRepo.save(confirmed);

    LotExecution retrieved = lotExecRepo.findOptionalById(saved.getId()).get();

    assertEquals(saved.getPaddleNumber(), retrieved.getPaddleNumber());
  }

  @Test(expected = DataIntegrityViolationException.class)
  public void testSaveFailsIfPreconditionsFail() {
    // remove precondition
    lotRepo.delete(createdLot);

    LotExecution confirmed = new LotExecution();
    LotExecutionId id = new LotExecutionId(SAMPLE_AGENT_ID, SAMPLE_LOT_ID, SAMPLE_AUCTION_ID,
        SAMPLE_SESSION_NUMBER);
    confirmed.setId(id);
    confirmed.setClientId(SAMPLE_CLIENT_ID);
    confirmed.setExecutionAmount(SAMPLE_AMOUNT);
    confirmed.setPaddleNumber(SAMPLE_PADDLE);

    lotExecRepo.save(confirmed);

  }

}
