package com.sothebys.paperless;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import com.jayway.jsonpath.JsonPath;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Web integration tests for the application's actuator endpoints.
 */
@ActiveProfiles("test")
//@EnableAutoConfiguration(exclude = {org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class,
//    org.springframework.boot.autoconfigure.security.SecurityFilterAutoConfiguration.class,
//    org.springframework.boot.autoconfigure.security.FallbackWebSecurityAutoConfiguration.class,
//    org.springframework.boot.autoconfigure.security.oauth2.OAuth2AutoConfiguration.class})
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@TestPropertySource(properties = {"security.basic.enabled=false", "management.security.enabled=false"})
public class PaperlessServiceActuatorWebIntegrationTest {

  @Autowired
  private TestRestTemplate restTemplate;

  @Value("${security.user.name}")
  private String username;

  @Value("${security.user.password}")
  private String password;

  @Value("${management.contextPath:/management}")
  private String managementContextPath;

  /**
   * Tests the actuator health endpoint without details.
   */
  @Test
  public void testHealthEndpoint() throws Exception {
    ResponseEntity<String> responseEntity =
        this.restTemplate.getForEntity(managementContextPath + "/health", String.class);
    assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    String body = responseEntity.getBody();
    assertThat(JsonPath.parse(body).read("$.status"), is(Status.UP.toString()));
  }

  /**
   * Tests the actuator health endpoint with details (authorization)
   */
  @Test
  @Ignore("requires headers and auctuator configuration")
  public void testHealthEndpointWithDetails() throws Exception {
//    HttpHeaders headers = new HttpHeaders();
//    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//    headers.add("X-Authorization", "Bearer " + loginResponse.get("token").textValue());
//    headers.add("Content-Type", "application/json");
    ResponseEntity<String> responseEntity = this.restTemplate.withBasicAuth(username, password)
        .getForEntity(managementContextPath + "/health", String.class);
    assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    String body = responseEntity.getBody();
    assertThat(JsonPath.parse(body).read("$.status"), is(Status.UP.toString()));
    assertThat(JsonPath.parse(body).read("$.diskSpace.status"), is(Status.UP.toString()));
  }

  /**
   * Tests the actuator info endpoint
   */
  @Test
  public void testInfoEndpoint() throws Exception {
    ResponseEntity<String> responseEntity =
        this.restTemplate.getForEntity(managementContextPath + "/info", String.class);
    assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    String body = responseEntity.getBody();
    assertThat(JsonPath.parse(body).read("$.build.artifact"), is("paperless-service"));
  }

}
