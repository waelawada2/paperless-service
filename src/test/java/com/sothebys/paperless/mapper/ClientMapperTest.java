package com.sothebys.paperless.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import com.sothebys.paperless.domain.BidClient;
import com.sothebys.paperless.domain.clientapi.ClientApiData;
import org.junit.Test;

public class ClientMapperTest {

  @Test
  public void testClientMapper() {
    // Given
    ClientApiData clientApiData = getClientApiData();
    // When
    BidClient result = ClientMapper.INSTANCE.clientApiDataToBidClient(clientApiData);
    // Then
    assertThat(result).isEqualTo(getExpectedResult());
  }

  private ClientApiData getClientApiData() {
    return new ClientApiData(123L, "Party Name", "email@sothebys.com", 7, "Individual",
        "countryConnection", "kcm Name", "kcm@sothebys.com", "Primary Contact Name");
  }

  private BidClient getExpectedResult() {
    return new BidClient(null, 7, null, "Party Name", "kcm Name", "INDIVIDUAL", null,
        "Primary Contact Name", null, null, "email@sothebys.com", null, null, null);
  }
}
