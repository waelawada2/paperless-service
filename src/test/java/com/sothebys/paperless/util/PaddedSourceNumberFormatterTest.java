package com.sothebys.paperless.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import com.sothebys.paperless.configuration.ApiConfiguration;
import com.sothebys.paperless.configuration.SapApiProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class PaddedSourceNumberFormatterTest {
  
  private final static String SAMPLE_PADDED_NUMBER_1500 = "      1,500";
  private final static Long SAMPLE_LONG_NUMBER_1500 = 1500L;
  private final static String SAMPLE_GARBAGE = "THINGS THAT SHOULD NOT BE";
  private final static String SAMPLE_FORMAT = "#,###";
  
  @Mock
  private SapApiProperties sapProperties;
  
  @InjectMocks
  private PaddedSourceNumberFormatter underTest;
  
  @Before
  public void setUp() {
    ApiConfiguration mockConfiguration = mock(ApiConfiguration.class);
    when(sapProperties.getConfiguration()).thenReturn(mockConfiguration);
    when(mockConfiguration.getAmountsFormat()).thenReturn(SAMPLE_FORMAT);
  }

  @Test
  public void parsePaddedString() {
    Long actualValue = underTest.parseAmount(SAMPLE_PADDED_NUMBER_1500);
    assertEquals(SAMPLE_LONG_NUMBER_1500, actualValue);
  }
  
  @Test
  public void parseGarbageStringFails_ReturnsNull() {
    Long actualValue = underTest.parseAmount(SAMPLE_GARBAGE);
    assertNull(actualValue);
  }

}
