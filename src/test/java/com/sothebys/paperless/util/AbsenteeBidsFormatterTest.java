package com.sothebys.paperless.util;

import static org.assertj.core.api.Assertions.assertThat;
import com.sothebys.paperless.domain.AbsenteeBid;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class AbsenteeBidsFormatterTest {

  private AbsenteeBidsFormatter sut;
  
  private List<AbsenteeBid> absenteeBids;
  
  private AbsenteeBid absenteeBid1 = new AbsenteeBid("1", "L1", 2000L, "20180422162120", false);
  private AbsenteeBid absenteeBid2 = new AbsenteeBid("2", "L2", 8000L, "20180422172222", false);
  private AbsenteeBid absenteeBid3 = new AbsenteeBid("3", "L3", 7000L, "20180422182422", false);
  private AbsenteeBid absenteeBid4 = new AbsenteeBid("4", "L4", 7000L, "20180423192522", false);
  private AbsenteeBid absenteeBid5 = new AbsenteeBid("5", "L5", 2000L, "20180423202622", false);
  private AbsenteeBid absenteeBid6 = new AbsenteeBid("6", "L6", 2000L, "20180423202722", false);
  private AbsenteeBid absenteeBid7 = new AbsenteeBid("7", "L7", 5000L, "20180424202723", false);
  private AbsenteeBid absenteeBid8 = new AbsenteeBid("8", "L8", 5000L, "20180424210822", false);
  
  @Before
  public void setup() {
    absenteeBids = Arrays.asList(absenteeBid1, absenteeBid2, absenteeBid3, absenteeBid4, absenteeBid5, absenteeBid6, absenteeBid7, absenteeBid8);
    sut = new AbsenteeBidsFormatter();
  }
  
  @Test
  public void itDisplaysOnlyTheThreeHigherBids() {
    // Given
    Long[] reservePrices = new Long[]{ 2000L, 6000L, 7000L };
    Integer[] expectedSize = new Integer[]{ 6, 6, 6 };
    
    for(int i = 0; i < reservePrices.length; i++) {
      // When
      List<AbsenteeBid> result = sut.formatAbsenteeBidList(reservePrices[i], absenteeBids);
      
      // Then
      assertThat(result.size()).as("Size for reserve %d", reservePrices[i]).isEqualTo(expectedSize[i]);      
    }
  }
  
  @Test
  public void bidsEqualOrHigherThanReserveGoFirst() {
    //Given
    absenteeBids = Arrays.asList(absenteeBid2, absenteeBid4, absenteeBid7, absenteeBid8);
    Long reservePrice = 5000L;
    
    //When
    List<AbsenteeBid> result = sut.formatAbsenteeBidList(reservePrice, absenteeBids);
    
    //Then
    AbsenteeBid reserve = new AbsenteeBid(null, "Reserve", reservePrice, null, true);
    assertThat(result).containsExactly(absenteeBid2, absenteeBid4, absenteeBid7, absenteeBid8, reserve);
  }

  @Test
  public void bidsLowerThanReserveGoLast() {
    //Given
    absenteeBids = Arrays.asList(absenteeBid2, absenteeBid4, absenteeBid7, absenteeBid8);
    Long reservePrice = 7000L;
    
    //When
    List<AbsenteeBid> result = sut.formatAbsenteeBidList(reservePrice, absenteeBids);
    
    //Then
    AbsenteeBid reserve = new AbsenteeBid(null, "Reserve", reservePrice, null, true);
    assertThat(result).containsExactly(absenteeBid2, absenteeBid4, reserve, absenteeBid7, absenteeBid8);
  }
  
  @Test
  public void bidsWithEqualAmountOrderedByRegistrationDate() {
    //Given
    absenteeBids = Arrays.asList(absenteeBid7, absenteeBid8, absenteeBid5, absenteeBid1);
    Long reservePrice = 2000L;
    
    //When
    List<AbsenteeBid> result = sut.formatAbsenteeBidList(reservePrice, absenteeBids);
    
    //Then
    AbsenteeBid reserve = new AbsenteeBid(null, "Reserve", reservePrice, null, true);
    assertThat(result).containsExactly(absenteeBid7, absenteeBid8, absenteeBid1, absenteeBid5, reserve);
  }
  

}
