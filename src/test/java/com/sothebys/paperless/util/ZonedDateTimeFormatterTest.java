package com.sothebys.paperless.util;

import static org.junit.Assert.*;
import java.time.ZonedDateTime;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@Slf4j
public class ZonedDateTimeFormatterTest {

  // GMT: Friday, March 16, 2018 2:00:00 PM
  private static final long TIMESTAMP = 1521208800000L;

  // GMT: Friday, March 16, 2018 00:00:00 AM
  private static final long ZERO_HOUR_TIMESTAMP = 1521172800000L;

  // GMT: Friday, March 16, 2018 6:01:00 PM
  private static final long FOUR_HOURS_AFTER_TIMESTAMP = 1521223260000L;

  @Autowired
  private ZonedDateTimeFormatter formatter;

  @Test
  public void itTruncatesToZeroHour() {
    // Given
    String location = "New York";

    ZonedDateTime zonedDateTime = formatter.of(TIMESTAMP, location);
    long truncated = formatter.toWindowStart(zonedDateTime);
    ZonedDateTime zeroHourZonedDateTime = formatter.of(ZERO_HOUR_TIMESTAMP, location);
    assertEquals(ZERO_HOUR_TIMESTAMP, truncated);
    assertEquals(ZERO_HOUR_TIMESTAMP, zeroHourZonedDateTime.toInstant().toEpochMilli());
  }

  @Test
  public void itAddsFourHoursAndOneMinute() {
    String location = "New York";

    ZonedDateTime zonedDateTime = formatter.of(TIMESTAMP, location);
    long fourHoursAfter = formatter.toWindowEnd(zonedDateTime);
    ZonedDateTime fiveMinutesAfterZonedDateTime =
        formatter.of(FOUR_HOURS_AFTER_TIMESTAMP, location);

    assertEquals(FOUR_HOURS_AFTER_TIMESTAMP, fourHoursAfter);
    assertEquals(FOUR_HOURS_AFTER_TIMESTAMP,
        fiveMinutesAfterZonedDateTime.toInstant().toEpochMilli());
  }

  @Test
  public void itValidatesAuctionSessionOpenWithinWindow() {
    String location = "New York";
    ZonedDateTime zonedDateTime = formatter.of(TIMESTAMP, location);

    // checking original date is within time window: 0-hour as start, plus five minutes as end
    boolean valid = formatter.isWithinWindow(zonedDateTime.toInstant().toEpochMilli(),
        ZERO_HOUR_TIMESTAMP, FOUR_HOURS_AFTER_TIMESTAMP);
    assertTrue(valid);

    // checking 0-hour date is outside time window: original date as start, plus-five-minutes date
    // as end
    valid = formatter.isWithinWindow(ZERO_HOUR_TIMESTAMP, zonedDateTime.toInstant().toEpochMilli(),
        FOUR_HOURS_AFTER_TIMESTAMP);
    assertFalse(valid);
  }

  private ZonedDateTime toZDT(long time) {
    return formatter.of(time, "New York");
  }

  @Test
  public void sampleDates() {
    String template = "TIMESTAMP=%s, ZERO_HOUR_TIMESTAMP=%s, FIVE_MINUTES_AFTER_TIMESTAMP=%s";
    long check = 1523397610000L;
    long start = 1523397600000L;
    long end = 1523435700689L;
    String formatted = String.format(template, check, start, end);
    log.debug(formatted);
    formatted = String.format(template, toZDT(check), toZDT(start), toZDT(end));
    log.debug(formatted);
  }

  @Test
  public void itFormatsNewYorkDateTime() {
    // Given
    String location = "New York";

    // When
    ZonedDateTime zonedTimeZone = formatter.of(TIMESTAMP, location);
    String time = formatter.formatCardTime(zonedTimeZone);
    String date = formatter.formatCardDate(zonedTimeZone);

    // Then
    assertEquals("16 March 2018", date);
    assertEquals("10:00 AM EDT", time);

  }

  @Test
  public void itFormatsLondonDateTime() {
    // Given
    String location = "London";

    // When
    ZonedDateTime zonedTimeZone = formatter.of(TIMESTAMP, location);
    String time = formatter.formatCardTime(zonedTimeZone);
    String date = formatter.formatCardDate(zonedTimeZone);

    // Then
    assertEquals("16 March 2018", date);
    assertEquals("02:00 PM GMT", time);

  }

  @Test
  public void itFormatsHongKongDateTime() {
    // Given
    String location = "Hong Kong";

    // When
    ZonedDateTime zonedTimeZone = formatter.of(TIMESTAMP, location);
    String time = formatter.formatCardTime(zonedTimeZone);
    String date = formatter.formatCardDate(zonedTimeZone);

    // Then
    assertEquals("16 March 2018", date);
    assertEquals("10:00 PM HKT", time);

  }

  @Test
  public void itFormatsParisDateTime() {
    // Given
    String location = "Paris";

    // When
    ZonedDateTime zonedTimeZone = formatter.of(TIMESTAMP, location);
    String time = formatter.formatCardTime(zonedTimeZone);
    String date = formatter.formatCardDate(zonedTimeZone);

    // Then
    assertEquals("16 March 2018", date);
    assertEquals("03:00 PM CET", time);

  }

  // Geneva, Milan & Doha
  @Test
  public void itFormatsConfigurableTimezones() {
    // Given
    String location = "Geneva";

    // When
    ZonedDateTime zonedTimeZone = formatter.of(TIMESTAMP, location);
    String time = formatter.formatCardTime(zonedTimeZone);
    String date = formatter.formatCardDate(zonedTimeZone);

    // Then
    assertEquals("16 March 2018", date);
    assertEquals("03:00 PM CET", time);

  }

}
