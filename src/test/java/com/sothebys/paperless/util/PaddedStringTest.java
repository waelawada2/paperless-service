package com.sothebys.paperless.util;

import static org.junit.Assert.*;
import org.junit.Test;

public class PaddedStringTest {
  private static final String PADDLE_WITH_L = "L10";
  private static final String PADDLE = "20";
  private static final String WRONG_PADDLE = "L0L10";

  private static final String UNPADDED_ACCOUNT_NUMBER = "59607";
  private static final String PADDED_ACCOUNT_NUMBER = "00059607";

  @Test
  public void testRightPaddleNumberWithL() {
    PaddedString paddedString = new PaddedString(PADDLE_WITH_L);
    assertTrue(paddedString.toString().equals("L0010"));
  }

  @Test
  public void testRightPaddleNumber() {
    assertTrue(new PaddedString(PADDLE).toString().equals("20"));
  }

  @Test
  public void testWithoutPadding() {
    final String paddleWithL = "L2030";
    final String paddle = "20301";
    assertTrue(new PaddedString(paddleWithL).toString().equals(paddleWithL));
    assertTrue(new PaddedString(paddle).toString().equals(paddle));
  }

  @Test
  public void testWrongPaddleNumberFormat() {
    assertTrue(new PaddedString(WRONG_PADDLE).toString().equals(WRONG_PADDLE));
  }

  @Test
  public void testPaddingAccountNumber() {
    assertEquals(PADDED_ACCOUNT_NUMBER,
        new PaddedString(UNPADDED_ACCOUNT_NUMBER).asClient().toString());
  }

}
