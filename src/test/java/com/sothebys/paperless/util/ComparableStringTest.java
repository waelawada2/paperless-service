package com.sothebys.paperless.util;

import static org.junit.Assert.*;
import org.junit.Test;

public class ComparableStringTest {

  @Test
  public void testZero() {
    assertEquals(0, new ComparableString(null).compareTo(null));
    assertEquals(0, new ComparableString("").compareTo(""));
    assertEquals(0, new ComparableString("10").compareTo("10"));
    assertEquals(0, new ComparableString("45a").compareTo("45A"));
  }

  @Test
  public void testNegative() {
    assertTrue(new ComparableString("").compareTo("0") < 0);
    assertTrue(new ComparableString(null).compareTo("1") < 0);
    assertTrue(new ComparableString("9").compareTo("10") < 0);
    assertTrue(new ComparableString("005").compareTo("10") < 0);
    assertTrue(new ComparableString("709").compareTo("709H") < 0);
    assertTrue(new ComparableString("rise").compareTo("sire") < 0);
  }

  @Test
  public void testPositive() {
    assertTrue(new ComparableString("32").compareTo("30") > 0);
    assertTrue(new ComparableString("1Z").compareTo("1") > 0);
    assertTrue(new ComparableString("110P").compareTo("101P") > 0);
    assertTrue(new ComparableString("1234AB").compareTo("1234W") > 0);
  }
  
  @Test
  public void testWithinRange() {
    assertTrue(new ComparableString("11").withinRange("10", "900"));
    assertTrue(new ComparableString("11").withinRange("11", "12"));
    assertTrue(new ComparableString("11").withinRange("11", "11"));
  }
  
  @Test
  public void testOutsideRange() {
    assertTrue(new ComparableString("11").outsideRange("50", "900"));
    assertTrue(new ComparableString("11").outsideRange("12", "900"));
    assertTrue(new ComparableString("901").outsideRange("12", "900"));
  }

}
