-- Clean data

delete from paperless_service.client_lot where client_id in ('1', '2', '3');
delete from paperless_service.agent_assigned_lot where agent_id in (101, 102);
delete from paperless_service.auction_client where client_id in ('1', '2', '3');
delete from paperless_service.client where client_id in ('1', '2', '3');
delete from paperless_service.agent where agent_id in (101, 102);

-- Insert agent
INSERT INTO paperless_service.agent
(agent_id, email, name, username)
VALUES(101, 'grace.londono@sothebys.com', 'Grace Londono', 'grace.londono');

INSERT INTO paperless_service.agent
(agent_id, email, name, username)
VALUES(102, 'alejandra.rossetti@sothebys.com', 'Alejandra Rossetti', 'alejandra.rossetti');


-- Insert clients

INSERT INTO paperless_service.client
(client_id, account_number, client_level, client_number, email, primary_contact_name, kcm, language_preference, other_phone, party_name, party_type, preferred_phone)
VALUES('1', '1', 1, 1, 'client.1@sothebys.com', 'Client Number One', 'Kcm for client one', 'en', null, 'Client Number One', 'INDIVIDUAL', '3101111111');

INSERT INTO paperless_service.client
(client_id, account_number, client_level, client_number, email, primary_contact_name, kcm, language_preference, other_phone, party_name, party_type, preferred_phone)
VALUES('2', '2', 1, 2, 'client.2@sothebys.com', 'Client Number Two', 'Kcm for client two', 'en', null, 'Client Number Two', 'COMPANY', '3102222222');

INSERT INTO paperless_service.client
(client_id, account_number, client_level, client_number, email, primary_contact_name, kcm, language_preference, other_phone, party_name, party_type, preferred_phone)
VALUES('3', '3', 1, 3, 'client.3@sothebys.com', 'Client Number Three', 'Kcm for client three', 'en', null, 'Client Number Three', 'JOINT', '3103333333');

--Dev Data (update phone numbers and preferred_contact_name)
UPDATE paperless_service.client
SET primary_contact_name='Contact For Joshua Mann', preferred_phone='+86 135 8588 6526'
WHERE client_id='06091441';
UPDATE paperless_service.client
SET primary_contact_name='Contact For A. Bernard Estate', preferred_phone='505-347-5208'
WHERE client_id='30066303';
UPDATE paperless_service.client
SET preferred_phone='707-663-6623'
WHERE client_id='35006737';
UPDATE paperless_service.client
SET primary_contact_name='Contact for Garry Fords', preferred_phone='+49 181 45 10215'
WHERE client_id='51063395';


-- Auciton with lots from 1-10 in session 1

SET @auctionId = 'L18100';

-- Assign paddles to clients 

INSERT INTO paperless_service.auction_client
(auction_id, client_id, paddle_number)
VALUES(@auctionId, '1', 'L00001');

INSERT INTO paperless_service.auction_client
(auction_id, client_id, paddle_number)
VALUES(@auctionId, '2', 'L00002');

INSERT INTO paperless_service.auction_client
(auction_id, client_id, paddle_number)
VALUES(@auctionId, '3', 'L00003');

-- Dev Data
INSERT INTO paperless_service.auction_client
(auction_id, client_id, paddle_number)
VALUES(@auctionId, '35006737', 'L00004');
INSERT INTO paperless_service.auction_client
(auction_id, client_id, paddle_number)
VALUES(@auctionId, '06091441', 'L00005');
INSERT INTO paperless_service.auction_client
(auction_id, client_id, paddle_number)
VALUES(@auctionId, '30066303', 'L00006');
INSERT INTO paperless_service.auction_client
(auction_id, client_id, paddle_number)
VALUES(@auctionId, '51063395', 'L00007');

-- Assign lots to agent (phone bidders)

INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(102, '1', '1', @auctionId, 1);

INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(102, '2', '2', @auctionId, 1);

INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(102, '2', '5', @auctionId, 1);

INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(102, '1', '7', @auctionId, 1);

INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(101, '2', '1', @auctionId, 1);

-- Dev Data
INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(102, '35006737', '2', @auctionId, 1);
INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(102, '06091441', '3', @auctionId, 1);
INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(102, '30066303', '4', @auctionId, 1);
INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(102, '51063395', '5', @auctionId, 1);

-- Assign lots to client

INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('1', '1', @auctionId, 1, 10000, null, now());

INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('2', '1', @auctionId, 1, 20000, null, now());

INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('2', '2', @auctionId, 1, 0, 20000, now());

INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('2', '5', @auctionId, 1, 10000, 0, now());

INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('1', '7', @auctionId, 1, 10000, 0, now());

-- Dev Data
INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('35006737', '2', @auctionId, 1, 10000, null, now());
INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('06091441', '3', @auctionId, 1, null, null, now());
INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('30066303', '4', @auctionId, 1, null, null, now());
INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('51063395', '5', @auctionId, 1, null, null, now());


-- Insert absentee bids

INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('3', '1', @auctionId, 1, null, 30000, now());

commit;


-- Fix missing auctions
-- Auction sessions without lots (failed initialization)
SELECT auction_id FROM auction where auction_id not in (select auction_id from lot);

-- Remove failed auctions
delete from auction_session where auction_id in (
	SELECT auction_id FROM auction where auction_id not in (select auction_id from lot
));
delete from auction where auction_id not in (select auction_id from lot);