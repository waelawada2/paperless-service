INSERT INTO paperless_service.auction
(auction_id, `date`, location, name, total_sessions, total_lots)
VALUES('PP0002', '2018-05-13 03:30:01', 'New York', 'Sample Sale with Reserve Data for all lots', 1, 12);

INSERT INTO paperless_service.auction_session
(auction_id, session_id, state, start_date, start_lot, end_lot, open_time_window_start, open_time_window_end)
VALUES('PP0002', 1, 'SCHEDULED', '2018-05-13 03:30:01', '1', '12', 1526162400000, 1526200560843);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('1', 'GBP', 12000, 8000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/302LN17OMP_9LLFJ.jpg', 'GERMAN SCHOOL, 16TH CENTURY', 'Predella panel depicting a kneeling donor with his wife and their ten children', '/data/events/2018/old-masters-l18030/catalogue/lot_167712125', 'oil on panel, unframed', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('2', 'GBP', 20000, 15000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/100L18030_9SDZ3.jpg', 'ANTWERP SCHOOL, FIRST HALF OF THE 16TH CENTURY', 'Biblical scene with the Annunciation to the Shepherds beyond', '/data/events/2018/old-masters-l18030/catalogue/lot_167796142', 'oil on panel', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('3', 'GBP', 20000, 15000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/036L18030_9RWFZ.jpg', 'FOLLOWER OF ROGIER VAN DER WEYDEN', 'Virgin and Child', '/data/events/2018/old-masters-l18030/catalogue/lot_167785792', 'oil on oak panel', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('4', 'GBP', 7000, 5000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/051L18030_9RWG3.jpg', 'FOLLOWER OF JAN PROVOOST', 'Virgin and Child', '/data/events/2018/old-masters-l18030/catalogue/lot_167785793', 'oil on oak panel', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('5', 'GBP', 40000, 30000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/140PVTOMP_9J6NZ_uncropped.jpg', 'ADRIAEN ISENBRANT', 'The Stigmatisation of Saint Francis', '/data/events/2018/old-masters-l18030/catalogue/lot_167787878', 'oil on oak panel', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('6', 'GBP', 8000, 6000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/045L18030_9MFRD_in_frame.jpg', 'NETHERLANDISH SCHOOL, 16TH CENTURY', 'Five Scenes from The Passion', '/data/events/2018/old-masters-l18030/catalogue/lot_167722641', 'a set of five, oil on panel, circular', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('7', 'GBP', 120000, 80000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/073L18030_9S8GJ.jpg', 'ENGLISH SCHOOL, CIRCA 1600-1603', 'Portrait of Anne Russell, Lady Herbert, later Countess of Worcester (d. 1639), full-length, holding a dog and an ostrich-feather fan', '/data/events/2018/old-masters-l18030/catalogue/lot_167794273', 'oil on canvas', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('8', 'GBP', 60000, 40000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/096L18030_9S8GK.jpg', 'ATTRIBUTED TO FRANÇOIS QUESNEL', 'Portrait of a French noblewoman, half-length, wearing a ruff, pearls, a cross-shaped ribbon, and holding a fan', '/data/events/2018/old-masters-l18030/catalogue/lot_167794274', 'oil on its original canvas', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('9', 'GBP', 18000, 12000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/141L18030_9QTBW.jpg', 'DUTCH SCHOOL, CIRCA 1635', 'Portrait of a boy with a horse, a landscape beyond', '/data/events/2018/old-masters-l18030/catalogue/lot_167781091', 'oil on oak panel', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('10', 'GBP', 6000, 4000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/063L18030_9PTX9.jpg', 'CIRCLE OF SIR PETER LELY', 'Portrait of a lady', '/data/events/2018/old-masters-l18030/catalogue/lot_167748580', 'oil on canvas', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('11', 'GBP', 8000, 6000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/079L17037_9MFRL.jpg', 'FOLLOWER OF SIR ANTHONY VAN DYCK', 'An oil sketch of Princess Mary (1631-1660), Princess Royal and later Princess of Orange', '/data/events/2018/old-masters-l18030/catalogue/lot_167780468', 'oil on canvas', 0, 'PP0002', 1);

INSERT INTO paperless_service.lot
(lot_id, currency, estimate_high, estimate_low, executed, image_url, lot_author, lot_description, lot_path, medium, reserve_price, auction_id, session_id)
VALUES('12', 'GBP', 12000, 8000, '', 'https://www.sothebys.com/content/dam/stb/lots/L18/L18030/081L18030_9F765.jpg', 'JOHN MICHAEL WRIGHT AND STUDIO', 'Portrait of Heneage Finch, 1st Earl of Nottingham, Lord Chancellor, in Peer''s robes with the purse of the Privy Seal', '/data/events/2018/old-masters-l18030/catalogue/lot_167780374', 'oil on canvas', 0, 'PP0002', 1);











