-- agent
INSERT INTO paperless_service.agent
(agent_id, email, name)
VALUES(102, 'alejandra.rossetti@sothebys.com', 'Alejandra Rosetti');
-- client
INSERT INTO paperless_service.client
(client_id, account_number, client_level, client_number, email, full_name, kcm, language_preference, other_phone, party_name, party_type, preferred_phone)
VALUES('623236', '1234567', 9, 1, 'samaijbuistrijploviets@aol.com', 'Mr Aleksandr Popov', 'kcm', 'es', null, null, 'INDIVIDUAL', '+49 181 45 10215');
-- assigned lots
INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(102, '623236', '1', 'L18100', 1);
INSERT INTO paperless_service.agent_assigned_lot
(agent_id, client_id, lot_id, auction_id, session_id)
VALUES(102, '623236', '2', 'L18100', 1);
--client lots
INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('623236', '1', 'L18100', 1, 10000, 0, '');
INSERT INTO paperless_service.client_lot
(client_id, lot_id, auction_id, session_id, cover_bid, absentee_bid, registration_datetime)
VALUES('623236', '2', 'L18100', 1, 0, 20000, '');
-- auction clients
INSERT INTO paperless_service.auction_client
(auction_id, client_id, paddle_number)
VALUES('L18100', '623236', 'L0010');
