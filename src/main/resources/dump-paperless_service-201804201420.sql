-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: paperless_service
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `agent_id` bigint(10) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent_assigned_lot`
--

DROP TABLE IF EXISTS `agent_assigned_lot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_assigned_lot` (
  `agent_id` bigint(10) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `lot_id` varchar(10) NOT NULL,
  `auction_id` varchar(10) NOT NULL,
  `session_id` int(2) NOT NULL,
  PRIMARY KEY (`lot_id`,`auction_id`,`session_id`,`agent_id`),
  KEY `agent_assigned_lot_agent_FK` (`agent_id`),
  CONSTRAINT `agent_assigned_lot_agent_FK` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`agent_id`),
  CONSTRAINT `agent_assigned_lot_lot_FK` FOREIGN KEY (`lot_id`, `auction_id`, `session_id`) REFERENCES `lot` (`lot_id`, `auction_id`, `session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auction`
--

DROP TABLE IF EXISTS `auction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction` (
  `auction_id` varchar(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `total_sessions` int(11) DEFAULT NULL,
  `total_lots` int(11) DEFAULT NULL,
  PRIMARY KEY (`auction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auction_client`
--

DROP TABLE IF EXISTS `auction_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction_client` (
  `auction_id` varchar(10) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `paddle_number` varchar(10) NOT NULL,
  PRIMARY KEY (`auction_id`,`client_id`),
  KEY `auction_client_client_FK` (`client_id`),
  CONSTRAINT `auction_client_auction_FK` FOREIGN KEY (`auction_id`) REFERENCES `auction` (`auction_id`),
  CONSTRAINT `auction_client_client_FK` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auction_session`
--

DROP TABLE IF EXISTS `auction_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction_session` (
  `auction_id` varchar(10) NOT NULL,
  `session_id` int(2) NOT NULL,
  `state` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `start_lot` varchar(10) DEFAULT NULL,
  `end_lot` varchar(10) DEFAULT NULL,
  `open_time_window_start` bigint(20) NOT NULL,
  `open_time_window_end` bigint(20) NOT NULL,
  PRIMARY KEY (`auction_id`,`session_id`),
  CONSTRAINT `auction_session_auction_FK` FOREIGN KEY (`auction_id`) REFERENCES `auction` (`auction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `client_id` varchar(255) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `client_level` int(11) DEFAULT NULL,
  `client_number` bigint(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `kcm` varchar(255) DEFAULT NULL,
  `language_preference` varchar(255) DEFAULT NULL,
  `other_phone` varchar(255) DEFAULT NULL,
  `party_name` varchar(255) DEFAULT NULL,
  `party_type` varchar(255) DEFAULT NULL,
  `preferred_phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `client_lot`
--

DROP TABLE IF EXISTS `client_lot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_lot` (
  `client_id` varchar(255) NOT NULL,
  `lot_id` varchar(10) NOT NULL,
  `auction_id` varchar(10) NOT NULL,
  `session_id` int(2) NOT NULL,
  `cover_bid` bigint(10) DEFAULT NULL,
  `absentee_bid` bigint(10) DEFAULT NULL,
  `registration_datetime` varchar(100) NOT NULL,
  PRIMARY KEY (`lot_id`,`client_id`,`auction_id`,`session_id`),
  KEY `client_lot_lot_FK` (`lot_id`,`auction_id`,`session_id`),
  KEY `client_lot_client_FK` (`client_id`),
  CONSTRAINT `client_lot_client_FK` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `client_lot_lot_FK` FOREIGN KEY (`lot_id`, `auction_id`, `session_id`) REFERENCES `lot` (`lot_id`, `auction_id`, `session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feature_config`
--

DROP TABLE IF EXISTS `feature_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature_config` (
  `feature` varchar(255) NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`feature`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lot`
--

DROP TABLE IF EXISTS `lot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lot` (
  `lot_id` varchar(10) NOT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `estimate_high` bigint(20) DEFAULT NULL,
  `estimate_low` bigint(20) DEFAULT NULL,
  `executed` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `lot_author` longtext,
  `lot_description` longtext,
  `lot_path` varchar(255) DEFAULT NULL,
  `medium` varchar(255) DEFAULT NULL,
  `reserve_price` bigint(20) NOT NULL,
  `auction_id` varchar(10) NOT NULL,
  `session_id` int(2) NOT NULL,
  PRIMARY KEY (`lot_id`,`auction_id`,`session_id`),
  KEY `lot_auction_session_FK` (`auction_id`,`session_id`),
  CONSTRAINT `lot_auction_session_FK` FOREIGN KEY (`auction_id`, `session_id`) REFERENCES `auction_session` (`auction_id`, `session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lot_execution`
--

DROP TABLE IF EXISTS `lot_execution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lot_execution` (
  `agent_id` bigint(10) NOT NULL,
  `lot_id` varchar(10) NOT NULL,
  `auction_id` varchar(10) NOT NULL,
  `session_id` int(2) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `execution_amount` bigint(20) NOT NULL,
  `execution_result` varchar(255) NOT NULL,
  `paddle_number` varchar(255) NOT NULL,
  PRIMARY KEY (`agent_id`,`lot_id`,`auction_id`,`session_id`),
  KEY `lot_execution_agent_assigned_lot_FK` (`lot_id`,`auction_id`,`session_id`,`agent_id`),
  CONSTRAINT `lot_execution_agent_assigned_lot_FK` FOREIGN KEY (`lot_id`, `auction_id`, `session_id`, `agent_id`) REFERENCES `agent_assigned_lot` (`lot_id`, `auction_id`, `session_id`, `agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lot_result`
--

DROP TABLE IF EXISTS `lot_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lot_result` (
  `lot_id` varchar(10) NOT NULL,
  `auction_id` varchar(10) NOT NULL,
  `session_id` int(2) NOT NULL,
  `hammer_price` bigint(10) NOT NULL,
  `status` varchar(255) NOT NULL,
  `paddle_number` varchar(255) NOT NULL,
  `auctioneer` varchar(255) NOT NULL,
  PRIMARY KEY (`auction_id`,`lot_id`,`session_id`),
  KEY `lot_result_lot_FK` (`lot_id`,`auction_id`,`session_id`),
  CONSTRAINT `lot_result_lot_FK` FOREIGN KEY (`lot_id`, `auction_id`, `session_id`) REFERENCES `lot` (`lot_id`, `auction_id`, `session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'paperless_service'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-20 14:20:55
