-- PLS-90 (part 1: reserve, estimates) Need to remove previous prod-sourced data
DELETE FROM paperless_service.lot
WHERE auction_id NOT IN ('777777','EGL030','EGL242','EGL258','EGL270','EGL335','KHTML5','KT1000','LWIN03','PPL001','RAJ001','T20409','TS0417');

DELETE FROM paperless_service.auction_session
WHERE auction_id NOT IN ('777777','EGL030','EGL242','EGL258','EGL270','EGL335','KHTML5','KT1000','LWIN03','PPL001','RAJ001','T20409','TS0417');

DELETE FROM paperless_service.auction
WHERE  auction_id NOT IN ('777777','EGL030','EGL242','EGL258','EGL270','EGL335','KHTML5','KT1000','LWIN03','PPL001','RAJ001','T20409','TS0417');
