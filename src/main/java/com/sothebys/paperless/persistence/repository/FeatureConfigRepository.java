package com.sothebys.paperless.persistence.repository;

import com.sothebys.paperless.persistence.entity.Feature;
import com.sothebys.paperless.persistence.entity.FeatureConfig;
import java.util.List;
import java.util.Set;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeatureConfigRepository extends CrudRepository<FeatureConfig, Feature> {

  public List<FeatureConfig> findByFeatureIn(Set<Feature> ids);
    
}
