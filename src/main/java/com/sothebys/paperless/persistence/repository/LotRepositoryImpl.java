package com.sothebys.paperless.persistence.repository;

import com.sothebys.paperless.persistence.entity.Lot;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class LotRepositoryImpl implements CustomizedLotRepository {

  @PersistenceContext
  private EntityManager entityManager;

  @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
  private int batchSize;

  @Transactional
  @Override
  public List<Lot> bulkSave(List<Lot> entities) {
    final List<Lot> savedEntities = new ArrayList<Lot>(entities.size());
    int i = 0;
    for (Lot lot : entities) {
      if (Optional.ofNullable(lot).isPresent()) {
        savedEntities.add(persistOrMerge(lot));
        i++;
        if (i % batchSize == 0) {
          // Flush a batch of inserts and release memory.
          entityManager.flush();
          entityManager.clear();
        } 
      }
    }
    return savedEntities;
  }

  private Lot persistOrMerge(Lot lot) {
    if (lot.getId() == null) {
      entityManager.persist(lot);
      return lot;
    } else {
      return entityManager.merge(lot);
    }
  }
  
}
