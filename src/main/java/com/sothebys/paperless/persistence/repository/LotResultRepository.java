package com.sothebys.paperless.persistence.repository;

import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.entity.LotResult;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LotResultRepository extends JpaRepository<LotResult, LotId> {

  List<LotResult> findById_AuctionIdAndId_SessionId(String auctionId, int sessionNumber);

}
