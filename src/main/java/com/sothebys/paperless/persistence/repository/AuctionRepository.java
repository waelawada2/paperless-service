package com.sothebys.paperless.persistence.repository;

import com.sothebys.paperless.persistence.entity.Auction;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuctionRepository extends JpaRepository<Auction, String>{

  Optional<Auction> findOptionalById(String auctionId);

  List<Auction> findByIdIn(Set<String> auctionSessionIds);
  
}
