package com.sothebys.paperless.persistence.repository;

import com.sothebys.paperless.persistence.entity.LotExecution;
import com.sothebys.paperless.persistence.entity.LotExecutionId;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LotExecutionRepository extends JpaRepository<LotExecution, LotExecutionId> {

  Optional<LotExecution> findOptionalById(LotExecutionId lotExecutionId);

}
