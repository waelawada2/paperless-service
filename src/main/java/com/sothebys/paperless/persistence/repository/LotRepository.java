package com.sothebys.paperless.persistence.repository;

import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.Lot;
import com.sothebys.paperless.persistence.entity.LotId;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LotRepository extends JpaRepository<Lot, LotId>, CustomizedLotRepository {

  public List<Lot> findAllByIdIn(Set<LotId> ids);

  public List<Lot> findByAuctionSession(AuctionSession auctionSession);

  public Optional<Lot> findOptionalById(LotId id);

}
