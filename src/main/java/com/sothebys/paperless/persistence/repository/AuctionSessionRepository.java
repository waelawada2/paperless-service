package com.sothebys.paperless.persistence.repository;

import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.AuctionSessionId;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuctionSessionRepository extends JpaRepository<AuctionSession, AuctionSessionId> {

  public Optional<AuctionSession> findOptionalById(AuctionSessionId id);

  public List<AuctionSession> findByIdIn(Set<AuctionSessionId> ids);

  public List<AuctionSession> getByState(AuctionSessionState state);

}
