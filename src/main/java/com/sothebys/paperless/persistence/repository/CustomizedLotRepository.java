package com.sothebys.paperless.persistence.repository;

import com.sothebys.paperless.persistence.entity.Lot;
import java.util.List;

public interface CustomizedLotRepository {

  List<Lot> bulkSave(List<Lot> entities);
  
}
