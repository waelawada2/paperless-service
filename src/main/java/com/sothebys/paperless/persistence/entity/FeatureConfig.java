package com.sothebys.paperless.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "feature_config")
public class FeatureConfig {

  @Id
  @Enumerated(EnumType.STRING)
  @Column(name = "feature", unique = true, nullable = false)
  private Feature feature;

  @Column(name = "enabled", nullable = false)
  private boolean enabled;

}
