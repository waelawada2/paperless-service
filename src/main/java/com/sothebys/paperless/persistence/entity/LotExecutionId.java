package com.sothebys.paperless.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class LotExecutionId implements java.io.Serializable {

  @Column(name = "agent_id", nullable = false)
  private String agent_id;

  @Column(name = "lot_id", nullable = false, length = 10)
  private String lotId;

  @Column(name = "auction_id", nullable = false, length = 10)
  private String auctionId;

  @Column(name = "session_id", nullable = false)
  private int sessionId;

}
