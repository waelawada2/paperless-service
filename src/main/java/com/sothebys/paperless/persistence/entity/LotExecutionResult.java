package com.sothebys.paperless.persistence.entity;

public enum LotExecutionResult {
  NOBID, UNDERBID, WON;
}
