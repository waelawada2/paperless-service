package com.sothebys.paperless.persistence.entity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import com.sothebys.paperless.util.PaddedString;

public class LotResultListener {
  @PrePersist
  @PreUpdate
  public void normalizePaddleNumber(final LotResult lotResult) {
    lotResult.setPaddleNumber(new PaddedString(lotResult.getPaddleNumber()).toString());
  }
}
