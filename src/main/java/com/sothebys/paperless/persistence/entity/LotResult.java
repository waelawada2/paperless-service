package com.sothebys.paperless.persistence.entity;

import com.sothebys.paperless.domain.LotClosureResult;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@EntityListeners(LotResultListener.class)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "lot_result")
public class LotResult {

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "auctionId",
          column = @Column(name = "auction_id", nullable = false, length = 10)),
      @AttributeOverride(name = "lotId",
          column = @Column(name = "lot_id", nullable = false, length = 10)),
      @AttributeOverride(name = "sessionId",
          column = @Column(name = "session_id", nullable = false))})
  private LotId id;

  @OneToOne(fetch = FetchType.LAZY)
  @PrimaryKeyJoinColumn
  private Lot lot;

  @Column(name = "hammer_price", nullable = false)
  private long hammerPrice;

  @Enumerated(EnumType.STRING)
  @Column(name = "status", nullable = false)
  private LotClosureResult status;

  @Column(name = "paddle_number", nullable = false)
  private String paddleNumber;

  @Column(name = "auctioneer", nullable = false)
  private String auctioneer;

}
