package com.sothebys.paperless.persistence.entity;

public enum AuctionSessionState {
  OPEN, SCHEDULED, CLOSED;
}
