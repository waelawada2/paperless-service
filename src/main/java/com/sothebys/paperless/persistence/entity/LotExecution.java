package com.sothebys.paperless.persistence.entity;

import com.sothebys.paperless.domain.LotExecutionResult;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lot_execution")
public class LotExecution {

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "agentId", column = @Column(name = "agent_id", nullable = false)),
      @AttributeOverride(name = "lotId",
          column = @Column(name = "lot_id", nullable = false, length = 10)),
      @AttributeOverride(name = "auctionId",
          column = @Column(name = "auction_id", nullable = false, length = 10)),
      @AttributeOverride(name = "sessionId",
          column = @Column(name = "session_id", nullable = false))})
  private LotExecutionId id;

  @Column(name = "client_id", nullable = false)
  private String clientId;

  @Column(name = "execution_amount", nullable = false)
  private long executionAmount;

  @Enumerated(EnumType.STRING)
  @Column(name = "execution_result", nullable = false)
  private LotExecutionResult executionResult;

  @Column(name = "paddle_number", nullable = false)
  private String paddleNumber;

}
