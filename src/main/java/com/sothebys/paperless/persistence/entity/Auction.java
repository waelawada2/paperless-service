package com.sothebys.paperless.persistence.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "auction")
public class Auction {

  @Id
  @Column(name = "auction_id", unique = true, nullable = false, length = 10)
  private String id;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date", length = 19)
  private Date date;

  @Column(name = "location")
  private String location;

  @Column(name = "name")
  private String name;

  @Column(name = "total_sessions")
  private Integer totalSessions;

  @Column(name = "total_lots")
  private Integer totalLots;

  @Column(name = "currency")
  private String currency;

  @OneToMany(mappedBy = "auction")
  private List<AuctionSession> auctionSessions = new ArrayList<AuctionSession>(0);

  public Auction(String id) {
    this.id = id;
  }

}
