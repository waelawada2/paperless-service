package com.sothebys.paperless.persistence.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "lot")
public class Lot {

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "lotId",
          column = @Column(name = "lot_id", nullable = false, length = 10)),
      @AttributeOverride(name = "auctionId",
          column = @Column(name = "auction_id", nullable = false, length = 10)),
      @AttributeOverride(name = "sessionId",
          column = @Column(name = "session_id", nullable = false))})
  private LotId id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "auction_id", referencedColumnName = "auction_id", nullable = false,
          insertable = false, updatable = false),
      @JoinColumn(name = "session_id", referencedColumnName = "session_id", nullable = false,
          insertable = false, updatable = false)})
  private AuctionSession auctionSession;

  @Column(name = "estimate_high")
  private Long estimateHigh;

  @Column(name = "estimate_low")
  private Long estimateLow;

  @Column(name = "executed")
  private String executed;

  @Column(name = "image_url")
  private String imageUrl;

  @Lob
  @Column(name = "lot_author")
  private String lotAuthor;

  @Lob
  @Column(name = "lot_description")
  private String lotDescription;

  @Column(name = "lot_path")
  private String lotPath;

  @Column(name = "medium")
  private String medium;

  @Column(name = "reserve_price", nullable = false)
  private long reservePrice;

  @OneToOne(mappedBy = "lot")
  private LotResult lotResult;

  @Column(name = "is_withdrawn", nullable = true)
  private Boolean isWithdrawn;

  public Lot(LotId id, AuctionSession auctionSession, long reservePrice) {
    this.id = id;
    this.auctionSession = auctionSession;
    this.reservePrice = reservePrice;
  }

}
