package com.sothebys.paperless.persistence.entity;

public enum Feature {
  VALIDATE_OPEN_AUCTION_TIME_WINDOW,
  VALIDATE_CLOSE_AUCTION_LOTS_CLOSED
}
