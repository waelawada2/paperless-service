package com.sothebys.paperless.persistence.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.util.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "auction_session")
public class AuctionSession {

  public AuctionSession(String auctionId, Integer sessionNumber) {
    this.id = new AuctionSessionId(auctionId, sessionNumber);
  }

  public AuctionSession(AuctionSessionId id) {
    this.id = id;
  }

  /**
   * @return true if either {@link #state} returns null or is equivalent to
   *          {@code AuctionSessionState#SCHEDULED}; false otherwise.
   */
  public boolean hasUnmodifiedState() {
    return getState() == null || AuctionSessionState.SCHEDULED.equals(getState());
  }

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "auctionId",
          column = @Column(name = "auction_id", nullable = false, length = 10)),
      @AttributeOverride(name = "sessionId",
          column = @Column(name = "session_id", nullable = false))})
  private AuctionSessionId id;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "auction_id", nullable = false, insertable = false, updatable = false)
  private Auction auction;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name= "start_date")
  private Date startDate;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name="open_date")
  private Date openDate;
  
  @Column(name="opening_auctioneer")
  private String openingAuctioneer;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name="close_date")
  private Date closeDate;
  
  @Column(name="closing_auctioneer")
  private String closingAuctioneer;
  
  @Column(name = "open_time_window_end", nullable = false)
  private long openTimeWindowEnd;
  
  @Column(name = "open_time_window_start", nullable = false)
  private long openTimeWindowStart;
  
  @Enumerated(EnumType.STRING)
  @Column(name = "state")
  private AuctionSessionState state;
  
  @Column(name = "start_lot", length = 10)
  private String startLot;
  
  @Column(name = "end_lot", length = 10)
  private String endLot;
  
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "auctionSession")
  private List<Lot> lots = new ArrayList<Lot>(0);

}
