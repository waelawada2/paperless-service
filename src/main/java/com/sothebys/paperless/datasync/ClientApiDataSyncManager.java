package com.sothebys.paperless.datasync;

import java.util.Set;

/**
 * Component responsible of syncing data between the client system and the DB
 */
public interface ClientApiDataSyncManager {

  /**
   * Fetches information of a client in the client system and stores it in the DB.
   * 
   * @param accountNumber the account number
   */
  void fetchClientDataForAccount(String accountNumber);

  /**
   * Check if clients exists in the DB or else fetch the info from client system and stores them in the DB.
   * 
   * @param accountNumbers the account numbers
   */
  void fetchClientDataForAccounts(Set<String> accountNumbers);

}
