package com.sothebys.paperless.exceptions;

public class LotNotFoundException extends EntityNotFoundException {

  private static final long serialVersionUID = 0L;

  public LotNotFoundException(String string) {
    super(String.format("Can't find lot %s", string.toString()));
  }

  public LotNotFoundException(String lotId, String auctionId, int sessionNumber) {
    super(String.format("Could not find lot %s in auction %s session %s", lotId, auctionId,
        sessionNumber));
  }

}
