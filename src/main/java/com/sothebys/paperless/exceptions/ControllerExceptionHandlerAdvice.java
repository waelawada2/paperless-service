package com.sothebys.paperless.exceptions;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandlerAdvice {

  private static final String ERROR = "error";
  private static final String DATA = "data";

  @ExceptionHandler({MethodArgumentTypeMismatchException.class,
      MissingServletRequestParameterException.class, IllegalArgumentException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public Map<String, String> handleBadArguments(Exception e) {
    log.error("Bad argument received: {}", e.getMessage());
    return Collections.singletonMap(ERROR, "Bad argument received");
  }

  @ExceptionHandler(AuctionSessionStateUpdateException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public Map<String, String> handleBadRequestOnOpenSession(AuctionSessionStateUpdateException e) {
    return Collections.singletonMap(ERROR, e.getMessage());
  }

  @ExceptionHandler({EntityNotFoundException.class, AuctionNotFoundException.class,
      LotNotFoundException.class})
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  public Map<String, String> handleEntityNotFound(EntityNotFoundException e) {
    return Collections.singletonMap(ERROR, e.getMessage());
  }

  @ExceptionHandler(WebApiResourceException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public Map<String, String> handleWebApiResourceException(WebApiResourceException e) {
    return Collections.singletonMap(ERROR,
        String.format("Cannot retrieve lots data from web api: %s", e.getMessage()));
  }

  @ExceptionHandler(DataAccessException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public Map<String, String> handleDataAccessExceptions(DataAccessException e) {
    return Collections.singletonMap(ERROR,
        String.format("Cannot access data from the database: %s", e.getMessage()));
  }

  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public Map<String, List<String>> handle(ConstraintViolationException exception) {
    return Collections.singletonMap(ERROR, exception.getConstraintViolations().stream()
        .map(ConstraintViolation::getMessage).collect(Collectors.toList()));
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public Map<String, String> handleExceptions(Exception e) {
    return Collections.singletonMap(ERROR,
        String.format("An unexpected error ocurred: %s", e.getMessage()));
  }

  @ExceptionHandler({LotExecutionException.class, LotClosureException.class})
  @ResponseStatus(HttpStatus.CONFLICT)
  @ResponseBody
  public Map<String, ? extends Object> handleLotExecutionException(PaperlessServiceException e) {
    Map<String, Object> errorMap = new HashMap<String, Object>();
    errorMap.put(ERROR, e.getMessage());
    errorMap.put(DATA, e.getData());
    return errorMap;
  }

  @ExceptionHandler(SAPClientException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public Map<String, String> handleSAPClientException(SAPClientException e) {
    return Collections.singletonMap(ERROR,
        String.format("Error contacting SAP"));
  }

}
