package com.sothebys.paperless.exceptions;

import com.sothebys.paperless.persistence.entity.LotExecutionId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class LotExecutionException extends PaperlessServiceException {

  private static final long serialVersionUID = -2000033507333340887L;


  public LotExecutionException(LotExecutionErrorType type, LotExecutionId lotExecId) {
    super(type.name());
    this.data.put("lotExecId", lotExecId);
  }


}
