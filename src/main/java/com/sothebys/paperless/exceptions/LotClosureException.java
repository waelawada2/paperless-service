package com.sothebys.paperless.exceptions;

public class LotClosureException extends PaperlessServiceException {

  private static final long serialVersionUID = 1L;
  private static final String ERROR_MESSAGE_TEMPLATE = "Cannot close lot due to: [%s]";

  public LotClosureException() {
    super();
  }

  public LotClosureException(String message, Throwable cause) {
    super(String.format(ERROR_MESSAGE_TEMPLATE, message), cause);
  }

  public LotClosureException(String message) {
    super(String.format(ERROR_MESSAGE_TEMPLATE, message));
  }
  
}
