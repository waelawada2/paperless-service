package com.sothebys.paperless.exceptions;

public class SAPClientException extends PaperlessServiceException {

    private static final long serialVersionUID = 1L;

    SAPClientException(String message) {
        super(message);
    }
}
