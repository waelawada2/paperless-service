package com.sothebys.paperless.exceptions;

import lombok.EqualsAndHashCode;

/**
 * An exception for when an auction is not found. 
 */
@EqualsAndHashCode(callSuper = false)
public class AuctionNotFoundException extends EntityNotFoundException {

  private static final long serialVersionUID = 1L;

  public AuctionNotFoundException(String auctionId) {
    super(String.format("Auction not found %s", auctionId));
  }
}
