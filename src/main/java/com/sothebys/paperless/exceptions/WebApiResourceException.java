package com.sothebys.paperless.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class WebApiResourceException extends PaperlessServiceException {

  private static final long serialVersionUID = 2473547609157547226L;

  public WebApiResourceException(String message) {
    super(message);
  }

  public WebApiResourceException(String message, Throwable cause) {
    super(message, cause);
  }
}
