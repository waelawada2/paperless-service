package com.sothebys.paperless.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class EntityNotFoundException extends PaperlessServiceException {

  private static final long serialVersionUID = -7003925711362803209L;
  
  public EntityNotFoundException(String message) {
    super(message);
  }

}
