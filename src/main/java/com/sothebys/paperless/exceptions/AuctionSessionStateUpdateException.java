package com.sothebys.paperless.exceptions;

import com.sothebys.paperless.persistence.entity.AuctionSessionState;

/**
 * Exception thrown when an error occurs during attempting to update an auction session state
 */
public class AuctionSessionStateUpdateException extends PaperlessServiceException {

  private static final long serialVersionUID = -1129054818372555828L;

  public AuctionSessionStateUpdateException(String auctionId, int sessionNumber) {
    super(String.format("Cannot find auction %s session %s", auctionId, sessionNumber));
  }

  public AuctionSessionStateUpdateException(String auctionId, int sessionNumber, AuctionSessionState state) {
    super(String.format("Cannot change state for auction %s session %s to %s", auctionId, sessionNumber, state.name()));
  }

  public AuctionSessionStateUpdateException(String message, AuctionSessionState state,
      long timeMillis) {
    super(String.format("Cannot change state for %s to %s: outside time window for state change.",
        message, state.name(), timeMillis));
  }

}
