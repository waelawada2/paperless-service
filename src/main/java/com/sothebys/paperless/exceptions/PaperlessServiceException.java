package com.sothebys.paperless.exceptions;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PaperlessServiceException extends RuntimeException {

  private static final long serialVersionUID = 2473547609157547226L;
  protected Map<String, Object> data = new HashMap<String, Object>();
  
  public PaperlessServiceException() {
    super();
  }

  public PaperlessServiceException(String message) {
    super(message);
  }

  public PaperlessServiceException(String message, Throwable cause) {
    super(message, cause);
  }
}
