package com.sothebys.paperless.controller;

import com.sothebys.paperless.configuration.toggle.ToggleRouter;
import com.sothebys.paperless.persistence.entity.Feature;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeatureConfigurationController {

  private ToggleRouter toggleRouter;

  public FeatureConfigurationController(ToggleRouter toggleRouter) {
    this.toggleRouter = toggleRouter;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/featureStatus/{feature}")
  public Boolean isFeatureEnabled(@PathVariable Feature feature) {
    return toggleRouter.isFeatureEnabled(feature);
  }
  
  @RequestMapping(method = RequestMethod.POST, value = "/featureStatus/{feature}")
  public void toggleFeature(@PathVariable Feature feature) {
    toggleRouter.toggleFeature(feature);
  }
}
