package com.sothebys.paperless.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ManagementController {

  @RequestMapping(method = RequestMethod.GET, value = "/thread")
  public String displayThreadName() {
    String threadName = Thread.currentThread().getName();
    return threadName;
  }
}
