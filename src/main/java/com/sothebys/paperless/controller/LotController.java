package com.sothebys.paperless.controller;

import com.sothebys.paperless.domain.BidLot;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.domain.integration.BidLots;
import com.sothebys.paperless.persistence.entity.LotResult;
import com.sothebys.paperless.service.AuctionService;
import com.sothebys.paperless.service.LotService;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller responsible for attending request related to lots.
 */
@RestController
@RequestMapping("/auctions/{auctionId}/lots")
@Slf4j
@Validated
public class LotController {

  private final AuctionService auctionService;
  private final LotService lotService;

  public LotController(AuctionService auctionService, LotService lotService) {
    this.auctionService = auctionService;
    this.lotService = lotService;
  }

  /**
   * Returns a list of Lots belonging to a given sale (auction). It may return them filtered by
   * agent.
   * 
   * @param showAssignedOnly true if the list should only include lots assigned to the agent.
   * @param auctionId the id of the auction to which all returned lots belong to.
   * @param sessionNumber the session number
   * @param clientId the client id
   * @param user the user information injected at runtime
   * @return a list of Lots
   */
  @ApiResponses(
      value = {@ApiResponse(code = 200, message = "The request was processed successfully"),
          @ApiResponse(code = 404,
              message = "No lots information found with the supplied criteria"),
          @ApiResponse(code = 500, message = "Server error occured")})
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public BidLots getLots(
      @ApiParam(value = "The id for the auction",
          required = true) @PathVariable(required = true) String auctionId,
      @ApiParam(value = "The auction session number", required = false,
          defaultValue = "1") @RequestParam(defaultValue = "1") int sessionNumber,
      @ApiParam(value = "Show only lots assigned to the agent",
          required = false) @RequestParam(required = false) Boolean showAssignedOnly,
      @ApiParam(value = "The id for the client",
          required = false) @RequestParam(required = false) Long clientId,
      User user) {
    showAssignedOnly = Optional.ofNullable(showAssignedOnly).orElse(false);
    if (showAssignedOnly) {
      return auctionService.getLotsByAgent(auctionId, sessionNumber, user);
    } else if (Optional.ofNullable(clientId).isPresent()) {
      return auctionService.getLotsByClient(auctionId, sessionNumber, clientId);
    } else {
      return auctionService.getLotsByAuction(auctionId, sessionNumber);
    }
  }

  @ApiResponses(
      value = {@ApiResponse(code = 200, message = "The lot information was retrieved successfully"),
          @ApiResponse(code = 404, message = "No auction/session/lot data found"),
          @ApiResponse(code = 410, message = "Lot is withdrawn"),
          @ApiResponse(code = 500, message = "Server error occured")})
  @GetMapping(value = "/{lotId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public BidLot getLotData(
      @ApiParam(value = "The id for the auction", required = true) @PathVariable String auctionId,
      @ApiParam(value = "The id for the lot", required = true) @PathVariable String lotId,
      @ApiParam(value = "The auction session number",
          required = true) @RequestParam(required = true) @Min(value = 1) int sessionNumber) {
    Optional<BidLot> optionalBidLot =
        Optional.ofNullable(lotService.getLotData(auctionId, sessionNumber, lotId));
    return optionalBidLot.get();
  }

  /**
   * Attempts to close a lot from hammer app. It involves passing a LotResult instance containing
   * information about the amount, the status of the closure (SOLD; UNSOLD), the auctioneer
   * performing the closure, and a paddle/client id for the case of SOLD.
   * 
   * @param auctionId path variable for auctionId
   * @param lotId path variable for lotId
   * @param sessionNumber the number of the session the case is in
   * @param lotResult the instance of LotResult containing details about the lot closure
   * @param auctioneer the user information injected at runtime
   * @return a copy of the LotResult with the status updated, reflecting the changes in the DB.
   */
  @ApiResponses(
      value = {@ApiResponse(code = 200, message = "The lot closure was completed by auctioneer"),
          @ApiResponse(code = 400, message = "Missing required parameter 'sessionNumber'"),
          @ApiResponse(code = 400, message = "Invalid value for 'sessionNumber' (int >= 0)"),
          @ApiResponse(code = 409, message = "A lot was already closed. Cannot complete closure."),
          @ApiResponse(code = 500, message = "Server error occured")})
  @PostMapping(value = "/{lotId}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public LotResult lotClose(
      @ApiParam(value = "The id for the auction",
          required = true) @PathVariable(required = true) String auctionId,
      @ApiParam(value = "The id for lot",
          required = true) @PathVariable(required = true) String lotId,
      @ApiParam(value = "The id for the auction session this lot is in",
          required = true) @RequestParam(required = true) @Min(value = 1) int sessionNumber,
      @RequestBody @Valid LotResult lotResult, User auctioneer) {
    log.debug("resource: lotClose(auction={}, session={}, lot={}, lotResult={})", auctionId,
        sessionNumber, lotId, lotResult);
    lotResult.setAuctioneer(auctioneer.getName());
    return lotService.closeLot(auctionId, sessionNumber, lotId, lotResult);
  }

}
