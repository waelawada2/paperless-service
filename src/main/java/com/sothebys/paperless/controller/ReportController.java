package com.sothebys.paperless.controller;

import com.sothebys.paperless.exceptions.PaperlessServiceException;
import com.sothebys.paperless.service.ReportService;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@AllArgsConstructor
@RestController
@Api(produces = "application/json", value = "The report controller.")
@RequestMapping(value = "/reports")
public class ReportController {

  private ReportService reportService;

  @ApiResponses({@ApiResponse(code = 404, message = "Auction session not found"),
      @ApiResponse(code = 200, message = "Report generated correctly")})
  @GetMapping(value = "download")
  public void getCSVReport(@ApiParam(required = true) @RequestParam String auctionId,
      @ApiParam(required = true,
          defaultValue = "1") @RequestParam(defaultValue = "1") int sessionNumber,
      HttpServletResponse response) {
    try {
      response.getWriter().write(reportService.getCSVReport(auctionId, sessionNumber));
    } catch (IOException e) {
      throw new PaperlessServiceException("Error generating report");
    }
    String reportFileName = reportService.getReportFileName(auctionId, sessionNumber);
    response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
        String.format("attachment; filename=\"%s\"", reportFileName));
  }
}
