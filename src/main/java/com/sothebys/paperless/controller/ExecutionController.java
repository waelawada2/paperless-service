package com.sothebys.paperless.controller;

import com.sothebys.paperless.domain.BidLot;
import com.sothebys.paperless.domain.LotResult;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.service.LotExecutionService;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller responsible for attending request related to executions.
 */
@RestController
@RequestMapping("/auctions/{auctionId}/lots/{lotId}/executions")
@Validated
public class ExecutionController {

  private LotExecutionService lotExecutionService;

  public ExecutionController(LotExecutionService lotExecutionService) {
    this.lotExecutionService = lotExecutionService;
  }

  @ApiResponses(
      value = {@ApiResponse(code = 200, message = "The lot execution was completed by agent"),
          @ApiResponse(code = 400, message = "Missing required parameter 'sessionNumber'"),
          @ApiResponse(code = 400, message = "Invalid value for 'sessionNumber' (int >= 0)"),
          @ApiResponse(code = 409,
              message = "A lot was already closed or confirmed. Cannot complete execution."),
          @ApiResponse(code = 500, message = "Server error occured")})
  @PostMapping
  @ResponseStatus(value = HttpStatus.OK)
  public BidLot lotExecuted(
      @ApiParam(value = "The id for the auction",
          required = true) @PathVariable(required = true) String auctionId,
      @ApiParam(value = "The session number",
          required = true) @RequestParam(required = true) Integer sessionNumber,
      @ApiParam(value = "The id for lot",
          required = true) @PathVariable(required = true) String lotId,
      @RequestBody @Valid LotResult lotExecutionResult, User user) {
    return lotExecutionService.confirmLotExecution(lotExecutionResult, auctionId, sessionNumber,
        lotId, user);
  }

}
