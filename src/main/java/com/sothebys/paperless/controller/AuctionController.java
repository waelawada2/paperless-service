package com.sothebys.paperless.controller;

import com.sothebys.paperless.domain.AuctionCard;
import com.sothebys.paperless.domain.AuctionCards;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.exceptions.AuctionSessionStateUpdateException;
import com.sothebys.paperless.exceptions.PaperlessServiceException;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import com.sothebys.paperless.service.AuctionService;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller responsible for attending request related to auctions.
 */
@RestController
@RequestMapping("/auctions")
@Slf4j
public class AuctionController {

  private final AuctionService auctionService;

  public AuctionController(AuctionService auctionService) {
    this.auctionService = auctionService;
  }

  /**
   * Get a list of auction data for cards.
   * @param state the state parameter to filter the auction cards by
   * @return List of auctions information
   */
  @ApiResponses(
      value = {@ApiResponse(code = 200, message = "The request was processed successfully"),
          @ApiResponse(code = 404,
              message = "No auctions information found with the supplied criteria"),
          @ApiResponse(code = 500, message = "Server error occured")})
  @GetMapping(value = "/cards", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public AuctionCards getAuctionCards(@ApiParam(name = "state", value = "Optional state filter",
      required = false) @RequestParam(name = "state", required = false) AuctionSessionState state) {
    if (Optional.ofNullable(state).isPresent()) {
      return auctionService.getAllAuctionCardsByState(state);
    } else {
      return auctionService.getAllAuctionCards();
    }
  }

  /**
   * Get a list of auction data for cards assigned to a phone bidder.
   * 
   * @param user the user information injected to the method runtime 
   * @return List of auctions information by phone bidder
   */
  @ApiResponses(
      value = {@ApiResponse(code = 200, message = "The request was processed successfully"),
          @ApiResponse(code = 404,
              message = "No auctions information found with the supplied criteria"),
          @ApiResponse(code = 500, message = "Server error occured")})
  @GetMapping(value = "/cardsByAgent", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public AuctionCards getAuctionCardsByAgent(User user) {
    log.debug("Serving cards for agent {}", user);
    return auctionService.getAuctionCardsForAgent(user);
  }

  /**
   * Updates auction session state with the supplied data.
   * 
   * @param auctionCard The auction object, containing a matching id in the desired state. If its
   *        OPEN, the action performed to this auction is to open it, and updating all of its lots
   *        states to open.
   * @param auctioneer the user information injected at runtime
   */
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "The auction session was successfully opened"),
      @ApiResponse(code = 404,
          message = "No auction sessions information found with the supplied criteria. Cannot open auction session"),
      @ApiResponse(code = 500, message = "Server error occured")})
  @PostMapping("/state")
  @ResponseStatus(value = HttpStatus.OK)
  public void updateAuctionSessionState(
      @ApiParam(value = "The Auction object, containing a matching Id and the desired state. ",
          required = true) @RequestBody AuctionCard auctionCard,
      User auctioneer) {
    if (auctionCard.getState() != null) {
      switch (auctionCard.getState()) {
        case OPEN:
          auctionService.openAuctionSession(auctionCard, auctioneer);
          break;
        case CLOSED:
          auctionService.closeAuctionSession(auctionCard, auctioneer);
          break;
        default:
          throw new PaperlessServiceException("Operation not supported");
      }

    } else {
      throw new AuctionSessionStateUpdateException(auctionCard.getAuctionId(),
          auctionCard.getSession(), auctionCard.getState());
    }
  }

}
