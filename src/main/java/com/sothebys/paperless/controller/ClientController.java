package com.sothebys.paperless.controller;

import com.sothebys.paperless.domain.BidClient;
import com.sothebys.paperless.service.ClientService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Slf4j
@AllArgsConstructor
@RestController
@Api(produces = "application/json", value = "The client information controller.")
@RequestMapping(value = "/client")
public class ClientController {

  private ClientService clientService;
  
  /**
   * Returns the client information in the context of the auction/session/lot.
   * 
   * @param clientId the client account number
   * @param auctionId the sale number
   * @param sessionNumber the session number
   * @param lotId the lot number;
   * @return the client information.
   */
  @ApiResponses(
      value = {@ApiResponse(code = 200, message = "The request was processed successfully"),
          @ApiResponse(code = 400, message = "If any required param is missing"),
          @ApiResponse(code = 404, message = "No client information found for auction/session/lot"),
          @ApiResponse(code = 500, message = "Server error occured")})
  @GetMapping("{clientId}")
  public BidClient getClientDataForLot(
      @ApiParam(value = "Client account number", required = true) @PathVariable String clientId,
      @ApiParam(value = "Sale number", required = true) @RequestParam String auctionId,
      @ApiParam(value = "Session number", required = true) @RequestParam int sessionNumber,
      @ApiParam(value = "Lot number", required = true) @RequestParam String lotId) {
    log.debug("Serving /client/{}?auctionId={}&sessionId={}&lotId={}", clientId, auctionId,
        sessionNumber, lotId);

    return clientService.getClientDataForLot(clientId, auctionId, sessionNumber, lotId);
    
  }
}
