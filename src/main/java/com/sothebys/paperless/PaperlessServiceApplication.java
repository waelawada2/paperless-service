package com.sothebys.paperless;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Main entry point to the application.
 */
@EnableScheduling
@EnableSwagger2
@SpringBootApplication
public class PaperlessServiceApplication {

  /**
   * Main method for starting the application.
   * @param args any arguments passed to the main method
   */
  public static void main(String[] args) {

    SpringApplication.run(PaperlessServiceApplication.class, args);

  }

}
