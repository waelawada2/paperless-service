package com.sothebys.paperless.domain.clientapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClientApiData {

  Long partyId;
  String partyName;
  String email;
  Integer clientLevelId;
  String partyType;
  String countryConnection;
  String kcmName;
  String kcmEmail;
  String primaryContactName;

}
