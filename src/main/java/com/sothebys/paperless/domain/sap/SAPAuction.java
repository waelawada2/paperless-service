package com.sothebys.paperless.domain.sap;

import lombok.Data;

@Data
public class SAPAuction {

  private String auctionId;
  private Integer sessionNumber;
  
}
