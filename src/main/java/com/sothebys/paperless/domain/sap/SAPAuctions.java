package com.sothebys.paperless.domain.sap;

import java.util.List;
import lombok.Data;

@Data
public class SAPAuctions {

  private List<SAPAuction> auctions;
  
}
