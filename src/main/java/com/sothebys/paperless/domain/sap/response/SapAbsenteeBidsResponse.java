package com.sothebys.paperless.domain.sap.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sothebys.paperless.domain.sap.SapBidder;
import com.sothebys.paperless.domain.sap.SapReturn;
import java.util.List;
import lombok.Data;

/**
 * Represents a wrapper for the response from SAP API to the Paperless Application
 */
@Data
public class SapAbsenteeBidsResponse {
  @JsonProperty("RETURN")
  private SapReturn returnField;
  @JsonProperty("ABSENTEE_BIDS")
  private List<SapBidder> absenteeBids;
}
