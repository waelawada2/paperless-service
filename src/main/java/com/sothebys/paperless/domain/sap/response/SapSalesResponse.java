package com.sothebys.paperless.domain.sap.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sothebys.paperless.domain.sap.SapReturn;
import com.sothebys.paperless.domain.sap.SapSaleRecord;
import java.util.List;
import lombok.Data;

@Data
public class SapSalesResponse {
  @JsonProperty("RETURN")
  private SapReturn returnField;
  @JsonProperty("SALE_RECORD")
  private List<SapSaleRecord> saleRecord;
}
