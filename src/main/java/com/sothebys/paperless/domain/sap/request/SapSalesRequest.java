package com.sothebys.paperless.domain.sap.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sothebys.paperless.domain.sap.SapReturn;
import com.sothebys.paperless.domain.sap.SapSaleRecord;
import java.util.Arrays;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SapSalesRequest extends SapBaseRequest {
  private static final long serialVersionUID = 1L;
  
  @JsonProperty("SALENUM")
  private String saleNum;
  @JsonProperty("LOTNUM")
  private String lotNum;
  @JsonProperty("RETURN")
  private SapReturn returnField = new SapReturn();
  @JsonProperty("SALE_RECORD")
  private List<SapSaleRecord> saleRecord = Arrays.asList(new SapSaleRecord());
}
