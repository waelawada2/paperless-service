package com.sothebys.paperless.domain.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SapReturn {
  @JsonProperty("TYPE")
  private String type;
  @JsonProperty("ID")
  private String id;
  @JsonProperty("NUMBER")
  private String number = "000";
  @JsonProperty("MESSAGE")
  private String message;
  @JsonProperty("LOG_NO")
  private String logNo;
  @JsonProperty("LOG_MSG_NO")
  private String logMsgNo = "000000";
  @JsonProperty("MESSAGE_V1")
  private String messageV1;
  @JsonProperty("MESSAGE_V2")
  private String messageV2;
  @JsonProperty("MESSAGE_V3")
  private String messageV3;
  @JsonProperty("MESSAGE_V4")
  private String messageV4;
}
