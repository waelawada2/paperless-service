package com.sothebys.paperless.domain.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = {"high", "low", "reserve"})
public class SapSaleRecord {

  private static final String WDN = "WDN";

  @JsonProperty("SALENUM")
  private String saleNum;
  @JsonProperty("LOTNUM")
  private String lotNum;
  @JsonProperty("STATUS")
  private String status;
  @JsonProperty("HIGH")
  private String high;
  @JsonProperty("LOW")
  private String low;
  @JsonProperty("RESERVE")
  private String reserve;
  @JsonProperty("RESERVE_CURR")
  private String reserveCurr;

    /**
   * For this SapSaleRecord, it asserts whether it represents a withdrawn lot
   * 
   * @return true if this SapSaleRecord represents a withdrawn lot, false otherwise
   */
  public boolean isWithdrawn() {
    return WDN.equalsIgnoreCase(getStatus());
  }
}
