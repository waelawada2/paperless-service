package com.sothebys.paperless.domain.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SapBidder {
  @JsonProperty("SALENUM")
  private String saleNum;
  @JsonProperty("CLIENT")
  private String client;
  @JsonProperty("LOTNUM")
  private String lotNum;
  @JsonProperty("BOLD_HEADING")
  private String boldHeading;
  @JsonProperty("PADDLE_NUM")
  private String paddleNum;
  @JsonProperty(value = "PLFAZ", defaultValue = "00000000")
  private String plFaz;
  @JsonProperty(value = "BDATE", defaultValue = "00000000")
  private String bDate;
  @JsonProperty(value = "BTIME", defaultValue = "000000")
  private String bTime;
  @JsonProperty("REG_SOURCE")
  private String regSource;
  @JsonProperty("REG_SOURCE_TXT")
  private String regSourceTxt;
  @JsonProperty("HIGH")
  private String high;
  @JsonProperty("LOW")
  private String low;
  @JsonProperty("RESERVE")
  private String reserve;
  @JsonProperty("RESERVE_CURR")
  private String reserveCurr;
  @JsonProperty("RESERVE_SALE")
  private String reserveSale;
  @JsonProperty("RES_SALE_CURR")
  private String resSaleCurr;
  @JsonProperty(value = "ASSIGN_EMPL", defaultValue = "00000000")
  private String assignEmpl;
  @JsonProperty("ASSIGN_USER")
  private String assignUser;
  @JsonProperty("ASSIGN_EMAIL")
  private String assignEmail;
  @JsonProperty("PLATFORM")
  private String platform;
  @JsonProperty("PLATFORM_TXT")
  private String platformTxt;
  @JsonProperty("IB_FLAG")
  private String ibFlag;
  @JsonProperty("DEL")
  private String del;
  @JsonProperty("TOPBID")
  private String topBid;
  @JsonProperty("PLUS")
  private String plus;
  @JsonProperty("ORIND")
  private String orInd;
  @JsonProperty("PHONE_BID")
  private String phoneBid;
  @JsonProperty("TEL1")
  private String tel1;
  @JsonProperty("TEL2")
  private String tel2;
  @JsonProperty("FAX")
  private String fax;
  @JsonProperty("COVER_BID")
  private String coverBid;
  @JsonProperty("PREF_LANGU")
  private String prefLangu;
  @JsonProperty("STATUS")
  private String status;
  @JsonProperty("GROUP_TYPE")
  private String groupType;
  @JsonProperty(value = "GROUP_NUM", defaultValue = "000000")
  private String groupNum;
  @JsonProperty(value = "PARCEL_CNT", defaultValue = "000000")
  private String parcelCnt;
  @JsonProperty("BID_NOROUND")
  private String bidNoround;
  @JsonProperty("HEADER_LOT")
  private String headerLot;
  @JsonProperty("AGENT_NAME")
  private String agentName;
}
