package com.sothebys.paperless.domain.sap.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sothebys.paperless.domain.sap.SapBidder;
import com.sothebys.paperless.domain.sap.SapReturn;
import java.util.Arrays;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SapBidsRequest extends SapBaseRequest {

  private static final long serialVersionUID = 1L;
  
  @JsonProperty("SALENUM")
  private String saleNum;
  @JsonProperty("CLIENT")
  private String client;
  @JsonProperty("LOTNUM")
  private String lotNum;
  @JsonProperty("EMAILID")
  private String emailId;
  @JsonProperty("RETURN")
  private SapReturn returnField = new SapReturn();
  @JsonProperty("ABSENTEE_BIDS")
  private List<SapBidder> absenteeBids = Arrays.asList(new SapBidder());
}
