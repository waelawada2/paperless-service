package com.sothebys.paperless.domain;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Auctions {
  
  private List<AuctionDTO> auctions;

}
