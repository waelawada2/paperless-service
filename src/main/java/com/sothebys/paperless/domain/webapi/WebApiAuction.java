package com.sothebys.paperless.domain.webapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sothebys.paperless.domain.AuctionTypes;

import java.util.List;
import java.util.Optional;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class WebApiAuction {

  private String currency;
  private String typeCode;
  private WebApiLocation location;
  private String saleNumber;
  private List<WebApiAuctionSession> sessions;
  private String title;
  private Integer totalLots;
  private Long startDate;
  private String eventStatusPath;

  /**
   * Checks the value of this WebApiAuction.typeCode
   * 
   * @return true if {@link #typeCode} is set to either "S" or "A" (single owner and regular); false
   *         otherwise
   */
  public boolean isSingleOwnerOrRegular() {
    return AuctionTypes.isAllowed(getTypeCode());
  }

  /**
   * @return true if {@link #eventStatusPath} is set to "SCH"; false
   *         otherwise
   */
  public boolean isScheduled() {
    return WebApiAuctionState.SCH.name().equalsIgnoreCase(getEventStatusPath());
  }

  /**
   * @return true if {@link #totalLots} value is greater than zero.
   */
  public boolean hasLots() {
    return Optional.ofNullable(getTotalLots()).isPresent() && getTotalLots() > 0;
  }

  /**
   * @return true if the auction's sessions all have
   *         {@code WebApiAuctionSession#getStartLot()} with a value different
   *         from zero; false otherwise.
   */
  public boolean hasAllSessionsStartLotNonZero() {
    return getSessions().stream().map(waas -> {
      if (Integer.valueOf(waas.getStartLot()) == 0) {
        return false;
      }
      return true;
    }).reduce(true, (a, b) -> a && b);
  }

  /**
   * @param lots the list of lots belonging to this WebApiAuction
   * @return true if any of this auction's lots'
   *         {@code WebApiBidLot#getLotNumber()} are set to "Preview"; false
   *         otherwise
   */
  public boolean isPreviewAuction(List<WebApiBidLot> lots) {
    return lots.stream().anyMatch(l -> l.isPreview());
  }

  /**
   * @param lots the list of lots belonging to this WebApiAuction
   * @return true if not all of this auction's lots'
   *         {@code WebApiBidLot#getLotNumber()} are set to "Preview"; false
   *         otherwise
   */
  public boolean isNotPreviewAuction(List<WebApiBidLot> lots) {
    return !isPreviewAuction(lots);
  }
}
