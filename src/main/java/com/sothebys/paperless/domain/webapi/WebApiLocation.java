package com.sothebys.paperless.domain.webapi;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WebApiLocation {
  private String locationPath;
  private String name;
}
