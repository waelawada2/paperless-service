package com.sothebys.paperless.domain.webapi;

public enum WebApiAuctionState {
  SCH, PRO, OVR, CLS
}
