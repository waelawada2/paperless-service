package com.sothebys.paperless.domain.webapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
public class WebApiBidLot {

  private static final String PREVIEW = "Preview";

  private String lotNumber;
  private String guaranteeLine;
  private String largeImage;
  private Long estimateHigh;
  private Long estimateLow;
  private String currency;
  private String executed;
  private String medium;
  private String lotPath;
  
  /**
   * Asserts whether a WebApiBidLot has an proper value set on "lotNumber" field.
   * 
   * @return true if lotNumber value is incorrect, false otherwise
   */
  public boolean isPreview() {
    return PREVIEW.equalsIgnoreCase(getLotNumber());
  }

  /**
   * Asserts whether a WebApiBidLot has an improper value set on "lotNumber"
   * field.
   * 
   * @return true if lotNumber value is correct, false otherwise
   */
  public boolean notPreview() {
    return !isPreview();
  }

}
