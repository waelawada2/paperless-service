package com.sothebys.paperless.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
public class BidClient {

  @NotNull
  private String clientNumber;
  private Integer clientLevel;
  @NotNull
  private String paddle;
  private String partyName;
  private String kcm;
  private String partyType;
  private String languagePreference;
  private String primaryContactName;
  private String preferredPhone;
  private String otherPhone;
  private String email;
  private Long coverBid;
  private String coverBidCurrency;
  private Long absenteeBid;

  public BidClient(String clientNumber, String paddle, Long absenteeBid) {
    this.clientNumber = clientNumber;
    this.paddle = paddle;
    this.absenteeBid = absenteeBid;
  }

}
