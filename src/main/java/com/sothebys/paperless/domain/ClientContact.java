package com.sothebys.paperless.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
public class ClientContact {

  private String clientNumber;

  private String partyName;

  private String primaryContactName;

  private String partyType;
  
  public String getPartyName() {
    if (this.partyName == null) {
      return String.format("No data for client %s", clientNumber);
    } else {
      return this.partyName;
    }
  }
  
  public String getPartyType() {
    if (this.partyType == null) {
      return "individual";
    } else {
      return this.partyType;
    }
  }
  
  public String getPrimaryContactName() {
    if (this.primaryContactName == null) {
      return String.format("No data for client %s", clientNumber);
    } else {
      return this.primaryContactName;
    }
  }

  public String getDisplayName() {
    return getPartyName();
  }
}
