package com.sothebys.paperless.domain;

import java.util.Arrays;

public enum AuctionTypes {
  S, A;
  
  public static boolean isAllowed(String typeCode) {
    return Arrays.asList(values()).stream()
      .anyMatch(at -> at.name().equalsIgnoreCase(typeCode));
  }
}
