package com.sothebys.paperless.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuctionDTO {

  private String auctionId;
  private String auctionName;
  private Integer auctionSessions;
  private AuctionSessionState state;
  
  @JsonFormat
  (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
  private Date auctionDate;
  private String auctionLocation;

}
