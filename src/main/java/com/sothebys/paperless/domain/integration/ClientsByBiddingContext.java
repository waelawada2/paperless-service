package com.sothebys.paperless.domain.integration;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientsByBiddingContext {
  
  private List<ClientByBiddingContext> clients;

}
