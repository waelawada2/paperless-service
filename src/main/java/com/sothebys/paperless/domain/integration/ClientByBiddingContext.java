package com.sothebys.paperless.domain.integration;

import java.util.List;
import lombok.Data;

@Data
public class ClientByBiddingContext {
  private List<String> lots;
  private BiddingClient client;
}
