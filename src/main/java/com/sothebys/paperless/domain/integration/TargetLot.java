package com.sothebys.paperless.domain.integration;

import lombok.Data;

@Data
public class TargetLot {

  private Integer lotNumber;
  private String lotAuthor;
  private String lotDescription;

}
