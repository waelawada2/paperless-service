package com.sothebys.paperless.domain.integration;

import com.sothebys.paperless.domain.AuctionDTO;
import com.sothebys.paperless.domain.BidLot;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BidLots {
  
  private List<BidLot> lots;
  private AuctionDTO auction;

}
