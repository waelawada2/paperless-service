package com.sothebys.paperless.domain.integration;

import lombok.Data;

@Data
public class BiddingClient {

  private Long clientNumber;
  private Integer clientLevel;
  private String paddle;
  private String partyName;
  private String firstName;
  private String lastName;
  private String kcm;
  private String partyType;
  private String languagePreference;
  private String contactFirstName;
  private String contactLastName;
  private String contactPreferredPhone;
  private String contactOtherPhone;
  private String contactEmail;
  private Long coverBid;
  private String coverBidCurrency;

}
