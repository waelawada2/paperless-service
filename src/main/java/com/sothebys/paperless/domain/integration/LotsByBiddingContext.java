package com.sothebys.paperless.domain.integration;

import java.util.List;
import lombok.Data;

@Data
public class LotsByBiddingContext {

  private String agentId;
  private String auctionId;
  private List<TargetLot> lots;

}
