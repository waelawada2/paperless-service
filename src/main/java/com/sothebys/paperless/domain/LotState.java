package com.sothebys.paperless.domain;

public enum LotState {
  SCHEDULED, OPEN, CONFIRMED_BY_BIDDER, CLOSED, WITHDRAWN, PENDING;
}
