package com.sothebys.paperless.domain;

public enum LotExecutionResult {
  NOBID, UNDERBID, WON;
}
