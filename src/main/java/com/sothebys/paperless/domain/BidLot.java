package com.sothebys.paperless.domain;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
public class BidLot {

  private String lotId;
  private String lotDescription;
  private String lotAuthor;
  private Long estimateLow;
  private Long estimateHigh;
  private String currency;
  private String imageUrl;
  private String executed;
  private String medium;
  private LotState state = LotState.SCHEDULED;
  private LotExecutionResult lotExecutionResult;
  private long lotExecutionAmount;
  private LotClosureResult lotClosureResult;
  private String lotClosurePaddle;
  private long lotClosureAmount;
  private ClientContact clientContact;
  private Long reservePrice;
  private List<AbsenteeBid> absenteeBidders;
  private List<AgentDTO> telephoneBidders;
  private boolean isWithdrawn;

  public BidLot(String lotId, long amount, LotExecutionResult lotResult, LotState state) {
    this.lotId = lotId;
    this.lotExecutionAmount = amount;
    this.lotExecutionResult = lotResult;
    this.state = state;
  }

  public BidLot(String lotId, long reservePrice) {
    this.lotId = lotId;
    this.reservePrice = reservePrice;
  }

}
