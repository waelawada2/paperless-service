package com.sothebys.paperless.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AbsenteeBid {

  private String clientNumber;
  private String paddle;
  private Long amount;
  private String registrationDatetime;
  private Boolean isReserve;
  
}
