package com.sothebys.paperless.domain;

public enum LotClosureResult {
  SLD, BI;
}
