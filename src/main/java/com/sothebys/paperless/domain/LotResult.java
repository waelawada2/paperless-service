package com.sothebys.paperless.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
public class LotResult {

  @NotNull
  private BidClient client;
  @NotNull
  private LotExecutionResult result;
  @NotNull
  private Long amount;
}
