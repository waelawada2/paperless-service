package com.sothebys.paperless.domain.conversion;

import com.sothebys.paperless.domain.BidLot;
import com.sothebys.paperless.domain.webapi.WebApiBidLot;
import com.sothebys.paperless.exceptions.WebApiResourceException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

public class WebApiBidLotToBidLot implements Converter<WebApiBidLot, BidLot> {

  @Override
  public BidLot convert(WebApiBidLot webApiLot) {
    BidLot lot = new BidLot();

    if (StringUtils.isEmpty(webApiLot.getLotNumber())) {
      throw new WebApiResourceException("cannot read lotNumber");
    }

    if (StringUtils.isEmpty(webApiLot.getGuaranteeLine())) {
      throw new WebApiResourceException("cannot read guaranteeLine");
    }

    lot.setLotId(webApiLot.getLotNumber());
    String[] guaranteedItems = webApiLot.getGuaranteeLine().split("\\|");
    if (guaranteedItems.length == 2) {
      if (!guaranteedItems[0].trim().equalsIgnoreCase(guaranteedItems[1].trim())) {
        lot.setLotDescription(guaranteedItems[1].trim());
      }
    }
    lot.setLotAuthor(guaranteedItems[0].trim());

    // Estimate price
    lot.setEstimateLow(webApiLot.getEstimateLow());
    lot.setEstimateHigh(webApiLot.getEstimateHigh());
    lot.setCurrency(webApiLot.getCurrency());

    // Image
    lot.setImageUrl(webApiLot.getLargeImage());
    
    // medium and executed
    lot.setExecuted(webApiLot.getExecuted());
    lot.setMedium(webApiLot.getMedium());

    return lot;
  }

}
