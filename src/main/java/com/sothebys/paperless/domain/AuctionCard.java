package com.sothebys.paperless.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class AuctionCard {

  private String auctionId;
  private String auctionName;
  private Integer auctionSessions;
  private Integer session;
  private String lots;
  private String date;
  private String time;
  private long openAuctionTried;
  private long openAuctionWindowStarts;
  private long openAuctionWindowEnds;
  private String location;
  @JsonIgnore
  private ZonedDateTime zonedDateTime;
  private AuctionSessionState state = AuctionSessionState.SCHEDULED;
  private ZonedDateTime closeDateTime;

  public AuctionCard(String auctionId, int sessionNumber) {
    this.auctionId = auctionId;
    this.session = sessionNumber;
  }

}
