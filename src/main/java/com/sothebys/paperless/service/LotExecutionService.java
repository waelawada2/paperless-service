package com.sothebys.paperless.service;

import com.sothebys.paperless.domain.BidLot;
import com.sothebys.paperless.domain.LotResult;
import com.sothebys.paperless.domain.User;

public interface LotExecutionService {

  /**
   * Confirms a lot execution after a phone bidder's action. Said action can be either confirm
   * bidder's client participation on given lot as WIN, UNDERBID or NOBID.
   * 
   * @param lotExecutionResult a summary of the lot execution information: paddle, client, amount,
   *        result.
   * @param auctionId the id of the auction the lot belongs to
   * @param sessionNumber the session number
   * @param lotId the id of the lot the client just participated in
   * @param agent the user containing agent data
   * @return BidLot with the confirmed lot data
   */
  BidLot confirmLotExecution(LotResult lotExecutionResult, String auctionId, int sessionNumber,
      String lotId, User agent);

}
