package com.sothebys.paperless.service;

import com.sothebys.paperless.domain.BidLot;
import com.sothebys.paperless.persistence.entity.LotResult;

/**
 * The Lot service interface
 */
public interface LotService {

  /**
   * Retrieves information related to an individual lot, provided the auction-sale and lot entries
   * are found in DB.
   * 
   * @param auctionId the identifier for the auction
   * @param sessionNumber the session number the lot belongs to
   * @param lotId the lot identifier
   * @return an instance of BidLot, containing reserve price and state.
   */
  BidLot getLotData(String auctionId, int sessionNumber, String lotId);

  /**
   * Attempts to close a lot as per request from REST client hammer app. It takes the BidLot
   * instance to extract the details about the closure.
   * 
   * @param auctionId the auctionId the lot belongs to
   * @param sessionNumber the session where this closure occured
   * @param lotId the Id of the lot to be closed
   * @param lotResult the extra data pertaining the lot closure event
   * @return same instance of LotResult, with data updated to the value that reflects changes done
   *         to DB.
   */
  LotResult closeLot(String auctionId, int sessionNumber, String lotId, LotResult lotResult);

}
