package com.sothebys.paperless.service;

import com.sothebys.paperless.domain.AuctionCard;
import com.sothebys.paperless.domain.AuctionCards;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.domain.integration.BidLots;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;

/**
 * The Auction service interface.
 */
public interface AuctionService {

  /**
   * Returns a list of BidLots assigned to a given phone bidder agent, for a given auction, for a
   * given session
   * 
   * @param auctionId The auction we want to get the lots from, including the session we want to
   *        filter the auction lots by
   * @param sessionNumber the session number
   * @param agent the phone bidder agent
   * @return the list of lots for the specified agent
   */
  BidLots getLotsByAgent(String auctionId, int sessionNumber, User agent);

  /**
   * * Returns a list of lots a given client is interested in, for a given auction, for a given
   * session
   * 
   * @param auctionId The auction we want to get the lots from
   * @param sessionNumber the session we want to filter the auction lots by
   * @param clientId the bidding client
   * @return the list of lots for the specified client
   */
  BidLots getLotsByClient(String auctionId, int sessionNumber, long clientId);

  /**
   * Returns a list of all BidLots in an auction, for a given session
   * 
   * @param auctionId The auction we want to get the lots from
   * @param sessionNumber the session we want to filter the auction lots by
   * @return the list of lots
   */
  BidLots getLotsByAuction(String auctionId, int sessionNumber);

  /**
   * Return a list of cards for the all upcoming auctions.
   * 
   * @return the list of auction cards
   */
  AuctionCards getAllAuctionCards();

  /**
   * Return a list of cards of auctions assigned to an agent.
   * 
   * @param agent The agent requesting the operation
   * @return the list of auction cards assigned to the agent.
   */
  AuctionCards getAuctionCardsForAgent(User agent);

  /**
   * Changes the state of the AuctionSession identified by auctionCard.auctionId and
   * auctionCard.session to OPEN. Also, all lots associated to the Auction are also updated to state
   * OPEN.
   * 
   * @param auctionCard an AuctionCard containing the id of the AuctionSession to be updated, also
   *        the session number
   * @param auctioneer userName that performed the operation
   */
  void openAuctionSession(AuctionCard auctionCard, User auctioneer);

  /**
   * Changes the state of the AuctionSession identified by auctionCard.auctionId and
   * auctionCard.sssion to CLOSE.
   * 
   * @param auctionCard an AuctionCard containing the id of the AuctionSession to be updated, also
   *        the session number
   * @param auctioneer the auctioneer login name performing the operation
   */
  void closeAuctionSession(AuctionCard auctionCard, User auctioneer);

  /**
   * Get the list of closed auctions cards filtered by state.
   * 
   *  @param state the state to filter the auctionsessions
   *  @return a wrapper object of type AuctionCards containing the prepared cards data
   */
  AuctionCards getAllAuctionCardsByState(AuctionSessionState state);

}
