package com.sothebys.paperless.service;

import org.springframework.stereotype.Service;

@Service
public interface ReportService {

  /**
   * Generate csv report for auction session.
   * 
   * @param auctionId the auction id
   * @param sessionNumber the session number
   * @return the csv report
   */
  public String getCSVReport(String auctionId, int sessionNumber);

  /**
   * Returns the name for the report file
   * @param auctionId the auction id
   * @param sessionNumber the session number
   * @return the report file name
   */
  public String getReportFileName(String auctionId, int sessionNumber);

}
