package com.sothebys.paperless.service.impl;

import com.sothebys.paperless.client.ClientApiClient;
import com.sothebys.paperless.client.SAPClient;
import com.sothebys.paperless.configuration.toggle.ToggleRouter;
import com.sothebys.paperless.domain.AuctionCard;
import com.sothebys.paperless.domain.AuctionCards;
import com.sothebys.paperless.domain.AuctionDTO;
import com.sothebys.paperless.domain.BidLot;
import com.sothebys.paperless.domain.ClientContact;
import com.sothebys.paperless.domain.LotState;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.domain.clientapi.ClientApiData;
import com.sothebys.paperless.domain.integration.BidLots;
import com.sothebys.paperless.domain.sap.SapBidder;
import com.sothebys.paperless.domain.sap.SapSaleRecord;
import com.sothebys.paperless.exceptions.AuctionNotFoundException;
import com.sothebys.paperless.exceptions.AuctionSessionStateUpdateException;
import com.sothebys.paperless.persistence.entity.Auction;
import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.AuctionSessionId;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import com.sothebys.paperless.persistence.entity.Feature;
import com.sothebys.paperless.persistence.entity.Lot;
import com.sothebys.paperless.persistence.entity.LotExecution;
import com.sothebys.paperless.persistence.entity.LotExecutionId;
import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.entity.LotResult;
import com.sothebys.paperless.persistence.repository.AuctionRepository;
import com.sothebys.paperless.persistence.repository.AuctionSessionRepository;
import com.sothebys.paperless.persistence.repository.LotExecutionRepository;
import com.sothebys.paperless.persistence.repository.LotRepository;
import com.sothebys.paperless.persistence.repository.LotResultRepository;
import com.sothebys.paperless.service.AuctionService;
import com.sothebys.paperless.util.ComparableString;
import com.sothebys.paperless.util.PaddedSourceNumberFormatter;
import com.sothebys.paperless.util.ZonedDateTimeFormatter;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;

/**
 * Auction service implementation.
 */
@AllArgsConstructor
@Slf4j
@Service
public class AuctionServiceImpl implements AuctionService {

  private final AuctionRepository auctionRepository;
  private final AuctionSessionRepository auctionSessionRepository;
  private final ClientApiClient clientApiClient;
  private final LocalStateManager localStateManager;
  private final LotExecutionRepository lotExecutionRepository;
  private final LotRepository lotRepository;
  private final LotResultRepository lotResultRepository;
  private final PaddedSourceNumberFormatter amountFormatter;
  private final SAPClient sapClient;
  private final ToggleRouter featureToggleRouter;
  private final ZonedDateTimeFormatter auctionCardZonedDateTime;

  /**
   * {@inheritDoc}
   */
  @Override
  public BidLots getLotsByAgent(String auctionId, int sessionNumber, User agent) {
    log.debug("Serving: getLotsByAgent(sale={}, session={}, agent={})", auctionId, agent,
        sessionNumber);

    StopWatch stopWatch = new StopWatch("getLotsByAgent");

    stopWatch.start("auctionRepository.findOptionalById");
    Auction auction = auctionRepository.findOptionalById(auctionId)
        .orElseThrow(() -> new AuctionNotFoundException(auctionId));
    stopWatch.stop();

    stopWatch.start("sapClient.getAssignedLotsForAgent");
    List<SapBidder> agentAssignmentsFromSap =
        sapClient.getAssignedLotsForAgent(auctionId, agent.getEmail());
    stopWatch.stop();

    Set<String> agentLotsIds =
        agentAssignmentsFromSap.stream().map(a -> a.getLotNum()).collect(Collectors.toSet());

    stopWatch.start("sapClient.getSaleStatusReserveEstimates & filter assignments");
    Map<String, Boolean> withdrawnAssertionsByLotId =
        sapClient.getSaleStatusReserveEstimates(auctionId).stream()
            .filter(s -> agentLotsIds.contains(s.getLotNum()))
            .collect(Collectors.toMap(a -> amountFormatter.unpad(a.getLotNum()),
                a -> a.isWithdrawn()));
    stopWatch.stop();

    stopWatch.start("mapping assigned lots and clients");
    Map<LotId, String> assignedClientIdsByLotIds = agentAssignmentsFromSap.stream()
        .collect(Collectors.toMap(
            al -> new LotId(amountFormatter.unpad(al.getLotNum()), auctionId, sessionNumber),
            al -> amountFormatter.unpad(al.getClient())));
    stopWatch.stop();

    stopWatch.start("lotRepository.findAllByIdIn");
    List<Lot> assignedLots = lotRepository.findAllByIdIn(assignedClientIdsByLotIds.keySet());
    stopWatch.stop();

    List<BidLot> lots = assignedLots.stream().map(lot -> {
      String clientId = assignedClientIdsByLotIds.get(lot.getId());
      BidLot bidLot = mapToBidLot(clientId, lot, agent, auction);
      if (withdrawnAssertionsByLotId.containsKey(lot.getId().getLotId())) {
        boolean isWithdrawn = withdrawnAssertionsByLotId.get(lot.getId().getLotId());
        if (isWithdrawn) {
          log.trace("marking withdrawn lot {} ({}-{})", lot.getId().getLotId(), auctionId,
              sessionNumber);
          bidLot.setWithdrawn(isWithdrawn);
        }
      }
      return bidLot;
    }).sorted((a, b) -> new ComparableString(a.getLotId()).compareTo(b.getLotId()))
        .collect(Collectors.toList());

    AuctionSession auctionSession = this.getAuctionSessionFromAuction(auction, sessionNumber);
    BidLots result = new BidLots(lots,
        new AuctionDTO(auction.getId(), auction.getName(), auction.getTotalSessions(),
            auctionSession.getState(), auction.getDate(), auction.getLocation()));

    log.trace("{}", stopWatch);
    return result;
  }

  /**
   * Uses data from public api and sap to populate a BidLot instance to be fed to agent assigned lot
   * list
   * 
   * @param clientId
   * @param lot
   * @param agentId
   * @param auctionId
   * @param sessionNumber
   * @return
   */
  private BidLot mapToBidLot(String clientId, Lot lot, User agent, Auction auction) {
    String auctionId = auction.getId();
    int sessionNumber = lot.getId().getSessionId();
    BidLot bidLot = new BidLot();
    bidLot.setLotId(lot.getId().getLotId());
    bidLot.setLotDescription(lot.getLotDescription());
    bidLot.setLotAuthor(lot.getLotAuthor());
    bidLot.setEstimateLow(lot.getEstimateLow());
    bidLot.setEstimateHigh(lot.getEstimateHigh());
    bidLot.setImageUrl(lot.getImageUrl());
    bidLot.setMedium(lot.getMedium());
    bidLot.setExecuted(lot.getExecuted());
    bidLot.setCurrency(auction.getCurrency());
    // Check if there is a lot execution (from bidder)
    StopWatch stopWatch = new StopWatch("mapToBidLot");
    stopWatch.start("lotExecutionRepository.findOptionalById #" + lot.getId().getLotId());
    Optional<LotExecution> lotExecution =
        lotExecutionRepository.findOptionalById(new LotExecutionId(
            String.valueOf(agent.getUsername()), lot.getId().getLotId(), auctionId, sessionNumber));
    lotExecution.ifPresent(le -> {
      bidLot.setLotExecutionAmount(le.getExecutionAmount());
      bidLot.setLotExecutionResult(le.getExecutionResult());
    });
    stopWatch.stop();

    // Also check if is there a lot result (from Auctioneer)

    stopWatch.start("lotResultRepository.findOne #" + lot.getId().getLotId());
    LotResult lotResult = lotResultRepository.findOne(lot.getId());
    stopWatch.stop();

    if (Optional.ofNullable(lotResult).isPresent()) {
      bidLot.setLotClosureAmount(lotResult.getHammerPrice());
      bidLot.setLotClosureResult(lotResult.getStatus());
    }
    bidLot.setState(this.localStateManager.deriveLotState(Optional.ofNullable(lotResult),
        lotExecution, lot.getAuctionSession().getState()));
    try {

      stopWatch.start("clientApiClient.findHolderPartyByAccountNumber #" + lot.getId().getLotId());
      Optional<ClientApiData> clientData =
          Optional.ofNullable(clientApiClient.findHolderPartyByAccountNumber(clientId));
      clientData.ifPresent((c) -> {
        ClientContact clientContact = new ClientContact(clientId, c.getPartyName(),
            c.getPartyName(), c.getPartyType().toLowerCase());
        bidLot.setClientContact(clientContact);
      });
      stopWatch.stop();

    } catch (Exception e) {
      log.error("no client data available for accountNumber: {}", clientId);
      ClientContact clientContact = new ClientContact();
      clientContact.setClientNumber(clientId);
      bidLot.setClientContact(clientContact);
    }
    return bidLot;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public BidLots getLotsByAuction(String auctionId, int sessionNumber) {
    Auction auction = auctionRepository.findOptionalById(auctionId)
        .orElseThrow(() -> new AuctionNotFoundException(auctionId));
    AuctionSession session = this.getAuctionSessionFromAuction(auction, sessionNumber);
    List<Lot> dbLots = lotRepository.findByAuctionSession(session);
    List<BidLot> lots = dbLots.stream().map(lot -> {
      BidLot bidLot = new BidLot();
      bidLot.setLotId(lot.getId().getLotId());
      bidLot.setLotDescription(lot.getLotDescription());
      bidLot.setLotAuthor(lot.getLotAuthor());
      bidLot.setEstimateLow(lot.getEstimateLow());
      bidLot.setEstimateHigh(lot.getEstimateHigh());
      bidLot.setCurrency(auction.getCurrency());
      bidLot.setImageUrl(lot.getImageUrl());
      bidLot.setMedium(lot.getMedium());
      bidLot.setExecuted(lot.getExecuted());
      SapSaleRecord sapRecord =
          sapClient.getLotStatusReserveEstimates(auctionId, lot.getId().getLotId());
      if (sapRecord.isWithdrawn()) {
        bidLot.setWithdrawn(true);
      }
      bidLot.setState(this.localStateManager.deriveLotState(Optional.ofNullable(lot.getLotResult()),
          lot.getAuctionSession().getState()));
      // Check if is there a lot result (from Auctioneer)
      Optional.ofNullable(lot.getLotResult()).ifPresent(result -> {
        bidLot.setLotClosureAmount(result.getHammerPrice());
        bidLot.setLotClosureResult(result.getStatus());
      });
      return bidLot;
    }).sorted((a, b) -> Integer.valueOf(a.getLotId()).compareTo(Integer.valueOf(b.getLotId())))
        .collect(Collectors.toList());
    AuctionDTO domainAuction = new AuctionDTO(auction.getId(), auction.getName(),
        auction.getTotalSessions(), session.getState(), auction.getDate(), auction.getLocation());
    BidLots result = new BidLots(lots, domainAuction);
    return result;
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public BidLots getLotsByClient(String auctionId, int sessionNumber, long clientId) {
    log.debug("Serving: getLotsByClient(sale={}, session={}, client={}): ", auctionId,
        sessionNumber, clientId);

    StopWatch stopWatch = new StopWatch("getLotsByClient");
    stopWatch.start("sapClient.getLotsOfInterestForClient");
    List<SapBidder> lotsOfInterestForClient =
        sapClient.getLotsOfInterestForClient(auctionId, Long.toString(clientId));
    stopWatch.stop();

    Set<LotId> lotIds = lotsOfInterestForClient.stream()
        .map(sb -> new LotId(amountFormatter.unpad(sb.getLotNum()), auctionId, sessionNumber))
        .collect(Collectors.toSet());

    stopWatch.start("lotRepository.findAllByIdIn");
    List<BidLot> lotsData = lotRepository.findAllByIdIn(lotIds).stream().map(lot -> {
      BidLot bidLot = new BidLot();
      bidLot.setLotId(lot.getId().getLotId());
      bidLot.setLotDescription(lot.getLotDescription());
      bidLot.setLotAuthor(lot.getLotAuthor());
      SapSaleRecord sapRecord =
          sapClient.getLotStatusReserveEstimates(auctionId, lot.getId().getLotId());
      if (sapRecord.isWithdrawn()) {
        bidLot.setWithdrawn(true);
      }
      return bidLot;
    }).sorted((a, b) -> new ComparableString(a.getLotId()).compareTo(b.getLotId()))
      .collect(Collectors.toList());
    stopWatch.stop();

    log.trace("{}", stopWatch);
    return new BidLots(lotsData, null);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AuctionCards getAllAuctionCards() {
    log.debug("Serving getAllAuctionCards()");

    List<Auction> auctions = auctionRepository.findAll();

    AuctionCards auctionCards = prepareAuctionCards(auctions);

    return auctionCards;

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AuctionCards getAuctionCardsForAgent(User agent) {

    StopWatch stopWatch = new StopWatch("getAuctionCardsForAgent");
    Stream<Auction> auctionsStream = Stream.empty();

    stopWatch.start("sapClient.getSalesByAgent");
    List<SapBidder> registeredBiddingLots = sapClient.getSalesByAgent(agent.getEmail());
    stopWatch.stop();

    if (Optional.ofNullable(registeredBiddingLots).isPresent()) {
      stopWatch.start("finding DB auctions");
      Set<String> auctionIds =
          registeredBiddingLots.stream().map(s -> s.getSaleNum()).collect(Collectors.toSet());
      auctionsStream = auctionRepository.findByIdIn(auctionIds).stream();
      stopWatch.stop();
    }

    stopWatch.start("filtering and collecting");
    // filter and collect from stream
    List<Auction> auctions = auctionsStream.distinct().collect(Collectors.toList());
    stopWatch.stop();

    stopWatch.start("Prepare and filter cards");
    // Prepare cards
    Set<String> assignedLotIds = registeredBiddingLots.stream()
        .map(s -> amountFormatter.unpad(s.getLotNum())).sorted().collect(Collectors.toSet());
    AuctionCards auctionCards = prepareAuctionCards(auctions, assignedLotIds);
    stopWatch.stop();

    log.trace("{}", stopWatch);
    return auctionCards;
  }

  // Private Methods


  /**
   * Prepares the card information from the list of auctions in the DB.
   *
   * @param webApiAuctions the public API list of auctions
   * @return auction cards information
   */
  private AuctionCards prepareAuctionCards(List<Auction> auctions) {
    return prepareAuctionCards(auctions, null);
  }

  /**
   * Prepares the card information, filtering out sessions without lots assigned
   * 
   * @param auctions the auctions used as source to create the cards
   * @param assignedLotIds a set containing the ids of the assigned lots as returned by SAP
   * @return the filtered cards depending on assigned lots within each session
   */
  private AuctionCards prepareAuctionCards(List<Auction> auctions, Set<String> assignedLotIds) {

    // Create cards, one per each session of an auction
    List<AuctionSession> auctionSessions = auctions.stream()
        .flatMap(a -> a.getAuctionSessions().stream().map(as -> as)).collect(Collectors.toList());

    Stream<AuctionSession> auctionSessionStream = auctionSessions.stream();
    if (Optional.ofNullable(assignedLotIds).isPresent()) {
      auctionSessionStream = auctionSessionStream.filter(as -> {
        boolean sessionHasLotsAssigned = false;
        long assignedLotsForThisSession = assignedLotIds.stream()
            .filter(s -> new ComparableString(s).withinRange(as.getStartLot(), as.getEndLot()))
            .count();
        log.trace("filtering auction-session {} by assigned lots, found {}",
            as.getAuction().getId() + "-" + as.getId().getSessionId(), assignedLotsForThisSession);
        sessionHasLotsAssigned = assignedLotsForThisSession >= 1;
        return sessionHasLotsAssigned;
      });
    }
    List<AuctionCard> auctionCards = auctionSessionStream.map(as -> {
      AuctionCard card = new AuctionCard();
      card.setAuctionId(as.getAuction().getId());
      card.setAuctionName(as.getAuction().getName());
      if (Optional.ofNullable(as.getAuction().getLocation()).isPresent()) {
        card.setLocation(as.getAuction().getLocation());
      }
      ZonedDateTime zonedDateTime =
          auctionCardZonedDateTime.of(as.getStartDate().getTime(), card.getLocation());
      card.setAuctionSessions(as.getAuction().getTotalSessions());
      card.setSession(as.getId().getSessionId());
      card.setZonedDateTime(zonedDateTime);
      card.setOpenAuctionWindowEnds(as.getOpenTimeWindowEnd());
      card.setOpenAuctionWindowStarts(as.getOpenTimeWindowStart());
      card.setDate(auctionCardZonedDateTime.formatCardDate(zonedDateTime));
      card.setTime(auctionCardZonedDateTime.formatCardTime(zonedDateTime));
      String lots;
      if (as.getAuction().getTotalSessions() > 1) {
        lots = String.join(" - ", as.getStartLot(), as.getEndLot());
      } else {
        lots = String.valueOf(as.getAuction().getTotalLots());
      }
      card.setLots(lots);
      card.setState(as.getState());
      Optional.ofNullable(as.getCloseDate()).ifPresent(closeDate -> {
        ZonedDateTime closeDateTime =
            auctionCardZonedDateTime.of(as.getCloseDate().getTime(), card.getLocation());
        card.setCloseDateTime(closeDateTime);
      });
      return card;
    }).collect(Collectors.toList());

    return new AuctionCards(sortByStateAndZonedDateTime(auctionCards),
        extractLocationsList(auctions));
  }

  private List<AuctionCard> sortByStateAndZonedDateTime(List<AuctionCard> auctionCards) {
    auctionCards.sort(
        Comparator.comparing(AuctionCard::getState).thenComparing(AuctionCard::getZonedDateTime));
    return auctionCards;
  }

  private Set<String> extractLocationsList(List<Auction> auctions) {
    Set<String> locations = auctions.stream().map(Auction::getLocation).collect(Collectors.toSet());
    return locations;
  }

  @Override
  @Transactional
  public void openAuctionSession(AuctionCard auctionCard, User auctioneer) {
    log.debug("serving openAuctionSession({})", auctionCard);

    long check = auctionCard.getOpenAuctionTried();
    long start = auctionCard.getOpenAuctionWindowStarts();
    long end = auctionCard.getOpenAuctionWindowEnds();
    if (featureToggleRouter.isFeatureEnabled(Feature.VALIDATE_OPEN_AUCTION_TIME_WINDOW)
        && !auctionCardZonedDateTime.isWithinWindow(check, start, end)) {
      throw new AuctionSessionStateUpdateException(
          String.format("%s %s", auctionCard.getAuctionId(), auctionCard.getSession()),
          auctionCard.getState(), check);
    }

    String auctionId = auctionCard.getAuctionId();
    int session = auctionCard.getSession();

    AuctionSessionId auctionSessionId = new AuctionSessionId(auctionId, session);

    AuctionSession auctionSession =
        Optional.ofNullable(auctionSessionRepository.findOne(auctionSessionId))
            .orElseThrow(() -> new AuctionSessionStateUpdateException(auctionId, session));

    if (auctionSession.getState() == AuctionSessionState.OPEN) {
      throw new AuctionSessionStateUpdateException(auctionId, session, auctionCard.getState());
    }

    auctionSession.setOpenDate(new Date());
    auctionSession.setOpeningAuctioneer(auctioneer.getUsername());
    auctionSession.setState(AuctionSessionState.OPEN);

    auctionSessionRepository.save(auctionSession);
  }

  @Override
  public void closeAuctionSession(AuctionCard auctionCard, User auctioneer) {
    String auctionId = auctionCard.getAuctionId();
    int session = auctionCard.getSession();

    AuctionSessionId auctionSessionId = new AuctionSessionId(auctionId, session);

    AuctionSession auctionSession =
        Optional.ofNullable(auctionSessionRepository.findOne(auctionSessionId))
            .orElseThrow(() -> new AuctionSessionStateUpdateException(auctionId, session));

    List<Lot> lotsInAuction =
        lotRepository.findByAuctionSession(new AuctionSession(auctionId, session));

    boolean isFeatureEnabled =
        featureToggleRouter.isFeatureEnabled(Feature.VALIDATE_CLOSE_AUCTION_LOTS_CLOSED);
    // Check that all lots are CLOSED before attempting to close an auction
    if (isFeatureEnabled && !validateAllLotsAreInState(lotsInAuction, LotState.CLOSED)) {
      throw new AuctionSessionStateUpdateException(auctionCard.getAuctionId(),
          auctionCard.getSession(), auctionCard.getState());
    }

    if (auctionSession.getState() == AuctionSessionState.CLOSED) {
      throw new AuctionSessionStateUpdateException(auctionId, session, auctionCard.getState());
    }

    auctionSession.setState(AuctionSessionState.CLOSED);
    auctionSession.setCloseDate(new Date());
    auctionSession.setClosingAuctioneer(auctioneer.getUsername());
    auctionSessionRepository.save(auctionSession);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AuctionCards getAllAuctionCardsByState(AuctionSessionState state) {
    List<AuctionSession> auctionSessions = auctionSessionRepository.getByState(state);
    List<Auction> auctions = auctionSessions.stream().map(AuctionSession::getAuction).distinct()
        .collect(Collectors.toList());

    List<AuctionCard> auctionCards = prepareAuctionCards(auctions).getCards();

    // Remove any session that doesn't have the desired state
    auctionCards =
        auctionCards.stream().filter(c -> c.getState() == state).collect(Collectors.toList());

    if (state.equals(AuctionSessionState.CLOSED)) {
      // If auctions are closed, sort them by close datetime
      auctionCards = sortByCloseDateTimeDescending(auctionCards);
    } else {
      auctionCards = sortByStateAndZonedDateTime(auctionCards);
    }

    AuctionCards cards = new AuctionCards(auctionCards, extractLocationsList(auctions));
    return cards;
  }

  private List<AuctionCard> sortByCloseDateTimeDescending(List<AuctionCard> auctionCards) {
    auctionCards.sort(Comparator.comparing(AuctionCard::getCloseDateTime).reversed());
    return auctionCards;
  }

  private Boolean validateAllLotsAreInState(List<Lot> lots, LotState expectedState) {
    return lots.stream().filter(l -> {
      SapSaleRecord sapLotData =
          sapClient.getLotStatusReserveEstimates(l.getId().getAuctionId(), l.getId().getLotId());
      return !(sapLotData.isWithdrawn());
    }).allMatch(lot -> {
      LotState lotState = localStateManager.deriveLotState(Optional.ofNullable(lot.getLotResult()),
          lot.getAuctionSession().getState());
      return lotState == expectedState;
    });
  }

  private AuctionSession getAuctionSessionFromAuction(Auction auction, int sessionNumber) {
    return auction.getAuctionSessions().stream()
        .filter(as -> as.getId().getSessionId() == sessionNumber).findFirst().get();
  }

}
