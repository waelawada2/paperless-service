package com.sothebys.paperless.service.impl;

import com.sothebys.paperless.client.SAPClient;
import com.sothebys.paperless.domain.AbsenteeBid;
import com.sothebys.paperless.domain.AgentDTO;
import com.sothebys.paperless.domain.BidLot;
import com.sothebys.paperless.domain.sap.SapBidder;
import com.sothebys.paperless.domain.sap.SapSaleRecord;
import com.sothebys.paperless.exceptions.AuctionNotFoundException;
import com.sothebys.paperless.exceptions.LotClosureException;
import com.sothebys.paperless.exceptions.LotNotFoundException;
import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.AuctionSessionId;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import com.sothebys.paperless.persistence.entity.Lot;
import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.entity.LotResult;
import com.sothebys.paperless.persistence.repository.AuctionSessionRepository;
import com.sothebys.paperless.persistence.repository.LotRepository;
import com.sothebys.paperless.persistence.repository.LotResultRepository;
import com.sothebys.paperless.service.LotService;
import com.sothebys.paperless.util.AbsenteeBidsFormatter;
import com.sothebys.paperless.util.PaddedSourceNumberFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.transaction.annotation.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

@Slf4j
@Service
@AllArgsConstructor
public class LotServiceImpl implements LotService {

  private final AbsenteeBidsFormatter absenteeBidsFormatter;
  private final LotRepository lotRepository;
  private final LocalStateManager localStateManager;
  private final LotResultRepository lotResultRepository;
  private final PaddedSourceNumberFormatter amountFormatter;
  private final SAPClient sapClient;
  private final AuctionSessionRepository auctionSessionRepository;

  /**
   * {@inheritDoc}
   */
  @Override
  public BidLot getLotData(String auctionId, int sessionNumber, String lotId) {
    log.debug("serving getBidLot(auctionId={}, sessionNumber={}, lotId={})", auctionId,
        sessionNumber, lotId);
    
    StopWatch stopWatch = new StopWatch("getLotData");

    stopWatch.start("auctionSessionRepository.findOptionalById");
    AuctionSession auctionSession =
        auctionSessionRepository.findOptionalById(new AuctionSessionId(auctionId, sessionNumber))
            .orElseThrow(() -> new AuctionNotFoundException(
                String.format("can't find auctionId=%s", auctionId)));
    stopWatch.stop();
    
    stopWatch.start("lotRepository.findOptionalById");
    Lot lot = lotRepository.findOptionalById(new LotId(lotId, auctionId, sessionNumber))
      .orElseThrow(() -> new LotNotFoundException(lotId, auctionId, sessionNumber));
    stopWatch.stop();

    stopWatch.start("local setting");
    BidLot bidLot = new BidLot();
    bidLot.setLotId(lotId);
    bidLot.setLotDescription(lot.getLotDescription());
    bidLot.setLotAuthor(lot.getLotAuthor());
    bidLot.setCurrency(auctionSession.getAuction().getCurrency());
    bidLot.setImageUrl(lot.getImageUrl());
    bidLot.setMedium(lot.getMedium());
    bidLot.setExecuted(lot.getExecuted());
    Optional<LotResult> lotResult = Optional.ofNullable(lot.getLotResult());
    lotResult.ifPresent( lr -> {
      bidLot.setLotClosureAmount(lr.getHammerPrice());
      bidLot.setLotClosureResult(lr.getStatus());
      bidLot.setLotClosurePaddle(lr.getPaddleNumber());
    });
    stopWatch.stop();

    stopWatch.start("sapClient.getLotStatusReserveEstimates");
    SapSaleRecord saleRecord = sapClient.getLotStatusReserveEstimates(auctionId, lotId);
    stopWatch.stop();
    
    if (saleRecord.isWithdrawn()) {
      bidLot.setWithdrawn(true);
    }

    stopWatch.start("formatting data");
    bidLot.setEstimateLow(amountFormatter.parseAmount(saleRecord.getLow()));
    bidLot.setEstimateHigh(amountFormatter.parseAmount(saleRecord.getHigh()));
    String reserve = Optional.ofNullable(saleRecord.getReserve()).orElse("0");
    bidLot.setReservePrice(amountFormatter.parseAmount(reserve));
    stopWatch.stop();

    stopWatch.start("localStateManager.deriveLotState");
    bidLot.setState(this.localStateManager.deriveLotState(
        lotResult, lot.getAuctionSession().getState()));
    stopWatch.stop();

    List<AbsenteeBid> absenteeList = new ArrayList<>();
    List<AgentDTO> phoneBidders = new ArrayList<>();

    stopWatch.start("sapClient.getAbsenteesAndPhoneBiddersForLot");
    List<SapBidder> sapBidders = sapClient.getAbsenteesAndPhoneBiddersForLot(auctionId, lotId);
    stopWatch.stop();

    stopWatch.start("populating absentee and phone bidders arrays");
    for (SapBidder bidder : sapBidders) {
      if (SAPClient.X.equalsIgnoreCase(bidder.getPhoneBid())) {
        // phone agent
        AgentDTO agentDto = new AgentDTO();
        agentDto.setName(bidder.getAssignUser());
        agentDto.setPaddleNumber(bidder.getPaddleNum());
        agentDto.setUserName(bidder.getAssignEmail());
        phoneBidders.add(agentDto);
      } else {
        // absentee
        AbsenteeBid absenteeBid = new AbsenteeBid();
        absenteeBid.setAmount(amountFormatter.parseAmount(bidder.getTopBid()));
        absenteeBid.setClientNumber(bidder.getClient());
        absenteeBid.setPaddle(bidder.getPaddleNum());
        absenteeBid
            .setRegistrationDatetime(String.format("%s%s", bidder.getBDate(), bidder.getBTime()));
        if (absenteeList.add(absenteeBid)) {
          log.debug("Added absenteeBid entry: {}", absenteeBid.toString());
        }
      }
    }
    absenteeList = absenteeBidsFormatter.formatAbsenteeBidList(bidLot.getReservePrice(), absenteeList);
    bidLot.setAbsenteeBidders(absenteeList);
    bidLot.setTelephoneBidders(phoneBidders);
    stopWatch.stop();
    log.trace("{}", stopWatch);
    return bidLot;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public LotResult closeLot(String auctionId, int sessionNumber, String lotId,
      LotResult lotResult) {

    AuctionSession auctionSession = auctionSessionRepository
        .findOptionalById(new AuctionSessionId(auctionId, sessionNumber))
        .orElseThrow(() -> new AuctionNotFoundException(
            String.format("Auction %s with session %s does not exist", auctionId, sessionNumber)));

    if (auctionSession.getState() == AuctionSessionState.CLOSED) {
      throw new LotClosureException(
          "You can not update the state of a lot when its auction is closed");
    }


    log.debug(
        "closeLot(auctionId={}, sessionNumber={}, String lotId={}, paddle={}, result={}, price={})",
        auctionId, sessionNumber, lotId, lotResult.getPaddleNumber(), lotResult.getStatus(),
        lotResult.getHammerPrice());

    LotId lotIdObj = new LotId(lotId, auctionId, sessionNumber);

    // insert lot result
    lotResult.setId(lotIdObj);
    lotResultRepository.save(lotResult);

    return lotResult;
  }

}
