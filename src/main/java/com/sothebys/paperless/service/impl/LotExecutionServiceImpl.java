package com.sothebys.paperless.service.impl;

import com.sothebys.paperless.domain.BidLot;
import com.sothebys.paperless.domain.LotResult;
import com.sothebys.paperless.domain.User;
import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.AuctionSessionId;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import com.sothebys.paperless.persistence.entity.LotExecution;
import com.sothebys.paperless.persistence.entity.LotExecutionId;
import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.repository.AuctionSessionRepository;
import com.sothebys.paperless.persistence.repository.LotExecutionRepository;
import com.sothebys.paperless.persistence.repository.LotResultRepository;
import com.sothebys.paperless.service.LotExecutionService;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation class for Lot Execution Service
 */
@AllArgsConstructor
@Service
@Slf4j
public class LotExecutionServiceImpl implements LotExecutionService {

  private final AuctionSessionRepository auctionSessionRepository;
  private final LocalStateManager stateManager;
  private final LotExecutionRepository lotExecutionRepository;
  private final LotResultRepository lotResultRepository;

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public BidLot confirmLotExecution(LotResult lotResult, String auctionId, int sessionNumber,
      String lotId, User agent) {
    log.debug(
        "Serving confirmLotExecution(paddle={}, auctionId={}, lotId={}, agent={}; result={}, amount={})",
        lotResult.getClient().getPaddle(), auctionId, lotId, agent, lotResult.getResult(),
        lotResult.getAmount());

    LotExecutionId lotExecutionId =
        new LotExecutionId(agent.getUsername(), lotId, auctionId, sessionNumber);

    // look for lot result from Auctioneer
    Optional<com.sothebys.paperless.persistence.entity.LotResult> lotResultFromAuctioneer = Optional
        .ofNullable(lotResultRepository.findOne(new LotId(lotId, auctionId, sessionNumber)));

    // Fetch AuctionState to get its state
    Optional<AuctionSession> auctionSession =
        auctionSessionRepository.findOptionalById(new AuctionSessionId(auctionId, sessionNumber));
    AuctionSessionState state = auctionSession.isPresent() ? auctionSession.get().getState()
        : AuctionSessionState.SCHEDULED;

    // look for an existing confirmed lot
    Optional<LotExecution> optionalLotExecutionResult =
        lotExecutionRepository.findOptionalById(lotExecutionId);

    LotExecution lotExecutionResult;
    if (optionalLotExecutionResult.isPresent()) {
      lotExecutionResult = optionalLotExecutionResult.get();
    } else {
      lotExecutionResult = new LotExecution();
      lotExecutionResult.setId(lotExecutionId);
      lotExecutionResult.setClientId(lotResult.getClient().getClientNumber());
      lotExecutionResult.setPaddleNumber(lotResult.getClient().getPaddle());
    }
    lotExecutionResult.setExecutionAmount(lotResult.getAmount());
    lotExecutionResult.setExecutionResult(lotResult.getResult());

    LotExecution persistedLotExecution = lotExecutionRepository.save(lotExecutionResult);
    log.debug("persisted lot execution id={}", persistedLotExecution.getId());

    BidLot bidLot = new BidLot(lotId, lotResult.getAmount(), lotResult.getResult(), stateManager
        .deriveLotState(lotResultFromAuctioneer, Optional.of(lotExecutionResult), state));
    return bidLot;
  }
}
