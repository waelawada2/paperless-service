package com.sothebys.paperless.service.impl;

import com.sothebys.paperless.exceptions.EntityNotFoundException;
import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.AuctionSessionId;
import com.sothebys.paperless.persistence.entity.LotResult;
import com.sothebys.paperless.persistence.repository.AuctionSessionRepository;
import com.sothebys.paperless.persistence.repository.LotResultRepository;
import com.sothebys.paperless.service.ReportService;
import com.sothebys.paperless.util.ZonedDateTimeFormatter;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ReportServiceImpl implements ReportService {

  private static final String REPORT_DATE_FORMAT = "dd_MMMM_yyyy_hh:mm_a";
  private static final String CSV_REPORT_HEADER_TEMPLATE =
      "Lot Number, Hammer Price (%s), Status, Paddle Number, Auctioneer\n";
  private LotResultRepository lotResultRepository;
  private AuctionSessionRepository auctionSessionRepository;
  private ZonedDateTimeFormatter zonedDateTimeFormatter;

  @Override
  public String getCSVReport(String auctionId, int sessionNumber) {
    AuctionSession auctionSession =
    auctionSessionRepository.findOptionalById(new AuctionSessionId(auctionId, sessionNumber))
        .orElseThrow(() -> new EntityNotFoundException(
            String.format("Can't find auction %s session %s", auctionId, sessionNumber)));
    List<LotResult> lotResults =
        lotResultRepository.findById_AuctionIdAndId_SessionId(auctionId, sessionNumber);
    if (lotResults.isEmpty()) {
      throw new EntityNotFoundException(
          String.format("Can't find lots for auction %s and session %s", auctionId, sessionNumber));
    }
    StringBuilder sb = new StringBuilder(String.format(CSV_REPORT_HEADER_TEMPLATE, 
      auctionSession.getAuction().getCurrency()));
    lotResults.stream().sorted(Comparator.comparingInt(l -> Integer.valueOf(l.getId().getLotId())))
    .forEach(lr -> {
      sb.append(String.format("%s,%s,%s,\t%s,%s\n", lr.getId().getLotId(), lr.getHammerPrice(),
          lr.getStatus(), lr.getPaddleNumber(), lr.getAuctioneer()));
    });
    return sb.toString();
  }

  @Override
  public String getReportFileName(String auctionId, int sessionNumber) {
    AuctionSession auctionSession =
        auctionSessionRepository.findOptionalById(new AuctionSessionId(auctionId, sessionNumber))
            .orElseThrow(() -> new EntityNotFoundException(
                String.format("Can't find auction %s session %s", auctionId, sessionNumber)));
    ZonedDateTime closeDateTime = zonedDateTimeFormatter.of(auctionSession.getCloseDate().getTime(), auctionSession.getAuction().getLocation());
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(REPORT_DATE_FORMAT);
    String closeDate = dateTimeFormatter.format(closeDateTime);
    int totalSessions = auctionSession.getAuction().getTotalSessions();
    String filename;
    if(totalSessions > 1){
      filename = String.format("%s_%s_%s.csv", auctionId, sessionNumber, closeDate);
    } else {
      filename = String.format("%s_%s.csv", auctionId, closeDate);
    }
    return filename.toUpperCase();
  }

}
