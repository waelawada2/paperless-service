package com.sothebys.paperless.service.impl;

import com.sothebys.paperless.client.ClientApiClient;
import com.sothebys.paperless.client.SAPClient;
import com.sothebys.paperless.domain.BidClient;
import com.sothebys.paperless.domain.clientapi.ClientApiData;
import com.sothebys.paperless.domain.sap.SapBidder;
import com.sothebys.paperless.mapper.ClientMapper;
import com.sothebys.paperless.service.ClientService;
import com.sothebys.paperless.util.PaddedSourceNumberFormatter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

@AllArgsConstructor
@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

  private final ClientApiClient clientApiClient;
  private final PaddedSourceNumberFormatter amountFormatter;
  private final SAPClient sapClient;

  /**
   * {@inheritDoc}
   */
  @Override
  public BidClient getClientDataForLot(String clientId, String auctionId, int sessionNumber,
      String lotId) {
    log.debug("Serving getClientDataForLot(clientId={}, auctionId={}, sessionNumber={}, lotId={})",
        clientId, auctionId, sessionNumber, lotId);

    StopWatch stopWatch = new StopWatch("getClientDataForLot");
    stopWatch.start("clientApiClient.findHolderPartyByAccountNumber");
    ClientApiData data = clientApiClient.findHolderPartyByAccountNumber(clientId);
    stopWatch.stop();

    stopWatch.start("mapping");
    BidClient bidClient = ClientMapper.INSTANCE.clientApiDataToBidClient(data);
    bidClient.setClientNumber(clientId);
    stopWatch.stop();

    stopWatch.start("sapClient.getBidderDataForLot");
    SapBidder bidderClientLotData = sapClient.getBidderDataForLot(auctionId, lotId, clientId);
    stopWatch.stop();

    bidClient.setAbsenteeBid(amountFormatter.parseAmount(bidderClientLotData.getTopBid()));
    bidClient.setCoverBid(amountFormatter.parseAmount(bidderClientLotData.getCoverBid()));
    bidClient.setCoverBidCurrency(bidderClientLotData.getReserveCurr());
    bidClient.setPaddle(bidderClientLotData.getPaddleNum());
    bidClient.setPreferredPhone(bidderClientLotData.getTel1());
    bidClient.setOtherPhone(bidderClientLotData.getTel2());
    bidClient.setLanguagePreference(bidderClientLotData.getPrefLangu());

    log.trace("{}", stopWatch);
    return bidClient;

  }

}
