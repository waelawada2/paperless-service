package com.sothebys.paperless.service.impl;

import com.sothebys.paperless.client.PublicApiClient;
import com.sothebys.paperless.client.SAPClient;
import com.sothebys.paperless.domain.LotState;
import com.sothebys.paperless.domain.sap.SapSaleRecord;
import com.sothebys.paperless.domain.webapi.WebApiAuction;
import com.sothebys.paperless.domain.webapi.WebApiAuctions;
import com.sothebys.paperless.domain.webapi.WebApiBidLot;
import com.sothebys.paperless.domain.webapi.WebApiBidLots;
import com.sothebys.paperless.exceptions.PaperlessServiceException;
import com.sothebys.paperless.persistence.entity.Auction;
import com.sothebys.paperless.persistence.entity.AuctionSession;
import com.sothebys.paperless.persistence.entity.AuctionSessionId;
import com.sothebys.paperless.persistence.entity.AuctionSessionState;
import com.sothebys.paperless.persistence.entity.Lot;
import com.sothebys.paperless.persistence.entity.LotExecution;
import com.sothebys.paperless.persistence.entity.LotId;
import com.sothebys.paperless.persistence.entity.LotResult;
import com.sothebys.paperless.persistence.repository.AuctionRepository;
import com.sothebys.paperless.persistence.repository.AuctionSessionRepository;
import com.sothebys.paperless.persistence.repository.LotRepository;
import com.sothebys.paperless.util.ComparableString;
import com.sothebys.paperless.util.PaddedSourceNumberFormatter;
import com.sothebys.paperless.util.ZonedDateTimeFormatter;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;

/**
 * A component to handle all things related to the local state persistence.
 */
@AllArgsConstructor
@Component
@Slf4j
public class LocalStateManager {

  public final static Long MOCK_RESERVE_PRICE = 450000L;

  private final AuctionRepository auctionRepository;
  private final AuctionSessionRepository auctionSessionRepository;
  private final LotRepository lotRepository;
  private final PaddedSourceNumberFormatter amountFormatter;
  private final PublicApiClient publicApiClient;
  private final SAPClient sapClient;
  private final ZonedDateTimeFormatter zonedDateTimeFormatter;

  public List<WebApiAuction> getAllWebApiAuctionsFiltered() {

    // Do the first request, to get the total number of auctions
    WebApiAuctions webApiAuctions = publicApiClient.getUpcomingAuctionsListPaginated(0);
    int totalAuctions = webApiAuctions.getTotalAuctions();

    // Prepare empty object for results
    WebApiAuctions result = new WebApiAuctions(
        new ArrayList<WebApiAuction>(webApiAuctions.getAuctions()), totalAuctions);

    // Pending pages:
    int totalPages = (int) Math.ceil(totalAuctions / 20.0);

    // A list of pending requests (one per page)
    List<CompletableFuture<WebApiAuctions>> pendingPageRequests = new ArrayList<>();
    for (int i = 1; i < totalPages; i++) {
      final Integer page = new Integer(i);
      CompletableFuture<WebApiAuctions> request = CompletableFuture
          .supplyAsync(() -> publicApiClient.getUpcomingAuctionsListPaginated(page));
      pendingPageRequests.add(request);
    }

    // Combine requests to do in parallel
    CompletableFuture<?>[] pendingRequests =
        pendingPageRequests.toArray(new CompletableFuture[pendingPageRequests.size()]);
    CompletableFuture<Void> combinedPendingRequests = CompletableFuture.allOf(pendingRequests);

    // Combine results in original request.
    combinedPendingRequests.thenAccept(
        i -> pendingPageRequests.forEach(f -> result.getAuctions().addAll(f.join().getAuctions())))
        .exceptionally(ex -> {
          log.debug("Error obtaining all pages from auction list {}", ex);
          throw new PaperlessServiceException(ex.getMessage());
        });

    // Go!
    try {
      combinedPendingRequests.get();
    } catch (Exception e) {
      throw new PaperlessServiceException(e.getMessage());
    }

    log.debug("Received {} auctions from all calls to the public api", result.getAuctions().size());

    return result.getAuctions().stream()
      .filter(waa -> waa.isSingleOwnerOrRegular())
      .filter(waa -> waa.isScheduled())
      .filter(waa -> waa.hasLots())
      .filter(waa -> waa.hasAllSessionsStartLotNonZero())
      .filter(waa -> waa.isNotPreviewAuction(
        publicApiClient.getAllLotsByAuction(waa.getSaleNumber()).getLots()))
      .distinct().collect(Collectors.toList());
  }

  /**
   * Compares the list of auctions returned by the public API and the persisted auctions, and add
   * the missing auctions to the database.
   */
  public void syncAuctionsAndLotState() {
    List<WebApiAuction> webApiAuctions = getAllWebApiAuctionsFiltered();
    log.debug("Syncing state for {} auctions", webApiAuctions.size());

    // Get all incoming IDs.
    Set<String> auctionSessionIds =
        webApiAuctions.stream().map(WebApiAuction::getSaleNumber).collect(Collectors.toSet());

    log.debug("Attempting to update {} records", auctionSessionIds.size());

    // Create missing auctions
    webApiAuctions.stream().filter(waa -> auctionSessionIds.contains(waa.getSaleNumber()))
        .forEach(waa -> {
          try {
            persistAuctionAndLots(waa);
          } catch (Exception e) {
            log.error("Error persisting auction {}: {}", waa.getSaleNumber(), e);
          }
        });

  }

  @Scheduled(cron = "${webapi.configuration.sync.cron}")
  public void scheduledDataSync() {
    StopWatch sw = new StopWatch("scheduledDataSync");
    log.info("** Running scheduled task now {}", Instant.now());
    
    sw.start("syncAuctionsAndLotState");
    syncAuctionsAndLotState();
    sw.stop();
    
    log.info("** Scheduled task completed. {}", sw);
  }

  /**
   * Derives a Lot state by asserting the relationships between lot, auction-session, execution, and
   * result.
   * 
   * @param lotResult the lot result object
   * @param lotExecution the lot execution object
   * @param auctionSessionState an instance of the enym type
   * @return a LotState enum instance with the derived state.
   */
  public LotState deriveLotState(Optional<LotResult> lotResult, Optional<LotExecution> lotExecution, AuctionSessionState auctionSessionState) {
    if (!Optional.ofNullable(auctionSessionState).isPresent()) {
      throw new IllegalArgumentException();
    }
    
    if (lotResult.isPresent()) {
      if (lotExecution.isPresent()) {
        return LotState.CLOSED;
      } else {
        return LotState.PENDING;
      }
    }

    if (lotExecution.isPresent()) {
      return LotState.CONFIRMED_BY_BIDDER;
    }

    if (auctionSessionState.equals(AuctionSessionState.OPEN)) {
      return LotState.OPEN;
    }

    return LotState.SCHEDULED;

  }
  
  /**
   * Derives state for a Lot, by assessing the status of related entities: LotResult and
   * AuctionSession
   * 
   * @param lotResult the lot result object
   * @param auctionSessionState an instance of the auction session state enum
   * @return an instance of LotState enum derived from relationships to this Lot.
   */
  public LotState deriveLotState(Optional<LotResult> lotResult, AuctionSessionState auctionSessionState) {
    if (!Optional.ofNullable(auctionSessionState).isPresent()) {
      throw new IllegalArgumentException();
    }
    
    if (lotResult.isPresent()) {
      return LotState.CLOSED;
    }
    
    if (auctionSessionState.equals(AuctionSessionState.OPEN)) {
      return LotState.OPEN;
    }

    return LotState.SCHEDULED;
  }

  @Transactional
  public void persistAuctionAndLots(WebApiAuction waa) {
    // Create/update auction
    Auction auction = auctionRepository.findOptionalById(
      waa.getSaleNumber()).orElse(new Auction(waa.getSaleNumber()));
    auction.setName(waa.getTitle());
    auction.setTotalSessions(waa.getSessions().size());
    auction.setTotalLots(waa.getTotalLots());
    auction.setDate(new Date(waa.getStartDate()));
    auction.setLocation(waa.getLocation().getName());
    auction.setCurrency(waa.getCurrency());
    auctionRepository.save(auction);

    waa.getSessions().forEach(waas -> {
      ZonedDateTime zonedDateTime =
          zonedDateTimeFormatter.of(waas.getStartDate(), waa.getLocation().getName());
      AuctionSessionId auctionSessionId = 
        new AuctionSessionId(waa.getSaleNumber(), waas.getSessionNumber());
        
      // Create/update auction session
      AuctionSession auctionSession = auctionSessionRepository
        .findOptionalById(auctionSessionId).orElse(new AuctionSession(auctionSessionId));
        
      if (auctionSession.hasUnmodifiedState()) {
        auctionSession.setState(AuctionSessionState.SCHEDULED);
        auctionSession.setStartLot(waas.getStartLot());
        auctionSession.setEndLot(waas.getEndLot());
        auctionSession.setStartDate(new Date(waas.getStartDate()));
        auctionSession.setOpenTimeWindowStart(zonedDateTimeFormatter.toWindowStart(zonedDateTime));
        auctionSession.setOpenTimeWindowEnd(zonedDateTimeFormatter.toWindowEnd(zonedDateTime));
        auctionSessionRepository.save(auctionSession);
  
        // Create/update lots for session
        WebApiBidLots webApilots = 
          publicApiClient.getLotsByAuctionSession(waa.getSaleNumber(), waas.getSessionNumber());
        
        Map<String, WebApiBidLot> sessionWebLotsById =
          webApilots.getLots().stream().collect(Collectors.toMap(k -> k.getLotNumber(), l -> l));
  
        List<SapSaleRecord> allSapLots = sapClient.getSaleStatusReserveEstimates(waa.getSaleNumber());
  
        List<Lot> lots = allSapLots.stream().distinct().map(sl -> {
          Lot lot = new Lot();
          String parsedLotId = amountFormatter.unpad(sl.getLotNum());
          lot.setId(new LotId(parsedLotId, waa.getSaleNumber(), waas.getSessionNumber()));
          lot.setAuctionSession(auctionSession);
          if (!sessionWebLotsById.containsKey(parsedLotId)) {
            // check for WDN lot
            if (sl.isWithdrawn()) {
              log.trace("lot {} is WDN", parsedLotId);
              lot.setIsWithdrawn(Boolean.TRUE);
            }
            String start = waas.getStartLot();
            String end = waas.getEndLot();
            if (new ComparableString(parsedLotId).outsideRange(start, end)) {
              log.trace(
                  "[persistAuctionAndLots] skipping lot {}: Outside range for auction-session {} (lot range {}-{})",
                  parsedLotId, waa.getSaleNumber() + "-" + waas.getSessionNumber(), waas.getStartLot(),
                  waas.getEndLot());
              return null;
            }
          } else {
            WebApiBidLot wal = sessionWebLotsById.get(parsedLotId);
            // Description and Author from guarantee line
            String[] guaranteedItems = wal.getGuaranteeLine().split("\\|");
            if (guaranteedItems.length == 2) {
              if (!guaranteedItems[0].trim().equalsIgnoreCase(guaranteedItems[1].trim())) {
                lot.setLotDescription(guaranteedItems[1].trim());
              }
            }
            lot.setLotAuthor(guaranteedItems[0].trim());
            lot.setEstimateLow(wal.getEstimateLow());
            lot.setEstimateHigh(wal.getEstimateHigh());
            lot.setImageUrl(wal.getLargeImage());
            lot.setMedium(wal.getMedium());
            lot.setExecuted(wal.getExecuted());
            lot.setLotPath(wal.getLotPath());
          }
          return lot;
        }).collect(Collectors.toList());
  
        lotRepository.bulkSave(lots);
      } else {
        log.trace(
            "[persistAuctionAndLots] skipping auction-session {}-{}. Status was previously modified by auctioneer ({}).",
            auctionSession.getId().getAuctionId(), auctionSession.getId().getSessionId(), auctionSession.getState());
      }
    });
  }

}
