package com.sothebys.paperless.service;

import com.sothebys.paperless.domain.BidClient;

public interface ClientService {

  /**
   * Fetches client data from Client System and complements it with assignment data from SAP (paddle
   * for the given sale, and coverBid or absenteeBid for the given lot)
   * 
   * @param clientId the id of the client
   * @param auctionId the id of the auction 
   * @param sessionNumber the session number
   * @param lotId the id of the lot
   * @return the BidClient data as fetched from Client System and SAP.
   */
  BidClient getClientDataForLot(String clientId, String auctionId, int sessionNumber, String lotId);

}
