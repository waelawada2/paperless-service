package com.sothebys.paperless.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.util.StringUtils;

/**
 * Implementation of a comparable String. Overrides {@link #compareTo(String)} with a specific
 * behavior, so that lot ids are compared in a proper way.
 * 
 * @author ricardo.buitrago.consultant@sothebys.com
 *
 */
@Data
@AllArgsConstructor
public class ComparableString implements Comparable<String> {

  private String self;

//@formatter:off
  /*
   * (non-Javadoc) 
   * <pre>
   *    Compares the wrapped {@code String} against another {@code String}. The
   *    comparison should be sensitive to empty strings, although with low probability of occurence:
   * 
   *    * For the case when both the wrapped instance and the compared string are empty, the
   *    comparison returns 0. 
   *    * In case of empty string value for the wrapped {@code String}, the
   *    comparison should return -1. 
   *    * Similarly, when the compared string is empty, the comparison should return 1. 
   *    
   *    For all other cases, the values are first considered as an integer number. If they can't 
   *    be parsed, a comparison between the sum of codepoints of each string is made. 
   *    
   *    1) If they're found equal, an attempt to normalize in upper case and compare codepoints sum is made. 
   *    If they're found equal, the result is returned. 
   *    
   *    2) If the uppercased strings codepoints sum comparison is not zero, the result will be returned.
   *    
   *    3) In the specific case when the uppercased codepoints sum comparison is zero, a further 
   *    attempt to call {@code String#compareTo} between the two strings, and the result is returned.
   *    That will prevent to return zero when comparing anagrams, thus maintaining the contract of #compareTo
   * </pre>
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
//@formatter:on
  @Override
  public int compareTo(String other) {
    if (StringUtils.isEmpty(self) && StringUtils.isEmpty(other)) {
      return 0;
    } else if (StringUtils.isEmpty(self)) {
      return -1;
    } else if (StringUtils.isEmpty(other)) {
      return 1;
    } else {
      try {
        int selfInt = Integer.parseInt(self);
        int otherInt = Integer.parseInt(other);
        return selfInt - otherInt;
      } catch (NumberFormatException e) {
        int operUpper = self.toUpperCase().codePoints().reduce((a, b) -> a + b).getAsInt()
            - other.toUpperCase().codePoints().reduce((a, b) -> a + b).getAsInt();
        if (operUpper != 0) {
          return operUpper;
        } else {
          int oper = self.codePoints().reduce((a, b) -> a + b).getAsInt()
              - other.codePoints().reduce((a, b) -> a + b).getAsInt();

          if (oper == 0 && operUpper == 0) {
            operUpper = self.toUpperCase().compareTo(other.toUpperCase()); // check anagrams
          }
          return operUpper;
        }
      }
    }
  }

  /**
   * Decides if a String is within a range bound by lower and upper String values. Uses
   * {@link #compareTo(String)}
   * 
   * @param lowerBoundary a String value which is expected to be lesser than this ComparableString
   *        inner value.
   * @param upperBoundary a String value which is expected to be greater than this ComparableString
   *        inner value.
   * @return true if both expected conditions are met, or false otherwise
   */
  public boolean withinRange(String lowerBoundary, String upperBoundary) {
    return compareTo(lowerBoundary) >= 0 && compareTo(upperBoundary) <= 0;
  }

  /**
   * Decides if a String is outside a range bound by lower and upper String values. Uses
   * {@link #compareTo(String)}
   * 
   * @param lowerBoundary a String value which is expected to be greater than this ComparableString
   *        inner value.
   * @param upperBoundary a String value which is expected to be lesser than this ComparableString
   *        inner value.
   * @return true if both expected conditions are met, or false otherwise
   */
  public boolean outsideRange(String lowerBoundary, String upperBoundary) {
    return !withinRange(lowerBoundary, upperBoundary);
  }
}
