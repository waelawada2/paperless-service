package com.sothebys.paperless.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;

/**
 * Implementation of generic JsonSerializer. Aims at dealing with null values in members of object sent
 * to ObjectMapper for serialization in a proper manner.
 */
public class NullSerializer extends JsonSerializer<Object> {

  /**
   * {@inheritDoc}
   */
  /*
   * (non-Javadoc)
   * 
   * @see com.fasterxml.jackson.databind.JsonSerializer#serialize(java.lang.Object,
   * com.fasterxml.jackson.core.JsonGenerator, com.fasterxml.jackson.databind.SerializerProvider)
   * 
   * Replaces null vallues with an empty string.
   */
  @Override
  public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers)
      throws IOException, JsonProcessingException {
    gen.writeString("");
  }
}
