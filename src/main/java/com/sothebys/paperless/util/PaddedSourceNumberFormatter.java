package com.sothebys.paperless.util;

import com.sothebys.paperless.configuration.SapApiProperties;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Utility class that parses fixed-width source strings as long number.
 * 
 * @author ricardo.buitrago.consultant@sothebys.com
 */
@AllArgsConstructor
@Component
@Slf4j
public class PaddedSourceNumberFormatter {

  private final SapApiProperties sapProperties;

  /**
   * Attempts to use a DecimalFormat with configured pattern to parse a (very likely space-padded)
   * source string data as a long number.
   * 
   * @param source the space-padded string that usually represents an amount
   * @return null if the source string cannot be parsed, or the long representation otherwise
   */
  public Long parseAmount(String source) {
    DecimalFormat amountsFormat =
        new DecimalFormat(sapProperties.getConfiguration().getAmountsFormat());
    try {
      Number number = amountsFormat.parse(source.trim());
      return number.longValue();
    } catch (ParseException e) {
      log.warn("cannot parse amount {} as long. Defaulting to null value", source);
      return null;
    }
  }

  /**
   * Attempts to strip the zero padding to the left of a String that represents a number (clientId,
   * lotId, etc). Ids coming from SAP are usually padded to a fixed length with zeroes to the left.
   * Using {@link #parseAmount(String)}, the string value of the number is returned if the parsing
   * is correctly executed. This method will return a null value under the same conditions as
   * {@link #parseAmount(String)}.
   * 
   * @param source the space-padded string that usually represents an id
   * @return null if the source string cannot be parsed, or the unpadded string representation
   *         otherwise
   */
  public String unpad(String source) {
    Optional<Long> parsedLong = Optional.ofNullable(parseAmount(source));
    return parsedLong.isPresent() ? parsedLong.get().toString() : null;
  }

}
