package com.sothebys.paperless.util;

import com.sothebys.paperless.configuration.DateTimeConfiguration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Optional;
import org.springframework.stereotype.Component;

/**
 * A class that handles date and time formatting
 */
@Component
public class ZonedDateTimeFormatter {

  private static final long ONE = 1;
  private static final long FOUR = 4;

  private String defaultTimezone;
  private String dateFormat;
  private String timeFormat;
  private Map<String, String> timeZones;

  public ZonedDateTimeFormatter(DateTimeConfiguration config) {
    this.dateFormat = config.getDateFormat();
    this.timeFormat = config.getTimeFormat();
    this.defaultTimezone = config.getDefaultTimezone();
    this.timeZones = config.getTimeZones();
  }

  /**
   * Formats a date using the configured pattern.
   * 
   * @param zonedDateTime a datetime with timezone information
   * @return formatted date
   */
  public String formatCardDate(ZonedDateTime zonedDateTime) {
    return zonedDateTime.format(DateTimeFormatter.ofPattern(dateFormat));
  }

  /**
   * Formats a time using the configured pattern
   * 
   * @param zonedDateTime a datetime with timezone information
   * @return formatted time
   */
  public String formatCardTime(ZonedDateTime zonedDateTime) {
    return zonedDateTime.format(DateTimeFormatter.ofPattern(timeFormat));
  }

  /**
   * Build a ZonedDateTime of the epoch and location.
   * 
   * @param epoch a long representing a datetime
   * @param location the location to use for getting the timezone
   * @return a datetime with timezone information
   */
  public ZonedDateTime of(Long epoch, String location) {

    // Search configured timezones, else try to compute one or use default
    String zoneId = Optional.ofNullable(timeZones.get(location)).orElseGet(() -> {

      // Normalize location string for search in tzdb
      final String normalizedLocation = location.replace(" ", "_");

      // Look for a timezone that contains our normalized location
      String computedZoneId = ZoneId.getAvailableZoneIds().stream()
          .filter(zoneString -> zoneString.contains(normalizedLocation)).findFirst()
          .orElse(defaultTimezone);

      return computedZoneId;
    });

    return ZonedDateTime.ofInstant(Instant.ofEpochMilli(epoch), ZoneId.of(zoneId));

  }

  /**
   * Truncates date to 0-hour same day, keeping timezone setting.
   * 
   * @param zonedDateTime the timezone-set date instance
   * @return the zonedDateTime set to 0-hour
   */
  public long toWindowStart(ZonedDateTime zonedDateTime) {
    return zonedDateTime.truncatedTo(ChronoUnit.DAYS).toInstant().toEpochMilli();
  }

  /**
   * Adds four hours to provided ZonedDateTime
   * 
   * @param zonedDateTime the timezone-set date instance
   * @return the zoneDateTime set to five minutes later
   */
  public long toWindowEnd(ZonedDateTime zonedDateTime) {
    return zonedDateTime.plusMinutes(ONE).plusHours(FOUR).toInstant().toEpochMilli();
  }

  /**
   * Checks whether a date to check (in millis) is within a window of 0-hour same day (in millis)
   * and provided window end time (in millis) plus five minutes
   * 
   * @param timeToCheckMillis the millisecond time to be checked within time window
   * @param windowStartMillis the millisecond time for the start of time window
   * @param windowEndMillis the millisecond time for the end of time window
   * @return true, if the time stamp passed as a parameter is within the range, false otherwise
   */
  public boolean isWithinWindow(long timeToCheckMillis, long windowStartMillis,
      long windowEndMillis) {
    return windowStartMillis <= timeToCheckMillis && timeToCheckMillis <= windowEndMillis;
  }


}
