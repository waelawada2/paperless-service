package com.sothebys.paperless.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;

public class FileResourceUtil {

  protected String fileContentString(String name) throws IOException {
    String path = String.format("%s/%s", System.getProperty("user.dir"), name);
    File file = new File(path);
    return new String(Files.readAllBytes(file.toPath()));
  }

  protected String resourceContentString(String name) throws IOException {
    File file = new File(this.getClass().getClassLoader().getResource(name).getFile());
    return new String(Files.readAllBytes(file.toPath()));
  }

  protected String resourceContentString(Resource resource) throws IOException {
    return StreamUtils.copyToString(resource.getInputStream(), Charset.defaultCharset());
  }

}
