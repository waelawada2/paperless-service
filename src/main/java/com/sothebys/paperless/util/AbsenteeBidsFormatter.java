package com.sothebys.paperless.util;

import com.sothebys.paperless.domain.AbsenteeBid;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class AbsenteeBidsFormatter {

  public List<AbsenteeBid> formatAbsenteeBidList(Long reservePrice,
      List<AbsenteeBid> absenteeBids) {

    // Top three higher absentees amounts
    Set<Long> amounts = absenteeBids.stream().map(AbsenteeBid::getAmount).distinct()
        .sorted(Comparator.reverseOrder()).limit(3).collect(Collectors.toSet());

    List<AbsenteeBid> result = absenteeBids.stream().filter(ab -> amounts.contains(ab.getAmount()))
        .map(ab -> {
            ab.setIsReserve(false);
            return ab;
        })
        .collect(Collectors.toList());

    // Add the reserve price
    result.add(new AbsenteeBid(null, "Reserve", reservePrice, null, true));

    // Final order by amount and registration time
    result = result.stream().sorted(Comparator.comparingLong(AbsenteeBid::getAmount).reversed()
        .thenComparing(AbsenteeBid::getIsReserve).thenComparing(AbsenteeBid::getRegistrationDatetime))
        .collect(Collectors.toList());

    return result;
  }

}
