package com.sothebys.paperless.util;

import com.sothebys.paperless.domain.User;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class UserHandlerMethodParameterResolver implements HandlerMethodArgumentResolver {

  @Override
  public boolean supportsParameter(MethodParameter parameter) {
    return parameter.getParameterType().equals(User.class);
  }

  @Override
  public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
      NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {

    KeycloakAuthenticationToken authenticationToken =
        (KeycloakAuthenticationToken) webRequest.getUserPrincipal();
    AccessToken token = authenticationToken.getAccount().getKeycloakSecurityContext().getToken();
    return new User(token.getName(), token.getEmail().toUpperCase(), token.getPreferredUsername());
  }

}
