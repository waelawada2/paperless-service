package com.sothebys.paperless.util;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class PaddedString {

  /**
   * Padding limit for accountNumbers as used by Client System API requests
   */
  public static final int PADDING_LIMIT_FOR_ACCOUNT_NUMBER = 8;

  private static final int PADDING_LIMIT_FOR_PADDLE = -1;
  private static final int PADDING_LIMIT_FOR_L_PADDLE = 4;
  private static final String L_PREFIX = "L";

  private String _self;
  private int desiredLength;

  public PaddedString(String value) {
    this._self = value;
  }

  /**
   * Fluid interface for applying a new lenght to pad a string to
   * 
   * @param length the int value to be set as limit
   * @return an instance of PaddedString to continue configuring or invoke {@link #toString()}
   */
  public PaddedString withLength(int length) {
    this.desiredLength = length;
    return this;
  }

  /**
   * Fluid interface for applying {@link #PADDING_LIMIT_FOR_ACCOUNT_NUMBER} as new padding limit
   * 
   * @return an instance of PaddedString continue configuring or invoke {@link #toString()}
   */
  public PaddedString asClient() {
    this.desiredLength = PADDING_LIMIT_FOR_ACCOUNT_NUMBER;
    return this;
  }

  /**
   * Checks whether {@value #_self} represents a prefixed paddle number.
   * 
   * @returns true if the value represents a prefixed paddle number; false
   *          otherwise.
   */
  private boolean isPrefixed() {
    return _self.toUpperCase().indexOf(L_PREFIX) == 0;
  }

  /***
   * Attempts to append leading zeroes to a given string. If a failure occurs (such is the case of
   * non-prefixed paddles that fail to be parsed as a number), it will return the original string
   * value. Also, the original string will be returned if the string is of length greater or equal
   * than value set for {@link #desiredLength}.
   * 
   * Examples: For paddles, e.g.: {@code L10 -> L0010, 10 -> 00010}. For accountNumbers, e.g.:
   * {@code 139501 -> 000139501}.
   * 
   * @return a String padded to the length specified by {@link #desiredLength} (if set to a value
   *         greater than zero). Otherwise the string is assumed as a paddle, and
   *         {@link #PADDING_LIMIT_FOR_PADDLE} or {@link #PADDING_LIMIT_FOR_L_PADDLE} is used as
   *         limit on each case accordingly.
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final int PAD_TO = detectPaddingLimit();
    // when value to pad is already at desired length
    if (_self.length() >= PAD_TO) {
      return _self;
    }
    // when value represents a paddle not prefixed by L
    if (PAD_TO == PADDING_LIMIT_FOR_PADDLE) {
      return _self;
    }
    String prefix = "";
    String padThisNumber = _self;
    int padCharactersCount = PAD_TO;
    if (isPrefixed()) {
      // specific to paddle numbers padding only
      prefix = String.valueOf(_self.charAt(0));
      padThisNumber = _self.substring(1);
      padCharactersCount = PADDING_LIMIT_FOR_L_PADDLE;
    }
    String formatExpression =
        new StringBuilder("%0").append(padCharactersCount).append("d").toString();
    try {
      return new StringBuilder(prefix)
          .append(String.format(formatExpression, Integer.parseInt(padThisNumber))).toString();
    } catch (NumberFormatException ex) {
      log.warn("Cannot pad string ({}). Using raw value instead", _self);
    }
    return _self;
  }

  // Private method

  /**
   * Detects either padding must use set property {@link #desiredLength}, or
   * assert the prefix and set the limit as adequate for each case
   * {@link #PADDING_LIMIT_FOR_PADDLE}
   * 
   * @return the value set to {@link #getLength()} if it's greater than zero, or
   *         {@link #PADDING_LIMIT_FOR_PADDLE} otherwise.
   */
  private int detectPaddingLimit() {
    return getDesiredLength() > 0 ? getDesiredLength() 
      : (isPrefixed() ? PADDING_LIMIT_FOR_L_PADDLE : PADDING_LIMIT_FOR_PADDLE);
  }
}
