package com.sothebys.paperless.mapper;

import com.sothebys.paperless.domain.BidClient;
import com.sothebys.paperless.domain.clientapi.ClientApiData;

public abstract class ClientMapperDecorator implements ClientMapper {

  private final ClientMapper delegate;

  public ClientMapperDecorator(ClientMapper delegate) {
    this.delegate = delegate;
  }

  public BidClient clientApiDataToBidClient(ClientApiData clientApiData) {
    BidClient bidClient = delegate.clientApiDataToBidClient(clientApiData);
    bidClient.setPartyType(clientApiData.getPartyType().toUpperCase());
    return bidClient;
  }

}
