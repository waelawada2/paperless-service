package com.sothebys.paperless.mapper;

import com.sothebys.paperless.domain.BidClient;
import com.sothebys.paperless.domain.clientapi.ClientApiData;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@DecoratedWith(ClientMapperDecorator.class)
@Mapper
public interface ClientMapper {

  ClientMapper INSTANCE = Mappers.getMapper(ClientMapper.class);

  @Mappings({@Mapping(source = "kcmName", target = "kcm"),
      @Mapping(source = "clientLevelId", target = "clientLevel"),
      @Mapping(target = "partyType", ignore = true)})
  BidClient clientApiDataToBidClient(ClientApiData clientApiData);
}
