package com.sothebys.paperless.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.paperless.configuration.ClientApiHeaders;
import com.sothebys.paperless.configuration.ClientApiProperties;
import com.sothebys.paperless.domain.clientapi.ClientApiData;
import com.sothebys.paperless.util.PaddedString;
import java.net.URI;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
public class ClientApiClient implements InitializingBean, PaperlessIntegrationClient {

  private static final String HEADER_X_API_KEY = "x-api-key";
  private static final String HEADER_CLIENT_SECRET = "client_secret";
  private static final String HEADER_CLIENT_ID = "client_id";
  private ClientApiProperties clientApiProperties;
  private HttpHeaders headers;
  private ObjectMapper objectMapper;
  private RestTemplate restTemplate;

  @Autowired
  public ClientApiClient(RestTemplate restTemplate, ClientApiProperties clientApiProperties,
      ObjectMapper objectMapper) {
    this.restTemplate = restTemplate;
    this.clientApiProperties = clientApiProperties;
    this.objectMapper = objectMapper;
  }

  /**
   * Connects via http as client to Client System API and performs a GET request, passing
   * accountNumber as a request paramenter.
   * 
   * @param accountNumber the client identification to query Client System
   * @return a mapped instance of ClientApiData containing data relevant to accountNUmber
   */
  public ClientApiData findHolderPartyByAccountNumber(String accountNumber) {
    String paddedAccountNumber = new PaddedString(accountNumber).asClient().toString();
    UriComponentsBuilder uriBuilder = UriComponentsBuilder
        .fromHttpUrl(clientApiProperties.getConfiguration().getUrl()
            + clientApiProperties.getPaths().getFindHolderPartyByAccountNumber())
        .queryParam("accountNumber", paddedAccountNumber);
    URI requestUrl = uriBuilder.build().encode().toUri();

    log.debug("Consuming ClientApiClient.findHolderPartyByAccountNumber({}): using URL: [{}]",
        paddedAccountNumber, requestUrl.toString());

    HttpEntity<ClientApiData> requestEntity = new HttpEntity<ClientApiData>(headers);
    ResponseEntity<String> response =
        restTemplate.exchange(requestUrl, HttpMethod.GET, requestEntity, String.class);

    if (response.getStatusCode() != HttpStatus.OK) {
      String message = String.format("Got %s from client api for accountNumber %s: %s",
          response.getStatusCodeValue(), paddedAccountNumber,
          deserializeJsonString(response.getBody(), Error.class, objectMapper, log));
      handleError(message, log);
    }

    return deserializeJsonString(response.getBody(), ClientApiData.class, objectMapper, log);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    headers = new HttpHeaders();
    ClientApiHeaders hdrProperties = clientApiProperties.getConfiguration().getHeaders();
    headers.add(HttpHeaders.CACHE_CONTROL, hdrProperties.getCacheControl());
    headers.add(HEADER_CLIENT_ID, hdrProperties.getClientId());
    headers.add(HEADER_CLIENT_SECRET, hdrProperties.getClientSecret());
    headers.add(HEADER_X_API_KEY, hdrProperties.getXApiKey());

  }

}
