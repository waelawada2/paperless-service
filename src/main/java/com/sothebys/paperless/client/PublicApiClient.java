package com.sothebys.paperless.client;

import com.google.common.collect.ImmutableMap;
import com.sothebys.paperless.configuration.ApiHeaders;
import com.sothebys.paperless.configuration.WebApiProperties;
import com.sothebys.paperless.domain.webapi.WebApiAuctionSession;
import com.sothebys.paperless.domain.webapi.WebApiAuctions;
import com.sothebys.paperless.domain.webapi.WebApiBidLot;
import com.sothebys.paperless.domain.webapi.WebApiBidLots;
import com.sothebys.paperless.exceptions.WebApiResourceException;
import com.sothebys.paperless.util.ComparableString;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * Client for the public API
 */
@Component
@Slf4j
public class PublicApiClient implements InitializingBean {

  private static final String MAX_AUCTIONS_PER_PAGE = "20";

  private RestTemplate restTemplate;
  private WebApiProperties webApiProperties;

  private HttpHeaders headers;


  @Autowired
  public PublicApiClient(RestTemplate restTemplate, WebApiProperties apiProperties) {
    this.restTemplate = restTemplate;
    this.webApiProperties = apiProperties;
  }

  /**
   * Filters lots within the range specified by a given session. As the wrapper object of the lots
   * list always contains the sessions context metadata, we assert whether it details more than one
   * session and then return those who are within the range of a given session.
   * 
   * @param foundLotsWrapper the wrapper object to the page of lots
   * @param sessionNumber the session Number we want to filter by
   * @return a list of lots filtered by session
   */
  public List<WebApiBidLot> filterBySession(WebApiBidLots foundLotsWrapper, long sessionNumber) {
    log.debug("Delegate Call: filterBySession(sale={}, session={})",
        foundLotsWrapper.getSale().getSaleNumber(), sessionNumber);
    List<WebApiBidLot> foundLots = foundLotsWrapper.getLots();
    boolean singleSessionSale =
        foundLotsWrapper.getSale().getSessions().size() == 1 && sessionNumber == 1;
    if (singleSessionSale) {
      return foundLots;
    }
    Optional<WebApiAuctionSession> session = foundLotsWrapper.getSale().getSessions().stream()
        .filter(s -> s.getSessionNumber() == sessionNumber).findAny();
    foundLots = new ArrayList<>();
    if (session.isPresent()) {
      for (WebApiBidLot lot : foundLotsWrapper.getLots()) {
        ComparableString lotId = new ComparableString(lot.getLotNumber());
        if (lotId.compareTo(session.get().getStartLot()) >= 0
            && lotId.compareTo(session.get().getEndLot()) <= 0) {
          foundLots.add(lot);
        }
      }
    }
    return foundLots;
  }

  /**
   * Get the lots for an auction, aggregated. Iterates through web API pages, but filters out the
   * lots not belonging to the sessions passed as an argument
   * 
   * @param saleNumber The auction sale number
   * @param sessionNumber the session number the lots returned will belong to. A value = 0 would
   *        mean no filter by session needed.
   * @return The lots of all the pages returned by the web API
   */
  public WebApiBidLots getLotsByAuctionSession(String saleNumber, long sessionNumber) {
    log.debug("Delegate Call: getLotsByAuctionSession({}, {})", saleNumber, sessionNumber);
    WebApiBidLots foundLots = getAllLotsByAuction(saleNumber);
    if (sessionNumber > 0 && !foundLots.getSale().getSessions().isEmpty()) {
      foundLots.setLots(filterBySession(foundLots, sessionNumber));
    }
    return foundLots;
  }

  /**
   * Get the lots for an auction, paginated
   * 
   * @param saleNumber The auction sale number
   * @param pageNum Page number to return
   * @return The page with lots in the given auction
   */
  public WebApiBidLots getLotsByAuctionPaginated(String saleNumber, Integer pageNum) {
    UriComponentsBuilder uriBuilder = UriComponentsBuilder
        .fromHttpUrl(
            webApiProperties.getConfiguration().getUrl() + webApiProperties.getPaths().getLots())
        .queryParam("saleNumber", saleNumber);
    if (Optional.ofNullable(pageNum).isPresent()) {
      uriBuilder.queryParam("pageNum", pageNum);
    }
    URI requestUrl = uriBuilder.build().encode().toUri();

    log.debug("Consuming: getLotsByAuctionPaginated({}): using URL: [{}]", saleNumber,
        requestUrl.toString());
    HttpEntity<WebApiBidLots> requestEntity = new HttpEntity<WebApiBidLots>(headers);
    ResponseEntity<WebApiBidLots> response =
        restTemplate.exchange(requestUrl, HttpMethod.GET, requestEntity, WebApiBidLots.class);

    if (response.getStatusCode() != HttpStatus.OK) {
      handleError(response, "Cannot retrieve list of lots with data");
    }

    return response.getBody();

  }
  
  /**
   * Gets the lots from an auction. All of them.
   * @param saleNumber the id for the sale that will be retrieved from Public API
   * @return a wrapper object of type WebApiBidLots containing list of lots
   */
  public WebApiBidLots getAllLotsByAuction(String saleNumber) {
    UriComponentsBuilder uriBuilder = UriComponentsBuilder
        .fromHttpUrl(
            webApiProperties.getConfiguration().getUrl() + webApiProperties.getPaths().getLots())
        .queryParam("saleNumber", saleNumber).queryParam("allLots","true");

    URI requestUrl = uriBuilder.build().encode().toUri();

    log.debug("Consuming: getAllLotsByAuction({}): using URL: [{}]", saleNumber,
        requestUrl.toString());
    HttpEntity<WebApiBidLots> requestEntity = new HttpEntity<WebApiBidLots>(headers);
    ResponseEntity<WebApiBidLots> response =
        restTemplate.exchange(requestUrl, HttpMethod.GET, requestEntity, WebApiBidLots.class);

    if (response.getStatusCode() != HttpStatus.OK) {
      handleError(response, "Cannot retrieve list of lots with data");
    }

    return response.getBody();
  }

  /**
   * Get the list of upcoming auctions. when making this call.
   * @param pageNum the desired page to be returned
   * @return a wrapper object of type WebApiAuctions containing the auctions
   */
  public WebApiAuctions getUpcomingAuctionsListPaginated(Integer pageNum) {
    UriComponentsBuilder uriBuilder =
        UriComponentsBuilder.fromHttpUrl(webApiProperties.getConfiguration().getUrl()
            + webApiProperties.getPaths().getAuctionsList());

    // Query params
    Map<String, String> queryParams = ImmutableMap.of("archived", "false", "itemsPerPage",
        MAX_AUCTIONS_PER_PAGE, "pageNum", String.valueOf(pageNum));
    queryParams.forEach((param, value) -> uriBuilder.queryParam(param, value));

    URI requestUrl = uriBuilder.build().encode().toUri();

    log.debug("Consuming: getAuctions(): using URL: [{}]", requestUrl);
    HttpEntity<WebApiAuctions> requestEntity = new HttpEntity<WebApiAuctions>(headers);
    ResponseEntity<WebApiAuctions> response = null;
    try {
      response =
          restTemplate.exchange(requestUrl, HttpMethod.GET, requestEntity, WebApiAuctions.class);
    } catch (NestedRuntimeException e) {
      log.error("Error consuming web api - can't continue loading auctions list paginated: {}",
          e.getMessage());
    }

    if (response == null || response.getStatusCode() != HttpStatus.OK) {
      handleError(response, "Cannot retrieve list of auctions from public API");
    }
    return response.getBody();
  }

  /**
   * Set common headers for all public api requests
   */
  @Override
  public void afterPropertiesSet() throws Exception {
    headers = new HttpHeaders();
    ApiHeaders hdrProperties = webApiProperties.getConfiguration().getHeaders();
    headers.add(HttpHeaders.ACCEPT_LANGUAGE, hdrProperties.getAcceptLanguage());
    headers.add(HttpHeaders.CONNECTION, hdrProperties.getConnection());
    headers.add(HttpHeaders.USER_AGENT, hdrProperties.getUserAgent());
  }

  private void handleError(ResponseEntity<?> response, String errorMessage) {
    errorMessage = response == null ? errorMessage
        : String.format("%s %s: %s)", errorMessage, response.getStatusCodeValue(),
            response.getStatusCode().getReasonPhrase());
    log.error(errorMessage);
    throw new WebApiResourceException(errorMessage);
  }

}
