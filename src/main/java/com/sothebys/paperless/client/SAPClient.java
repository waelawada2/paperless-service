package com.sothebys.paperless.client;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.google.common.annotations.VisibleForTesting;
import com.sothebys.paperless.configuration.ApiAuthorization;
import com.sothebys.paperless.configuration.SapApiProperties;
import com.sothebys.paperless.domain.sap.SapBidder;
import com.sothebys.paperless.domain.sap.SapSaleRecord;
import com.sothebys.paperless.domain.sap.request.SapBaseRequest;
import com.sothebys.paperless.domain.sap.request.SapBidsRequest;
import com.sothebys.paperless.domain.sap.request.SapSalesRequest;
import com.sothebys.paperless.domain.sap.response.SapAbsenteeBidsResponse;
import com.sothebys.paperless.domain.sap.response.SapSalesResponse;
import com.sothebys.paperless.util.NullSerializer;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
public class SAPClient implements InitializingBean, PaperlessIntegrationClient {

  public static final String X = "X";
  
  private static final String SAP_CLIENT_REQ_PARAM = "sap-client";
  private static final String JSON_STRING_REQUEST_FORM_PARAM = "JSON_String";
  private static final String EMPTY_FIELD_VALUE = "";

  private HttpHeaders headers;
  private final ObjectMapper objectMapper;
  private final RestTemplate restTemplate;
  private final SapApiProperties sapApiProperties;

  public SAPClient(ObjectMapper objectMapper, RestTemplate restTemplate,
      SapApiProperties sapApiProperties) {
    this.objectMapper = objectMapper;
    this.restTemplate = restTemplate;
    this.sapApiProperties = sapApiProperties;
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    headers = new HttpHeaders();

    ApiAuthorization authConfig =
        sapApiProperties.getConfiguration().getHeaders().getAuthorization();

    // headers setup
    headers.add(HttpHeaders.AUTHORIZATION, configureAuthHeader(authConfig));
    headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    // Config for null serialization: JSON request needs to comply with strict non-null values
    DefaultSerializerProvider defaultSerializerProvider = new DefaultSerializerProvider.Impl();
    defaultSerializerProvider.setNullValueSerializer(new NullSerializer());
    objectMapper.setSerializerProvider(defaultSerializerProvider);
    objectMapper.setSerializationInclusion(Include.NON_NULL);

    // Config for RestTemplate message converter
    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    List<MediaType> mediaTypes =
        Arrays.asList(MediaType.APPLICATION_FORM_URLENCODED, MediaType.TEXT_HTML);
    converter.setSupportedMediaTypes(mediaTypes);
    restTemplate.getMessageConverters().add(converter);

  }

  /**
   * Set headers
   * @param headers the headers to set
   */
  @VisibleForTesting
  public void setHeaders(HttpHeaders headers) {
    this.headers = headers;
  }

  /**
   * Configures header value for authentication
   * 
   * @param authConfig the object containing retrieved configuration
   * @return a plain-text string with encoded authentication credentials
   */
  private String configureAuthHeader(ApiAuthorization authConfig) {
    // configure Character set for encoding
    Charset cs = Charset.forName(authConfig.getEncoding());
    // prepare credentials
    String auth = String.format("%s:%s", authConfig.getUsername(), authConfig.getPassword());
    // encode credentials
    byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(cs));
    // return encoded string in plain text
    return String.format("%s %s", authConfig.getType(), new String(encodedAuth));
  }

  /**
   * Configures an instance of HttpEntity so that it includes correct form, request parameters,
   * along with the required headers for the API.
   * 
   * @param sapRequest the SAPRequest object instance mapping the values required for the request to
   *        the SAP API.
   * @return an instance of HttpEntity configured with headers to be sent to SAP API.
   */
  private HttpEntity<MultiValueMap<String, String>> configureRequestEntity(
      SapBaseRequest sapRequest) {
    String json_string = serializeObjectToJson(sapRequest, objectMapper, log);
    if (log.isTraceEnabled()) {
      log.trace(String.format("configureRequestEntity(sapRequest=%s)", json_string));
    }
    // configuring form request params
    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
    map.add(JSON_STRING_REQUEST_FORM_PARAM, json_string);

    // using configured request and pre-populated headers to create HttpEntity
    HttpEntity<MultiValueMap<String, String>> request =
        new HttpEntity<MultiValueMap<String, String>>(map, headers);

    return request;
  }

  /**
   * Configures an instance of URI for SAP bids request with required parameters
   * 
   * @return an instance of URI configured for SAP requests.
   */
  private URI configureRequestUriForBids() {
    return configureRequestUri(sapApiProperties.getPaths().getBids());
  }

  /**
   * Configures an instance of URI for SAP sale request with required parameters
   * 
   * @return an instance of URI configured for SAP requests.
   */
  private URI configureRequestUriForSale() {
    return configureRequestUri(sapApiProperties.getPaths().getSale());
  }

  private URI configureRequestUri(String appendPath) {
    String url = sapApiProperties.getConfiguration().getUrl();
    String sapClient = sapApiProperties.getConfiguration().getQueryParameters().getSapClient();

    // building URI
    String uriPath = String.format("%s/%s", url, appendPath);
    UriComponentsBuilder uriBuilder =
        UriComponentsBuilder.fromHttpUrl(uriPath).queryParam(SAP_CLIENT_REQ_PARAM, sapClient);

    URI requestUri = uriBuilder.build().encode().toUri();
    return requestUri;
  }

  /**
   * For a given SapBidder, it asserts whether it is a correctly agent-assigned phone bidder, or if
   * it represents an absentee bidder
   * 
   * @param b the SapBidder instance to perform assertions on
   * @return true if the SapBidder entry represents either a correctly agent-assigned phone bidder
   *         or an absentee bidder. False otherwise (if it is the case of a phoen bidder unassigned
   *         yet to an agent)
   */
  private boolean isAssignedPhoneOrAbsenteeBidder(SapBidder b) {
    return (X.equalsIgnoreCase(b.getPhoneBid()) && !StringUtils.isEmpty(b.getAssignEmail()))
        || StringUtils.isEmpty(b.getPhoneBid());
  }

  /**
   * For a given SapBidder, it asserts whether it is a correctly phone bidder
   * 
   * @param b the SapBidder instance to perform assertions on
   * @return true if the SapBidder entry represents a phone bidder. False otherwise
   */
  private boolean isPhoneBidder(SapBidder b) {
    return !StringUtils.isEmpty(b.getAssignEmail());
  }

  /**
   * For a given SapBidder, it asserts whether it is not a deleted assignment.
   * 
   * @param b the SapBidder instance to perform assertions on
   * @return true if the SapBidder entry represents an assignment that is not deleted; false
   *         otherwise.
   */
  private boolean isNotDeleted(SapBidder b) {
    return !X.equalsIgnoreCase(b.getDel());
  }

  /**
   * Retrieves SALENUM for every Sale the agent has assigned lots in.
   * 
   * @param agentEmailId the email ID for the agent of interest
   * @return an array of SapBidder objects containing the SALENUM of the assigned sale
   */
  public List<SapBidder> getSalesByAgent(String agentEmailId) {
    log.debug("getSalesByAgent(agentEmailId={})", agentEmailId);
    SapBidsRequest bidsRequest = new SapBidsRequest();
    bidsRequest.setSaleNum(EMPTY_FIELD_VALUE);
    bidsRequest.setEmailId(agentEmailId);

    URI requestUrl = configureRequestUriForBids();
    HttpEntity<MultiValueMap<String, String>> request = configureRequestEntity(bidsRequest);

    try {
      ResponseEntity<SapAbsenteeBidsResponse> responseEntity =
          restTemplate.postForEntity(requestUrl, request, SapAbsenteeBidsResponse.class);

      if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
        handleError(responseEntity, "can't consume SAP API", log);
      }

      SapAbsenteeBidsResponse response = responseEntity.getBody();
      int salesCount = response.getAbsenteeBids().size();
      log.debug("getSalesByAgent response: RETURN={}, got {} sales",
          response.getReturnField().toString(), salesCount);

      return response.getAbsenteeBids();
    } catch (RestClientException e) {
      log.error("can't connect to {}: {}", requestUrl, e.getMessage());
      return null;
    }
  }

  /**
   * Retrieves STATUS, reserve, estimate high and low data for every lot within a registered sale.
   * 
   * @param auctionId the equivalent to saleNum, passed as a parameter to retrieve data
   * @return a list of SaleRecord including those lots with WDN status (withdrawn), or null if a
   *         connection to SAP cannot be obtained.
   */
  public List<SapSaleRecord> getSaleStatusReserveEstimates(String auctionId) {
    log.debug("getSaleStatusReserveEstimates(auctionId={})", auctionId);
    SapSalesRequest salesRequest = new SapSalesRequest();
    salesRequest.setSaleNum(auctionId);

    URI requestUrl = configureRequestUriForSale();
    HttpEntity<MultiValueMap<String, String>> request = configureRequestEntity(salesRequest);

    try {
      ResponseEntity<SapSalesResponse> responseEntity =
          restTemplate.postForEntity(requestUrl, request, SapSalesResponse.class);

      if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
        handleError(responseEntity, "can't consume SAP API", log);
      }

      SapSalesResponse response = responseEntity.getBody();
      int lotsCount = response.getSaleRecord().size();
      long withdrawnLotsCount =
          response.getSaleRecord().stream().filter(l -> l.isWithdrawn()).count();

      log.debug(
          "getSaleStatusReserveEstimates response: RETURN={}, got {} lots, but found {} WDN lots. Returning all.",
          response.getReturnField().toString(), lotsCount, withdrawnLotsCount);

      return response.getSaleRecord();
    } catch (RestClientException e) {
      log.error("can't connect to {}: {}", requestUrl, e.getMessage());
      return null;
    }
  }

  /**
   * Retrieves status, reserve, estimate high and low data for a lot given a registered sale
   * 
   * @param auctionId the equivalent to saleNum, passed as a parameter to retrieve data
   * @param lotId the identifier to the lot we need to obtain fresh reserve/high/low
   * @return an instance of SapSaleRecord, or null if a connection to SAP cannot be obtained.
   */
  public SapSaleRecord getLotStatusReserveEstimates(String auctionId, String lotId) {
    log.debug("getLotStatusReserveEstimates(auctionId={}, lotId={})", auctionId, lotId);
    SapSalesRequest saleRequest = new SapSalesRequest();
    saleRequest.setSaleNum(auctionId);
    saleRequest.setLotNum(lotId);

    URI requestUrl = configureRequestUriForSale();
    HttpEntity<MultiValueMap<String, String>> request = configureRequestEntity(saleRequest);
    SapSaleRecord record = null;

    try {
      ResponseEntity<SapSalesResponse> responseEntity =
          restTemplate.postForEntity(requestUrl, request, SapSalesResponse.class);

      if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
        handleError(responseEntity, "can't consume SAP API", log);
      }

      SapSalesResponse response = responseEntity.getBody();

      if (Optional.ofNullable(response.getReturnField()).isPresent()
          && Optional.ofNullable(response.getSaleRecord()).isPresent()
          && !response.getSaleRecord().isEmpty()) {
        if (log.isTraceEnabled()) {
          log.trace("getLotStatusReserveEstimates response: RETURN={}, first lot={}",
              response.getReturnField().getMessage(), response.getSaleRecord().get(0));
        }
        record = response.getSaleRecord().get(0);
      } else {
        handleError(responseEntity, "Error obtaining sale record from SAP", log);
      }
    } catch (RestClientException e) {
      handleError("Error obtaining sale record from SAP", e, log);
    }
    return record;
  }


  /**
   * Retrieve SAP bidder data given a sale (auction) and an agentId (the email registered for a
   * given agent). In Bidders' app, this list represents the relationship between lots (lotId),
   * agents (agentId) and clients (client), so that all domain-specific data can be retrieved
   * on-demand to incumbent systems afterwards. This means the absentee bids are removed from the
   * list, in case any is returned, as this is used as a base to populate the lot list.
   * 
   * @param auctionId the id of the auction
   * @param agentEmailId the email in upper case, appropriate for SAP searches (stored in keycloak)
   * @return all agent assigned lots, or null if a connection to SAP cannot be obtained.
   */
  public List<SapBidder> getAssignedLotsForAgent(String auctionId, String agentEmailId) {
    log.debug("getAssignedLotsForAgent(auctionId={}, agentEmailId={})", auctionId, agentEmailId);
    SapBidsRequest absenteeBidsRequest = new SapBidsRequest();
    absenteeBidsRequest.setSaleNum(auctionId);
    absenteeBidsRequest.setEmailId(agentEmailId);

    URI requestUrl = configureRequestUriForBids();
    HttpEntity<MultiValueMap<String, String>> request = configureRequestEntity(absenteeBidsRequest);

    try {
      ResponseEntity<SapAbsenteeBidsResponse> responseEntity =
          restTemplate.postForEntity(requestUrl, request, SapAbsenteeBidsResponse.class);

      if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
        handleError(responseEntity, "can't consume SAP API", log);
      }

      SapAbsenteeBidsResponse response = responseEntity.getBody();
      int totalBidders = response.getAbsenteeBids().size();
      List<SapBidder> onlyAssignedNotDelBidLots =
          response.getAbsenteeBids().stream().filter(ab -> isPhoneBidder(ab))
              .filter(ab -> isNotDeleted(ab)).collect(Collectors.toList());

      log.debug(
          "getAssignedLotsForAgent response: RETURN={}, total bidders:{}, bidlots assigned to this agent: {}",
          response.getReturnField().toString(), totalBidders,
          totalBidders - onlyAssignedNotDelBidLots.size());

      return onlyAssignedNotDelBidLots;
    } catch (RestClientException e) {
      log.error("can't connect to {}: {}", requestUrl, e.getMessage());
      return null;
    }
  }

  /**
   * Retrieve SAP bidder data given a sale (auction) and a clientId. In Bidders' app, this data
   * represents the lots of interest for a given client (lotId).
   * 
   * @param auctionId the id of the auction
   * @param accountNumber the number of the account (client id internally)
   * @return all lots of interest entries for a given client and a sale, or null if a connection to
   *         SAP cannot be obtained.
   */
  public List<SapBidder> getLotsOfInterestForClient(String auctionId, String accountNumber) {
    log.debug("getLotsOfInterestForClient(auctionId={}, accountNumber={})", auctionId,
        accountNumber);
    SapBidsRequest absenteeBidsRequest = new SapBidsRequest();
    absenteeBidsRequest.setSaleNum(auctionId);
    absenteeBidsRequest.setClient(accountNumber);

    URI requestUrl = configureRequestUriForBids();
    HttpEntity<MultiValueMap<String, String>> request = configureRequestEntity(absenteeBidsRequest);

    List<SapBidder> lotsOfInterest = Arrays.asList(new SapBidder());
    try {
      ResponseEntity<SapAbsenteeBidsResponse> responseEntity =
          restTemplate.postForEntity(requestUrl, request, SapAbsenteeBidsResponse.class);

      if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
        handleError(responseEntity, "can't consume SAP API", log);
      }

      Optional<SapAbsenteeBidsResponse> response = Optional.ofNullable(responseEntity.getBody());
      if (response.isPresent()) {
        lotsOfInterest = response.get().getAbsenteeBids();
        List<SapBidder> onlyAssignedPhoneAndAbsentees = lotsOfInterest.stream()
            .filter(b -> isAssignedPhoneOrAbsenteeBidder(b)).collect(Collectors.toList());
        log.debug(
            "getLotsOfInterestForClient response: RETURN={}, (unfiltered) lots of interest: {}, only assigned/absentee: {}",
            response.get().getReturnField().toString(), lotsOfInterest.size(),
            onlyAssignedPhoneAndAbsentees.size());
      } else {
        String message =
            String.format("cannot retrieve lots of interest for client %s", accountNumber);
        handleError(responseEntity, message, log);
      }
    } catch (RestClientException e) {
      String message = String.format("can't connect to %s: %s", requestUrl, e.getMessage());
      handleError(message, e, log);
    }

    return lotsOfInterest;
  }

  /**
   * Retrieve SAP bidder data given a sale and lotId. On Auctioneer's app, this piece of data will
   * represent the list of phone agents (agentId, paddle) and the list of absentee bidders (absentee
   * bid amount, paddle). Both lists are displayed in an individual lot view.
   * 
   * @param auctionId the id of the auction
   * @param lotId the id of the lot
   * @return all absentee and phone agent assigned to a given lot in a sale, or null if a connection
   *         to SAP cannot be obtained.
   */
  public List<SapBidder> getAbsenteesAndPhoneBiddersForLot(String auctionId, String lotId) {
    log.debug("getAbsenteesAndPhoneBiddersForLot(auctionId={}, lotId={})", auctionId, lotId);
    SapBidsRequest absenteeBidsRequest = new SapBidsRequest();
    absenteeBidsRequest.setSaleNum(auctionId);
    absenteeBidsRequest.setLotNum(lotId);

    URI requestUrl = configureRequestUriForBids();
    HttpEntity<MultiValueMap<String, String>> request = configureRequestEntity(absenteeBidsRequest);

    List<SapBidder> result = null;
    try {
      ResponseEntity<SapAbsenteeBidsResponse> responseEntity =
          restTemplate.postForEntity(requestUrl, request, SapAbsenteeBidsResponse.class);

      if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
        handleError(responseEntity, "can't consume SAP API", log);
      }

      SapAbsenteeBidsResponse response = responseEntity.getBody();
      List<SapBidder> filteredDeletedBidders =
          response.getAbsenteeBids().stream().filter(b -> isNotDeleted(b))
              .filter(b -> isAssignedPhoneOrAbsenteeBidder(b)).collect(Collectors.toList());

      log.debug(
          "getAbsenteesAndPhoneBiddersForLot response: RETURN={}, total bidders: {}, filtered deleted: {}",
          response.getReturnField().toString(), response.getAbsenteeBids().size(),
          filteredDeletedBidders.size());

      result = filteredDeletedBidders;
    } catch (Exception e) {
      log.error("can't connect to {}: {}", requestUrl, e.getMessage());
      handleError("Error getting absentees and phone bidders from SAP", e, log);
    }
    return result;
  }

  /**
   * Retrieve SAP bidder data for an assigned lot and a client. In Bidders' app, this piece of data
   * will represent the relationship between client and lot (cover/absentee bid amount,
   * registration, paddle).
   * 
   * @param auctionId the id of the auction
   * @param lotId the id of the lot
   * @param accountNumber the id of the account (clientId internally)
   * @return bidder data for a given client v. lot relationship, or null if a connection to SAP
   *         cannot be obtained.
   */
  public SapBidder getBidderDataForLot(String auctionId, String lotId, String accountNumber) {
    log.debug("getBidderDataForLot(auctionId={}, lotId={}, accountNumber={})", auctionId, lotId,
        accountNumber);
    SapBidsRequest absenteeBidsRequest = new SapBidsRequest();
    absenteeBidsRequest.setSaleNum(auctionId);
    absenteeBidsRequest.setLotNum(lotId);
    absenteeBidsRequest.setClient(accountNumber);

    URI requestUrl = configureRequestUriForBids();
    HttpEntity<MultiValueMap<String, String>> request = configureRequestEntity(absenteeBidsRequest);
    SapBidder sapBidder = null;
    try {
      ResponseEntity<SapAbsenteeBidsResponse> responseEntity =
          restTemplate.postForEntity(requestUrl, request, SapAbsenteeBidsResponse.class);

      if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
        handleError(responseEntity, "can't consume SAP API", log);
      }

      SapAbsenteeBidsResponse response = responseEntity.getBody();
      Optional<List<SapBidder>> absenteeBids = Optional.ofNullable(response.getAbsenteeBids());
      if (absenteeBids.isPresent() && !absenteeBids.get().isEmpty()) {
        log.trace("getBidderDataForLot response: RETURN={}", response.getReturnField().toString());
        sapBidder = response.getAbsenteeBids().get(0);
      } else {
        String message =
            String.format("cannot retrieve data for assigned lot %s (auction %s) to client %s: %s",
                lotId, auctionId, accountNumber, response.getReturnField().getMessage());
        handleError(responseEntity, message, log);
      }
    } catch (RestClientException e) {
      String message = String.format("can't connect to %s: %s", requestUrl, e.getMessage());
      handleError(message, e, log);
    }
    return sapBidder;
  }

}
