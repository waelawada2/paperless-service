package com.sothebys.paperless.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.paperless.exceptions.PaperlessServiceException;
import java.io.IOException;
import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;

public interface PaperlessIntegrationClient {

  /**
   * Handles an error when communicating to an external REST API by using returned HTTP error code.
   * 
   * @param errorMessage the desired error message that needs to be propagated across the
   *        application.
   * @param log the instance of Logger to write to configured appenders.
   */
  default void handleError(String errorMessage, Logger log) {
    errorMessage = String.format("%s", errorMessage);
    log.error(errorMessage);
    throw new PaperlessServiceException(errorMessage);
  }

  /**
   * Handles an error when communicating to an external REST API by using returned HTTP error code.
   * 
   * @param response the ResponseEntity instance containing the error code plus the entity with
   *        error details.
   * @param errorMessage the desired error message that needs to be propagated across the
   *        application.
   * @param log the instance of Logger to write to configured appenders.
   */
  default void handleError(ResponseEntity<?> response, String errorMessage, Logger log) {
    errorMessage = String.format("%s [HTTP request response was %s: %s])", errorMessage,
        response.getStatusCodeValue(), response.getStatusCode().getReasonPhrase());
    log.error(errorMessage);
    throw new PaperlessServiceException(errorMessage);
  }

  /**
   * Handles an error when communicating to an external REST API, when there's no response.
   * 
   * @param errorMessage the desired error message that needs to be propagated across the
   *        application.
   * @param e exception thrown in the calling code
   * @param log the instance of Logger to write to configured appenders.
   */
  default void handleError(String errorMessage, Exception e, Logger log) {
    log.error("Error connecting to external service: {}", errorMessage);
    throw new PaperlessServiceException(errorMessage);
  }

  /**
   * Attempts to handle serialization for a given object, provided it complies with Jackson
   * serialization policies.
   * 
   * @param <T> the type of the object needed  to be serialized
   * @param object the instance of the object to be serialized
   * @param objectMapper the instance of Jackson ObjectMapper that handles serialization
   * @param log the instance of Logger to write to configured appenders.
   * @return a String with the JSON version of the object passed
   * @throws PaperlessServiceException in case the serialization fails
   */
  default <T> String serializeObjectToJson(T object, ObjectMapper objectMapper, Logger log) {
    String serialized = null;
    try {
      serialized = objectMapper.writeValueAsString(object);
    } catch (JsonProcessingException e) {
      String errMessage =
          String.format("Can't serialize request to SAP API. Object content: %s", object);
      // using String.format given the exception is taken as an object to expand expression
      log.error(errMessage, e);
      throw new PaperlessServiceException("error deserializing response from SAP API", e);
    }
    return serialized;
  }

  /**
   * Attempts to handle deserialization of a JSON object, provided the JSON is valid for
   * deserialization.
   * 
   * @param <T> the type of the data that needs deserialization.
   * @param json a String version of the response that needs to be used to populate an object
   *        instance.
   * @param valueType the typed class value to be used as type parameter to the deserializer.
   * @param objectMapper the instance of Jackson ObjectMapper that handles deserialization
   * @param log the instance of Logger to write to configured appenders.
   * @return a deserialized version of the data passed as a parameter
   */
  default <T> T deserializeJsonString(String json, Class<T> valueType, ObjectMapper objectMapper,
      Logger log) {
    T deserialized = null;
    try {
      deserialized = objectMapper.readValue(json, valueType);
    } catch (IOException e) {
      String errMessage =
          String.format("Can't deserialize response from SAP API request. Raw content: %s", json);
      // using String.format given the exception is taken as an object to expand expression
      log.error(errMessage, e);
      throw new PaperlessServiceException("error deserializing response from SAP API", e);
    }
    return deserialized;
  }

}
