package com.sothebys.paperless.configuration;

import lombok.Data;

@Data
public class ClientApiHeaders {

  private String cacheControl;
  private String clientId;
  private String clientSecret;
  private String xApiKey;

}
