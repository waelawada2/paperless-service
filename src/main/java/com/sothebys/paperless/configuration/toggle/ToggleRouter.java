package com.sothebys.paperless.configuration.toggle;

import com.sothebys.paperless.persistence.entity.Feature;
import com.sothebys.paperless.persistence.entity.FeatureConfig;
import com.sothebys.paperless.persistence.repository.FeatureConfigRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ToggleRouter {

  private FeatureConfigRepository featureConfigRepository;

  public ToggleRouter(FeatureConfigRepository featureConfigRepository) {
    this.featureConfigRepository = featureConfigRepository;
  }

  /**
   * Returns the enabled state of a given feature
   * 
   * @param feature a Feature mapped by enum
   * @return true, if config is found and an entry for that Feature enum value is found with true
   *         value.
   */
  public boolean isFeatureEnabled(Feature feature) {
    FeatureConfig featureConfig = featureConfigRepository.findOne(feature);
    if (featureConfig != null) {
      log.debug(
          String.format("is feature %s enabled: %s", feature.name(), featureConfig.isEnabled()));
      return featureConfig.isEnabled();
    }
    log.warn(String.format("feature % configuration not available", feature.name()));
    return false;
  }

  /**
   * Toggles a feature enabled/disabled. If no config is found, a new entry is inserted as enabled.
   * Otherwise, the value is switched.
   * 
   * @param feature a Feature mapped by enum
   */
  public void toggleFeature(Feature feature) {
    FeatureConfig persisted = featureConfigRepository.findOne(feature);
    if (persisted == null) {
      featureConfigRepository.save(new FeatureConfig(feature, true));
    } else {
      featureConfigRepository.save(new FeatureConfig(feature, !persisted.isEnabled()));
    }
  }

}
