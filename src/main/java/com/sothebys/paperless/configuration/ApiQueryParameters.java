package com.sothebys.paperless.configuration;

import lombok.Data;

@Data
public class ApiQueryParameters {
  private String sapClient;
}
