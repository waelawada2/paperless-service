package com.sothebys.paperless.configuration;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.paperless.domain.conversion.WebApiBidLotToBidLot;
import com.sothebys.paperless.util.UserHandlerMethodParameterResolver;
import java.util.List;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableTransactionManagement
@EnableConfigurationProperties({DateTimeConfiguration.class, ClientApiProperties.class,
    SapApiProperties.class, WebApiProperties.class})
public class PaperlessServiceConfiguration {

  /**
   * WebMVC Configuration.
   * @return an instance of WebMvcConfigurer
   */
  @Bean
  public WebMvcConfigurer webMvcConfigurer() {
    return new WebMvcConfigurerAdapter() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
      }

      @Override
      public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new UserHandlerMethodParameterResolver());
      }
    };
  }

  /**
   * Spring Converters Configuration.
   * @return an instance of ConversionService
   */
  @Bean
  public ConversionService conversionService() {
    DefaultConversionService defaultConvService = new DefaultConversionService();
    defaultConvService.addConverter(new WebApiBidLotToBidLot());
    return defaultConvService;
  }

  /**
   * RestTemplate for API calls
   * 
   * @param builder the RestTemplateBuilder instance needed to create a RestTemplate
   * @return an instance of RestTemplate
   */
  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    return builder.build();
  }

  /**
   * Object Mapper for Json transformations
   * @return a configured instance of ObjectMapper
   */
  @Bean
  public ObjectMapper mapper() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
    return mapper;
  }
}
