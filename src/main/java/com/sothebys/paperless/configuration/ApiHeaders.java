package com.sothebys.paperless.configuration;

import lombok.Data;

@Data
public class ApiHeaders {
  private String acceptLanguage;
  private String connection;
  private String userAgent;
  private ApiAuthorization authorization;
}
