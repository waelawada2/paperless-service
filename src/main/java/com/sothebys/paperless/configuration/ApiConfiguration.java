package com.sothebys.paperless.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "sap")
@Data
public class ApiConfiguration {
  private ApiHeaders headers;
  private String url;
  private Sync sync;
  private ApiQueryParameters queryParameters;
  private String amountsFormat;
  private String numberFormat;
}
