package com.sothebys.paperless.configuration;

import lombok.Data;

@Data
public class ApiPaths {
  private String lots;
  private String auctionsList;
  private String bids;
  private String sale;
}
