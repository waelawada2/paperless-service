package com.sothebys.paperless.configuration;

import lombok.Data;

@Data
public class ClientApiPaths {

  String findHolderPartyByAccountNumber;

}
