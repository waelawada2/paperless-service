package com.sothebys.paperless.configuration;

import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix="datetime")
@Data
public class DateTimeConfiguration {
  private String dateFormat;
  private String timeFormat;
  private String defaultTimezone;
  private Map<String, String> timeZones;
}
