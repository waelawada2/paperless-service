package com.sothebys.paperless.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "clientapi")
public class ClientApiProperties {
  
  ClientApiConfiguration configuration;
  ClientApiPaths paths;
  
}
