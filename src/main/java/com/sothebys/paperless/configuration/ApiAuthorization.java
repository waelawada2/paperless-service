package com.sothebys.paperless.configuration;

import lombok.Data;

@Data
public class ApiAuthorization {
  private String type;
  private String encoding;
  private String username;
  private String password;
}
