package com.sothebys.paperless.configuration;

import lombok.Data;

@Data
public class ClientApiConfiguration {

  ClientApiHeaders headers;
  String url;
  
}
