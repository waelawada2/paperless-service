package com.sothebys.paperless.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@Data
@ConfigurationProperties(prefix = "webapi")
public class WebApiProperties {
  private ApiConfiguration configuration;
  private ApiPaths paths;
}
